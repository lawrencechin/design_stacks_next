# Design Stacks: Next

![home](./readme_images/main.jpg)

A web app for collecting design-related images, bookmarks and rss feeds written in **php**. 

The next version of [Design Stacks](https://gitlab.com/lawrencechin/design_stacks). The main differences between the two versions include changing the php framework allowing for multiple user accounts and a modified design. The new design removes transparencies and applies a flatter style that was popular at the time.

## Screenshots

Unfortunately I don't have many images for this site; curious as I screenshot everything. With that in mind, if you would like to see the original designs then point your browser [here](https://lawrencechin.gitlab.io/design_stacks_next/).

### Home

![Homepage](./readme_images/home.jpg)
![Another home image](./readme_images/home_2.jpg)

### Design

![Image Collections](./readme_images/image_collections.jpg)
![Image Viewer](./readme_images/image_viewer.jpg)

### Feeds

![Feeds](./readme_images/feeds.jpg)

### Temp

![Temp](./readme_images/temp.jpg)
![Temp 2](./readme_images/temp_2.jpg)
