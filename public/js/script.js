var globals = {

	globalHeader: $('#mainHeader'),
	globalLogo: $('#logo'),
	globalModelLink: null,
	globalWallPaperStuff: null,
	//store last clicked button based on location - used to trigger close buttons
	headerButton : null,
	midButton: null,
	contentButton: null,
	body: $('body'),
	acceptableExt : ['jpeg', 'jpg', 'gif', 'png', 'tiff'],
	mediaQuery : window.matchMedia( "(max-width : 31.25em)" ),
	mobileNav : $('#expose_menus'),
	//change this when uploading site
	url : 'http://designstacks.000space.com/'

};

var utils = {
	
	loading : $('#loading_and_thumbs'),
	snp : $('#shallNotPass'),

	pleaseWait : function(){
		utils.loading.fadeIn();
		utils.snp.fadeIn().addClass('loading');
	},

	simpleSuccess : function(){
		utils.loading.fadeOut();
	},

	success : function(){
		utils.snp.removeClass('loading').addClass('success');
		utils.snp.find('p').html('Success!');
		
		setTimeout(function(){
			utils.loading.fadeOut();
			utils.snp.removeClass('success').fadeOut();
			utils.snp.find('p').html('Please Wait');
		}, 600);
	},

	error : function(resp){

		utils.snp.removeClass('loading').addClass('error');
		utils.snp.find('p').html(resp.msg);
		
		setTimeout(function(){
			utils.loading.fadeOut();
			utils.snp.removeClass('error').fadeOut();
			utils.snp.find('p').html('Please Wait');
		}, 2000);
	}
};

//Contains all functionality regarding forms. Nb. We previously used a form plugin for file uploads and a seperate object containing functions. Now we're using formData and doing everything here sans plugin. Cool.
var formSubmission = {

	cancelBtn : null,

	revealForm: function(button){
		//wrap button in 'query, use button to find corresponding form, remove active class from button
		var $button = $(button).removeClass('activeButton'),
		formID = '#'+ $button.attr('class')+'_form',
		buttonType = parseInt($button.attr('data-button')),
		form = globals.body.find(formID),

		buttonGlobal = buttonType === 1 ? 'headerButton' : buttonType === 2 ? 'midButton' : buttonType === 3 ? 'contentButton' : '';

		showForm(form, $button, buttonGlobal);

		if(buttonType === 2 || buttonType === 3){
			formSubmission.fillOutForm($button, form);
		}

		function hideForm(form, buttonG, buttonC, buttonGlobal){
			//toggle form display and toggle active button and remove selectList menu

			if(buttonG === null){
				//global not set
				return true;
			}else if(buttonG[0] === buttonC[0]){
				//if global and clicked are same
				form.removeClass('displayForm').removeClass('focusForm');
				buttonG.removeClass('activeButton');
				globals.body.find('.selectConvert').remove();
				setGlobal(button, buttonGlobal, false);
				header.revealMenu(true);
				return false;
			}else{
				buttonG.removeClass('activeButton');
				var formOldId = '#'+buttonG.attr('class')+'_form';
				globals.body.find(formOldId).removeClass('displayForm');
				globals.body.find('.selectConvert').remove();
				header.revealMenu(true);
				return true;
			}	
		}

		function showForm(form, button, buttonGlobal){
			if(hideForm(form, globals[buttonGlobal], button, buttonGlobal)){
				//check for mediaquery, check if a midbutton is clicked
				if(globals.mediaQuery.matches){
					if(buttonType === 2){
						globals.globalHeader.removeClass('mobileShiftMenu');
					}
					
				}

				if(!$('#revealMenu').hasClass('activeButton')){
					header.revealMenu();
				}
				
				form.addClass('displayForm').addClass('focusForm');
				button.addClass('activeButton');
				setGlobal(button, buttonGlobal, true);
				//convert select lists into a nicer looking form
				formSubmission.selectLists(form, true);
			}
		}

		function setGlobal(button, buttonGlobal, flag){
			//store last clicked button in global variable if different from previous click
			if(flag){
				globals[buttonGlobal] = button;
			}else{
				globals[buttonGlobal] = null;
			}
		}	
	},

	fillOutForm : function(button, form){
		var dataHolder = button.siblings()[0],
		dataF;
		if(dataHolder.tagName.toLowerCase() === 'div'){
			dataF = $(dataHolder).children();
		}else{
			dataF = $(dataHolder);
		}

		if(dataF.get(0).tagName.toLowerCase() === 'form'){
		}else{
			var dataAttr = dataF.data();
			if(dataAttr.location === 'stacks'){
				stacks.formEntry(dataF, dataAttr, form);
			}else if(dataAttr.location === 'default'){
				bookmarks.formEntry(dataF, dataAttr, form);
			}else{
				feeds.formEntry(dataF, dataAttr, form);
			}
		}
	},

	selectLists: function(form, flag){
		//HTML fragments to build the node
		var selectToList = $('<ul class="selectConvert">'),
		count = 0,
		searchbox = $('<div class="searchBar"><input type="text" name="search_models" placeholder="Search Name…"><button></button></div>');

		if(flag){
			form.find('select option').each(function(){
				var $this = $(this);

				if($this.data('catsort') === undefined){
					selectToList.append('<li class="'+$this.data('catcolour')+'"><a href="'+ $this.val()+'">'+ $this.text() +'</a></li>');
					count++;

				}else{
					selectToList.append('<li class="'+$this.data('catsort')+'"><a href="'+ $this.val()+'">'+ $this.text() +'</a><span>'+$this.data('catsort')+'</span></li>');
					count++;
				}
			});

			if(count > 12){
				selectToList.prepend(searchbox); 
				selectToList.liveFilter(searchbox.find('input'), 'li');
				selectToList.find('button').on('click', function(e){
					e.preventDefault();
					globals.body.find('.selectConvert').empty().remove();
					formSubmission.selectLists(form, true);
				});
			}

			form.find('.selectList').off().on('click', function(e){
				e.preventDefault();
				if(count > 12){
					selectToList.addClass('largeList');
				}
				
				globals.body.append(selectToList);
				globals.body.find('.selectConvert').off().on('click', 'a', function(e){

					e.preventDefault();
					var selectOption = $(this).attr('href'),
					selectText = $(this).text();

					form.find('a.selectList').text(selectText);
					formSubmission.selectLists(form, true);

					form.find('select').val(selectOption);
					globals.body.find('.selectConvert').empty().remove();
				});
			});
		}
	},

	deleteInlineForm: function(button){
		var $button = $(button),
		btnClass = $button.attr('class'),
		formID = '#'+btnClass+"_form",
		link = $button.parent().find('a:first'),
		deleteID = link.attr('data-id');

		if(btnClass.indexOf('_feed') > 0){
			link = $button.parent().find('span');
			deleteID = link.attr('data-id');
		}

		function removeBookmark(){
			$button.parent().remove();
		}

		if(confirm('Are you sure?')){
			globals.body.find(formID).find('[name*="_ID"]').val(deleteID).end().find('[type="submit"]').trigger('click');
			$button.addClass('activeButton');
			$button.parent().addClass('bookmark_remove');
			setTimeout(removeBookmark, 700);
		}
	},

	submitForms: function(nameFragment, flag){
		globals.body.find('form[id*="'+nameFragment+'"]').submit(function(e){
			e.preventDefault();
			var $this = $(this),
			action = $this.attr('action'),
			data;
			formSubmission.cancelBtn = $this.find('[name^="cancel_"]');
			if(flag){
				data = new FormData(document.forms.namedItem($this.attr('name')));
			}else{
				data = projects.formData($this);
			}

			formSubmission.postForms(action, data);
			
		});
	},

	postForms : function(url, data){
		utils.pleaseWait();

		$.ajax({
		  	type: "POST",
		  	url: url,
			data: data,
			dataType : 'json',
			processData: false,  // tell jQuery not to process the data
  			contentType: false })  // tell jQuery not to set contentType
		  	//check for errors
			.done(function(resp){
			  	if(resp.error){
					utils.error(resp);
				}else{
					utils.success();
					//make the form vanish upon success
					if(formSubmission.cancelBtn !== null){
						formSubmission.cancelBtn.trigger('click');
					}
					//actions to take depending on response
					formSubmission.formSuccess(resp);
				}
			}).fail(function(resp){
				utils.error(resp);
			});
	},

	formSuccess : function(json){
		switch(json.msg){
			case 'BookmarkChanged' :
				if(globals.body.find('#section_bookmarks').length){
					globals.globalHeader.find('#bookmarks').find('a:first').trigger('click');
				}
			break;

			case 'ModelChanged' :
				if(globals.body.find('#section_categories').length){
					//globals.globalHeader.find('#stack').find('a:first').trigger('click');
					$.get(globals.url+'stacks/stacksRefresh', function(resp){
						globals.body.find('#section_categories').empty().append(resp);
					});
					header.refreshHeader('#move_photo_projects');
				}else if(globals.body.find('#temp_files').length){
					header.refreshHeader('#dir_list');
					formSubmission.selectLists($('#dir_list'), true);
				}
				header.refreshHeader('#add_file_form');
			break;

			case 'ModelDeleted' : 
				//header.refreshHeader('#add_file_form');
				if(globals.body.find('#section_categories').length){
					header.refreshHeader('#add_file_form');
				}
			break;

			case 'FilesAdded' : 
				if(globals.body.find('#section_cat_content').length && globals.globalModelLink !== null){
					globals.globalModelLink.trigger('click');
				}else if(globals.body.find('#temp_files').length){
					temp.tempRemoveFiles(temp);
				}
			break;

			case 'FilesChanged' : 
				temp.tempRemoveFiles(projects);
			break;

			case 'FilesDeleted' : 
				temp.tempRemoveFiles(temp);
			break;

			case 'FeedChanged' : 
				$('#feed_control').find('button:nth-child(3)').trigger('click');
			break;

			case 'NoteChanged' :
				projects.noteFinish(json.note);
			break;
		}
	},

	closeForm : function(button){
		var $button = $(button),
		buttonToTrigger = $button.attr('data-cancel') === 'global' ? 'headerButton' : $button.attr('data-cancel') === 'midForm' ? 'midButton' : $button.attr('data-cancel') === 'contentForm' ? 'contentButton' : false;
		if(globals[buttonToTrigger] === null){

		}else{
			globals[buttonToTrigger].trigger('click');
		}
		
	},

	checkFileTypes : function(input){
		function hasExtension(fileName, exts) {
		    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
		}

		$input = $(input);
		$input.off().on('change', function(){
			var inputRaw = $input.get(0),
			fileArray = [];

			//for every file...
			for(var x = 0; x < inputRaw.files.length; x++) {
				if(!hasExtension(inputRaw.files[x].name, globals.acceptableExt)){
					fileArray.push(inputRaw.files[x].name);
				}
			}
			if(fileArray.length > 0){
				alert('You have selected incompatible file types. Acceptable file types are: '+globals.acceptableExt+'. The following files did not match: '+fileArray);
			}
		});	
	}

};

//Contains functions that control basic page loading
var header = {
	clickedStack : '',
	//functions that run on page load
	init : function(){
		header.loadContent();
		header.homeButton();
		formSubmission.submitForms('_form', true);
		wallpapers.fontSelection();
		header.refreshFeedCount(true);
		formSubmission.checkFileTypes('#fileUpload');

		Mousetrap.bind('b o o k', function(){ 
			if(!$('#md_stuff').is(':visible')){
				globals.globalHeader.find('.add_bookmark').trigger('click');
			}
		});
		Mousetrap.bind('u p', function(){ 
			if(!$('#md_stuff').is(':visible')){
				globals.globalHeader.find('.add_file').trigger('click');
			}
		});
		Mousetrap.bind('n e w', function(){
			if(!$('#md_stuff').is(':visible')){
				globals.globalHeader.find('.add_model').trigger('click');
			} 
			
		});

		//checks if we're in a specific media query
		if(globals.mediaQuery.matches){
			globals.globalHeader.find('#globalForms').after($('#wallpapers'));
			header.mobileMenu();
		}else{
			//grab the background image. Don't want to load this if on mobile
			$.get(globals.url+'stacks/getBckImg', function(resp){
				$('img', '#bg').attr('src', resp);
			});
		}
	},

	homeButton : function(){
		globals.globalLogo.on('click', function(){
			//globals.body.
			header.removeContent();
			header.mobileControl('','', true);
			//remove styles specfic to sections
			globals.body.removeClass('stacksSidebar');
			header.revealMenu(true);
			globals.body.find('.selectConvert').remove();

			mediaView.slideShowOff($('#slideShowButton'));
			$('#wallpapers').show();
		});	
	},

	loadContent : function(flag, link){

		function showContent(data){
			if(globals.mediaQuery.matches){
				globals.globalHeader.after(data);
				globals.globalHeader.addClass('mobileShiftMenu');
				header.mobileControl('','', true);
			}else{
				$('#wallpapers').after(data);
			}
			globals.body.find('.selectConvert').remove();
		}

		function finalLoad(link){
			$.ajax({
				type : 'GET',
				url : link })
				.done(function(data){
					showContent(data);
					$('#wallpapers').hide();
				});
		}

		if(flag){
			header.removeContent(true);
			finalLoad(link);
		}else{
			//load content into main section
			globals.globalHeader.on('click', 'a.navigation', function(e){
				e.preventDefault();
				//remove previous content
				header.removeContent();

				mediaView.slideShowOff($('#slideShowButton'));

				//remove styles specfic to sections
				globals.body.removeClass('stacksSidebar');
				header.revealMenu(true);

				finalLoad($(this).attr('href'));
			});
		}
	},

	loadExternalLinks : function(container){
		globals.body.find(container).on('click', 'a.external', function(e){
			e.preventDefault();
			var $this = $(this),
			location = $this.data();

			//internal function to load content through different mechanisms
			function loadContent(cont, link, location){
				var url = $(link).attr('href'),
				$container = $(cont).next('div').find('#content');
				$container.load(url, function(){
					globals.body.scrollTo(0);

					if(feeds.editBtn !== null){
						feeds.editBtn.removeClass('activeButton');
					}

					if(globals.mediaQuery.matches){

						if(location === 'stacks'){
							header.mobileControl(cont, '.expose_stacks');
						}else if(location === 'feedContent'){
							header.mobileControl(cont, '.expose_feeds');
						}
					}
				});
			}

			if(location.location === "stacks"){
				$('body').find('#noteSyntax').remove();
				loadContent(container, $this, location.location);
				var editbtn = $(container).siblings('div').find('button.editProjectFiles');
				if(editbtn.hasClass('activeButton')){
					editbtn.trigger('click');
				}
				globals.globalModelLink = $this;
				globals.body.addClass('stacksSidebar');
				//store parent location so we can scroll to it upon clicking and when we close the window
				header.clickedStack = $this.parent('div');
				setTimeout(function(){
					$(container).scrollTo(header.clickedStack);
				}, 1000);

			}else if(location.location === "feedContent"){
				loadContent(container, $this, location.location);
				feeds.markAsRead($this);
			}else{
				header.loadExternal($this.attr('href'));
				if(globals.mediaQuery.matches){
					header.mobileControl(container, '.expose_books');
				}
			}
		});
	},

	loadExternal : function(link){
		//first we check the x-frame status of the website. If positive we then load link in iframe or throw an error message
		var url = globals.url + 'bookmarks/checkXFrame/';

		$.ajax({
			url: url,
			type: 'post',
		  	data: {'url' : link}, 
		  	dataType: 'json',
		  	timeout: 10000 })
		  	.done(function(resp){
			  	if(resp.error){
			  		alert('Page unable to load in iframe');
			  	}else{
			  		var iframe = $('#contentFrame');
			  		iframe.attr('src', link);
			  		globals.body.addClass('stacksSidebar');
			  		var input = iframe.parent('div').siblings('header').find('input');
			  		input.val(iframe.attr('src'));
			  	}
			})
			.fail(function(resp){
			  	alert('There was an error fetching the site');
			});
	},

	mobileMenu : function(){
		globals.mobileNav.on('click', 'a', function(e){
			e.preventDefault();
			var $this = $(this),
			showClass = 'mobileShiftMenu';

			if($this.parent().hasClass('expose_main')){
				globals.globalHeader.toggleClass(showClass);
			}else if($this.parent().hasClass('expose_books')){
				$('#section_bookmarks').toggleClass(showClass);
			}else if($this.parent().hasClass('expose_stacks')){
				$('#section_categories').toggleClass(showClass);
			}else{
				$('#section_feeds').toggleClass(showClass);
			}
		});
	},

	mobileControl : function(shiftCont, button, flag){
		//function to hide secondary menu and reveal the, um, reveal button
		var exposedBtns = globals.mobileNav.find('.revealExposeBtn');
		if(flag){
			exposedBtns.removeClass('revealExposeBtn');
		}else{
			$(shiftCont).addClass('mobileShiftMenu');
			exposedBtns.removeClass('revealExposeBtn').end().find(button).addClass('revealExposeBtn');
		}
		
	},

	//Update Feeds badge every five minutes by callling a php page that returns the Feed_List count
	refreshFeedCount : function(flag){
		var feedLink = globals.globalHeader.find("#rss").find("a"),
		url = globals.url + 'feeds/getCount';

		$.ajax({
		  type: 'GET',
		  url: url,
		  dataType: 'json',
		}).done(function(resp){
	  		feedLink.attr('data-feedcount', resp.count);
	  		if(flag){
	  			setTimeout(header.refreshFeedCount, 300000, true);
	  		}
		  })
		  .fail(function() {
		  	alert('Could not retrieve unread feed count');
		});
	},
	//when adding/editing new design projects in various places the global add file forms select lists will not be updated. This function fixes that. 
	refreshHeader : function(form){
		//we have a part of the header pertaining to the select lists in a php file
		function headerStyle(data){
		    globals.body.find(form).find('select').replaceWith(data);
		}
		//we grab that data, store it in a div and replace it in the form. Simple. 	
		$.get(globals.url + 'stacks/refreshHeader', headerStyle);
	},

	removeContent : function(flag){
		if(flag){
		}else{
			globals.globalHeader.removeClass('mobileShiftMenu');
		}
		if(globals.mediaQuery.matches){
			loadedContent = globals.body.contents(':gt(2)');
		}else{
			loadedContent = globals.body.contents(':gt(3)');
		}
		
		//loop through list and remove everything upto footer or nothing if only footer exists
		function remove(){
			if(loadedContent.length > 1){
				for(var i = 0; i < loadedContent.length-2; i++){
					loadedContent[i].remove();
				}
			}	
		}
		
		setTimeout(remove, 300);
	},

	revealMenu : function(flag){
		var $button = $('#revealMenu');

		if(flag){
			globals.body.removeClass('reveal');
			$button.removeClass('activeButton');
		}else if(!globals.body.hasClass('stacksSidebar')){
			//don't add class in this event
			$button.removeClass('activeButton');
		}else{
			globals.body.toggleClass('reveal');
			$button.toggleClass('activeButton');
		}
	},

	close : function(flag){
		mediaView.slideShowOff($('#slideShowButton'));

		if(flag){
			globals.body.find('#content').empty();
			globals.globalModelLink = '';
			if(header.clickedStack !== ''){
				setTimeout(function(){
					$('#section_categories').scrollTo(header.clickedStack);
				}, 600);
				if(header.clickedStack.parent().parent().find('header').find('.search').hasClass('activeButton')){
					header.clickedStack.parent().parent().find('header').find('.search').trigger('click');
				}
			}
		}
		globals.body.find('#content').find('#contentFrame').attr('src', "about:blank");
		$('body').find('#noteSyntax').remove();
		globals.body.removeClass('stacksSidebar');
	},

	iframeChangeLocation : function(button){
		var $button = $(button),
		locations = $button.siblings('input').val();

		header.loadExternal(locations);
	}
};

//lazyload object 
var lazyLoad = {

	midContainer : '',
	midImage : '',
	midImgCont : '',
	contentImage: '',
	contentImgCont: '',

	init : function(image, imageContainer, container, flag){
		if(flag){
			this.midContainer = $(container);
			this.midImage = image;
			this.midImgCont = imageContainer;

			$(this.midImage, this.midImageContainer).lazyload({
				container : lazyLoad.midContainer
			});
		}else{
			this.contentImage = image;
			this.contentImgCont = imageContainer;

			$(this.contentImage, this.contentImgCont).lazyload({
				skip_invisible : false,
				container : globals.body
			});
		}
		
	},
	//when changing the order of images lazyload stops working correctly so we hit it up again
	triggerLoad : function(flag){
		if(flag){
			lazyLoad.init(this.midImage, this.midImgCont, this.midContainer, flag);
		}else{
			lazyLoad.init(this.contentImage, this.contentImgCont, '', flag);
		}
	}

};

var bookmarks = {
	//toggle classes of divs
	bookMenu : function(){
		var bookMenu = $('#bookmark_menu');
		bookMenu.on('click', 'a:not(.external)', function(e){
			e.preventDefault();
			var $this = $(this);
			//check if anything else has active class and remove except itself
			function loopList(container, link){
				container.find('div[class^="bookMenu_"]').each(function(){
					if(link.is($(this))){
					}else if($(this).hasClass('activeBook')){
						$(this).removeClass('activeBook');
						$(this).parents('#section_bookmarks').toggleClass('noScroll');
					}
				});
			}

			loopList(bookMenu, $this.parent('div'));

			//toggle class and scroll to top
			$this.parent('div').toggleClass('activeBook').end().parents('div#section_bookmarks').toggleClass('noScroll').scrollTo(0);
		});
	},
	//grab potential name and url from iframe and trigger the add bookmark button in header with those details
	bookmarkAdd : function(button){
		var $button = $(button),
		url = $button.siblings('input').val(),
		worldwide = url.indexOf('www'),
		index1 = null, index2 = null;

		if(worldwide > -1){
			index1 = url.indexOf('.')+1;
			index2 = url.indexOf('.', index1);
		}else{
			index1 = 7;
			index2 = url.indexOf('.', index1);
		}
		
		var name = url.substring(index1, index2);

		globals.globalHeader.find('#globalForms').find('.add_bookmark').trigger('click');

		globals.globalHeader.find('#add_bookmark_form').find('input[name="title"]').val(name).end().find('input[name="url"]').val(url);
	},

	formEntry : function(dataF, dataAttr, form){
		form.find('[name="edit_book_ID"]').val(dataAttr.id);
		form.find('[name="edit_title"]').val(dataAttr.title);
		form.find('[name="edit_link"]').val(dataF.attr('href'));
	}
};

var feeds = {
	editBtn : null,

	sort : function(button){
		var $button = $(button),
		menu = $button.siblings('ul:first'),
		stacksInput = $button.siblings('input');

		menu.toggleClass('show');
		if(stacksInput.hasClass('show')){
			stacksInput.removeClass('show').addClass('showUl');
		}else if(stacksInput.hasClass('showUl')){
			stacksInput.removeClass('showUl').addClass('show');
		}
		//remove click handler first otherwise we get multiple handlers each time the sort button is clicked, not ideal!
		menu.off().on('click', 'a', function(e){
			e.preventDefault();
			var $this = $(this),
			container = $button.parent('header').siblings('div');

			//removes all active classes
			function removeClass(){
				menu.find('.feedCat-1, .Graphics').removeClass('active').end().find('a').removeClass('active');
			}
			//hides all elements then displays the relevant items
			function displayFeeds(displayClass, container){
				container.find('div').hide().end().find('div'+displayClass).show();
				stacks.init();
				//trigger lazyload only if it has already been initialised
				if(lazyLoad.container !== ''){
					lazyLoad.triggerLoad(true);
				}
			}
			//different events if link clicked is already active: remove the sort
			if($this.hasClass('active')){
				removeClass();
				displayFeeds('', container);
				$button.removeClass('activeButton');
				menu.removeClass('show');
			}else{
				removeClass();
				$button.addClass('activeButton');
				if($this.attr('id') === undefined){
					displayFeeds('.'+$this.attr('class'), container);
				}else{
					displayFeeds('.'+$this.attr('id'), container);
				}
				
				$this.addClass('active');

				if($this.parent('li').hasClass('feedCat-1') || $this.parent('li').hasClass('Graphics')){
					$this.parent('li').addClass('active');
				}
			}
			
		});
	},

	edit : function(button){
		if(globals.mediaQuery.matches){
			header.mobileControl('#section_feeds', '.expose_feeds');
		}
		
		var $button = $(button),
		url = globals.url + '/feeds/editFeeds',
		content = $('div#content');

		this.editBtn = $button;

		$button.addClass('activeButton');

		globals.body.removeClass('stacksSidebar');

		content.off().load(url, function(){
			var editCont = content.find('#edit_cont');
			editCont.append(editCont.find('ul[class*="feed-"]'));
			editCont.off().on('click', 'h3 > a', function(e){
				e.preventDefault();
				editCont.find('ul[class*="feed-"]').hide().end().find('ul.'+$(this).attr('href')).show();
			});
		});
	},

	formValidate : function(input){
		var $input = $(input);
		if($input.attr('name') === 'edit_url'){
			$input.siblings('[name*="submit"]').prop('disabled', false);
		}
		$input.off().on('keyup', function(){
			var testValue = $(this).val();
			if(testValue.indexOf('http://') < 0){
				$input.siblings('[name*="submit"]').prop('disabled', true);
			}else{
				$input.siblings('[name*="submit"]').prop('disabled', false);
			}
		});
	},

	markAsRead : function(link){
		var ID = link.attr('data-id'),
		container = link.parents('.feedArticle'),
		url = globals.url + "feeds/markasRead/"+ID;

		feeds.readArticle();

		$.ajax({
			url : url,
			dataType : 'json',
			type : "GET" })
			.done(function(resp){
				if(resp.error){
					alert('There was an error marking item as read.');
				}else{
					container.addClass('markedRead');
					var feedCount = globals.globalHeader.find('#rss').find('a'),
					fCount = feedCount.attr('data-feedcount'),
					newCount = parseInt(fCount - 1);
					feedCount.attr('data-feedcount', newCount);
				}
			})
			.fail(function(){
				alert('There was an error marking item as read.');	
			});
	},

	getNew : function(button){
		//cache button, feed link in header, and get the last feed id displayed
		var $button = $(button),
		feedLink = globals.globalHeader.find("#rss").find('a'),
		url = globals.url + "feeds/lastItem",
		lastFeedID = $button.parents('header').siblings('div').find('.feedArticle:first-child').find('h3').find('a').attr('data-ID');

		//start animation
		$button.addClass('animated');
		//go to lastItem.php and check what the last item is on the server. 
		function repeatGetNew(){
			$.ajax({
				type: 'GET',
				url: url,
				dataType : 'json' 
			}).done(function(resp) {
				  	testFeed(resp);
				})
			  	.fail(function() {
				  	alert('Could not retrieve unread feed count');
				  	$button.removeClass('animated');
				});
		}
		//Compare IDs and run functions based on result
		function testFeed(resp){
			//no response - all feeds read
			if(resp.item === null){
				getLess();
			//nothing more current in db so hit the feeds 	
			}else if(resp.item === lastFeedID){
				getLess();
			//more in database so simply load them in rather than an expensive call to feeds	
			}else{
				getMore();
			}
		}
		//If there are more items in database then simply trigger a click on feed link
		function getMore(){
			feedLink.trigger('click');
		}
		//Otherwise run the index.php and refresh the feeds
		function getLess(){
			var url = globals.url + "feeds/refreshFeeds";
			
			utils.pleaseWait();

			$.ajax({
				type: 'GET',
				url: url,
				dataType : 'json' 
			}).done(function(resp){
			  	utils.success();
			  	//check for error
			  	if(resp.error){
			  		$button.removeClass('animated');
			  		alert('Something went awry :(. Msg: ' +resp.msg);
			  	//if the response is greater than current feed count then refresh 		
			  	}else if(resp.count > parseInt(feedLink.attr('data-feedcount'))){
		  			getMore();
		  			header.refreshFeedCount(false);
			  	}else{
			  		alert('No new feeds');
			  		$button.removeClass('animated');
			  	}
		  	})
		  	.fail(function(){
		  		var errorMsg = {
		  			'msg' : 'Could not retrieve unread feed count'
		  		};
		  		utils.error(errorMsg);
			  	$button.removeClass('animated');
			});
		}

		//first check if there are any new feeds in the database
		repeatGetNew();
	},

	markAllAsRead : function(button){
		var $button = $(button),
		menu = $button.siblings('#markFeeds');

		$button.toggleClass('activeButton');
		menu.toggleClass('show');

		menu.off().on('click', 'a', function(e){
			e.preventDefault();
			url = globals.url + 'feeds/markmultiRead/'+$(this).attr('data-option');
			var confirmation = confirm('Are sure?');
			if(confirmation){
				getRead(url);
			}
		});

		function getRead(url){
			$.ajax({
				url : url,
				type : "GET",
				dataType : 'json' 
			}).done(function(resp){
				if(resp.error){
					alert(resp.msg);
				}else{
					header.refreshFeedCount(false);
					globals.globalHeader.find("#rss").find('a').trigger('click');
				}
			}).fail(function(resp){
				alert('There was an error marking item as read.');
			});
		}	
	},

	readArticle : function(){
		//store iframe in variable to insert, cache content div
		var iframe = $('<iframe src="about:blank" id="contentFrame" sandbox="allow-forms allow-scripts allow-same-origin"></iframe>'),
		content = $('#content');
		//add click handler to content links
		content.off().on('click', 'a', function(e){
			e.preventDefault();
			//insert iframe into content
			content.empty().append(iframe);
			header.loadExternal($(this).attr('href'));
		});
	},

	formEntry : function(dataF, dataAttr, form){
		form.find('[name="edit_feed_ID"]').val(dataAttr.id);
		form.find('[name="edit_url"]').val(dataF.text());
	}
};

var stacks = {

	init : function(){
		//grab elements needed and run functions
		var searchStacks = $('#searchStacks'),
		$container = $('#stacksScrollContainer');

		stacks.searchProjects(searchStacks, $container);
		stacks.filterProjects('#stacks_control');
	},

	filterProjects: function(header){
		var $header = $(header);
		//needs to be switched off before on as will run many times use tsort to sort asc,desc or rand on whole list of projects
		$header.find('span').off().on('click', 'a', function(e){
			e.preventDefault();
			var ordered = $(this).attr('href');

			$header.siblings('div').find('div').tsort('h3', {
				order : ordered
			});
			
			lazyLoad.triggerLoad(true);
		});

	},

	searchProjects: function(input, container){
		//grab list of visible divs. Probably a slow and costly way to do this. This must be changed each time another event occurs so we can search a filtered list and not mess up the sort by function
		var visibleStacks = container.find('div:visible');
		//function to delay keyup so we wait until a more usable string is collected
		var delay = (function(){
		  var timer = 0;
		  return function(callback, ms){
		    clearTimeout (timer);
		    timer = setTimeout(callback, ms);
		  };
		})();
		//bind the event, add the delay and finally run the function where the real stuff happens
		input.off().keyup(function() {
		    delay(function(){
		      filterItems(input.val(), visibleStacks);
		     lazyLoad.triggerLoad(true);
		    }, 600 );
		});
		//grab search query and list of visible divs. Check for blank and show all otherwise search h3 element for string and hide anything that doesn't match
		var filterItems = function(inputVal, visibleStacks){
			if(inputVal === ' '){
				visibleStacks.each(function(){
					$(this).show();
				});
			}else{
				visibleStacks.each(function(){
					var $this = $(this);
					if ($this.find('h3').text().search(new RegExp(inputVal, "i")) < 0) {
		                $this.hide();
			        } else {
			            $this.show();
			        }
				});
			}
		};
	},

	showSearch : function(button){
		var $button = $(button),
		ul = $button.siblings('ul'),
		input = $button.siblings('input');

		if(globals.body.hasClass('stacksSidebar')){
			if(ul.hasClass('show')){
				if(input.hasClass('showUl')){
					input.removeClass('show').removeClass('showUl');
				}else{
					input.removeClass('show').addClass('showUl');
				}
			}else{
				if(input.hasClass('show')){
					input.removeClass('showUl').removeClass('show');
				}else{
					input.removeClass('showUl').addClass('show');
				}
				
			}
			$button.toggleClass('activeButton');
		}
	},

	formEntry : function(dataF, dataAttr, form){
		form.find('[name="edit_model_ID"]').val(dataAttr.id);
		form.find('[name="edit_model_title"]').val(dataAttr.title);
	},

	guides : function($container){
		if($container.length){
			$container.on({
				mouseenter : function(){
					$imgsrc = $(this).attr('data-img');
					$container.siblings('div').find('.imgOverlay').attr('src', $imgsrc).fadeIn();
				},

				mouseleave : function(){
					$container.siblings('div').find('.imgOverlay').hide();
				}
			}, 'li');
		}
		
	}
};

var projects = {
	//stores modeId when opening a project(variable sent from php page)
	modelID : null,
	//when submiting form we need to remove the elements from the DOM upon sucess. This is handled in another function we store them here until ready to be removed.
	mvFileList : null,
	noteOrig : null,

	//seperate function for showing project forms - too much work to get the other form function to work
	showForm : function(button){
		var $button = $(button),
		spacePos = $button.attr('class').indexOf(" ");
		if(spacePos > 0){
			classBtn = $button.attr('class').substr(0, spacePos);	
		}else{
			classBtn = $button.attr('class');
		}
		
		$button.toggleClass('activeButton');

		$('#section_cat_content').find('#'+classBtn).toggleClass('show');
	},
	//seperate show form requires seperate remove
	removeForm : function(button){
		var $button = $(button),
		btnClass = $button.parent('form').attr('id');

		$('#section_cat_content').find('button.'+btnClass).trigger('click');	
	},

	loadContent : function(container){
		//kill the slideshow which will persist if the cancel button isn't triggered
		mediaView.slideShowOff($('#slideShowButton'));

		var $container = $(container),
		$mediaContainer = $container.find('#media');
		//setup the mediaView variables when loading projects
		mediaView.init($mediaContainer);

		$container.off().on('click', 'a.onlyThis', function(e){
			e.preventDefault();
			var $this = $(this),
			link = $this.attr('href'),
			holder = $this.parent('div'),
			idFlag = holder.attr('id').substr(-3),
			fav = $this.attr('data-fav'),
			favID = $this.attr('data-id');
			mediaView.show(link, holder, idFlag, fav, favID);
		});
	},

	triggerSubmit : function(form){
		//delete form is hidden but uses the same logic as move form so we simply trigger the submit button in the form that in turns runs formData()
		if(confirm('Are you sure?')){
			globals.body.find('#'+form).find('input[type="submit"]').trigger('click');
		}
		
	},

	sorting : function(){
		var pics = globals.body.find('#model_pictures');

		globals.body.find('#models_header').find('span').off().on('click', 'a', function(e){
			e.preventDefault();

			var $this = $(this),
			sortingCriteria = $this.attr('href');

			function sort(container){
				container.find('div').tsort({
					attr: 'id',
					order: sortingCriteria
				});
				lazyLoad.triggerLoad(false);
			}

			sort(pics);
		});
	},

	editMode : function(button){
		var $button = $(button);
		$button.toggleClass('activeButton');
		//grab buttons and enable disabled buttons
		var btn = $button.siblings('button'),
		btnLength = btn.length;
		disenableBtn();

		function disenableBtn(){
			if($button.hasClass('activeButton')){
				for(var i=1; i <= 4; i++){
					$(btn[btnLength -i]).prop('disabled', false);
				}
			}else{
				for(var j=1; j <= 4; j++){
					$(btn[btnLength -j]).prop('disabled', true);
				}
			}
		}
		//change the click handler if in edit mode or return the click handler back to normal state
		if($button.hasClass('activeButton')){
			projects.editContent('#section_models');
		}else{
			//remove active class from links if done editing
			$button.parents('#section_cat_content').find('.activeTemp').removeClass('activeTemp');
			projects.loadContent('#section_models');
		}
	},

	editContent : function(container){
		var $container = $(container);

		$container.off().on('click', 'a.onlyThis', function(e){
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass('activeTemp');
		});
	},

	select : function(button){
		var $button = $(button),
		$parentContainer = $button.parents('#section_cat_content'),
		$imgContainer = $parentContainer.find('#model_pictures');

		$button.toggleClass('activeButton');

		function imgOrVid(flag){
			if(flag){
				$imgContainer.find('.onlyThis').addClass('activeTemp');
			}else{
				$imgContainer.find('.onlyThis').removeClass('activeTemp');
			}
		}

		if($button.hasClass('activeButton')){
			imgOrVid(true);
		}else{
			imgOrVid(false);
		}
	},

	formIDs : function(){
		$('#section_cat_content').find('#add_file_models').find('[name="add_model_ID"]').val(this.modelID);
	},

	formData : function(form){
		//we need to fill out the form using selected DOM elements frst then wrap this up in a FormData object
		
		var $parentContainer = $('#section_cat_content'),
		$imgContainer = $parentContainer.find('#model_pictures');

		$cont = $imgContainer;

		$selectedItems = $cont.find('.activeTemp').map(function(){
			  return $(this).attr('data-id');
			}).get();

		projects.mvFileList = $cont.find('.activeTemp').parent();

		form.find('[name="IDArray"]').val($selectedItems);

		data = new FormData(document.forms.namedItem(form.attr('name')));

		return data;
	},

	noteEdit : function($cont){
		$cont.on('dblclick click', 'article', function(e){
			e.preventDefault();
			var $this = $(this);

			if(e.type != 'click'){
				//get markdown text from server
				$.get(globals.url+'stacks/getNotes/'+projects.modelID+'/', function(resp){
					projects.noteReplace($this, resp);
				});
			}
		});
	},

	noteReplace : function($cont, $md){

		projects.noteOrig = $cont;
		//replace content with textarea and markdown(raw)
		$cont.replaceWith("<form id='noteedit' name='noteedit' action='"+globals.url+"stacks/editNotes' method='POST'><textarea id='md_stuff' class='mousetrap' name='noteval' placeholder='Enter note…'>"+$md+"</textarea><input type='hidden' name='edit_note_ID' value='"+projects.modelID+"'><button class='md_cancel' onclick='projects.noteCancel(event)'>Cancel</button><button class='md_submit'onclick='projects.noteSubmit()'>Submit</button></form>");

		Mousetrap.bind('tab', function(){ 
			var textArea = $('textarea.mousetrap'),
			start = textArea.get(0).selectionStart;
      		textArea.val(textArea.val().substring(0, start) + "\t" + textArea.val().substring(textArea.get(0).selectionEnd));
      		textArea.get(0).selectionStart = textArea.get(0).selectionEnd = start + 1;
      		return false;
		});
	},

	noteSubmit : function(){
		var $form = $('#noteedit');

		$form.submit(function(e){
			e.preventDefault();
			var $this = $(this),
			$data = new FormData(document.forms.namedItem($this.attr('name')));
			formSubmission.postForms($this.attr('action'), $data);
		});
	},

	noteCancel : function(e){
		e.preventDefault();
		$('#noteedit').replaceWith(projects.noteOrig);
	},

	noteFinish : function(note){
		$('#noteedit').replaceWith("<article class='noteTxt'>"+note+"</article>");
	},

	mdExpand : function(btn){
		var $btn = $(btn),
		$parent = $btn.parent();
		$parent.toggleClass('mdExpand');
	},

	mdSyntax : function(btn){
		var $btn = $(btn), 
		$div = $('<div id="noteSyntax"></div>');

		if($btn.hasClass('active')){
			$('body').find('#noteSyntax').remove();
			$btn.removeClass('active');
		}else{
			$btn.addClass('active');
			//load into the loading div a syntax sheet or a sidebar or something
			$div.load(globals.url + 'stacks/noteSyntax', function(){
				$('body').append($div);
			});
		}		
	}

};

var crop = {
	container : '',
	newImage : '',

	init: function(button){
		crop.container = $('#thumbs');
		//grab all selected files and send to crop.php
		var $button = $(button),
		$container = $button.parents('#section_cat_content'),
		picturesToCrop = $container.find('#model_pictures').find('.activeTemp'),
		thumbArray = [],
		picturesArray = [];
		//check if anything was selected
		if(picturesToCrop.length > 0){
			picturesToCrop.each(function(){
				var $this = $(this),
				thumb = $this.find('img').attr('src'),
				picture = $this.attr('href');
				//grab thumbs for display purposes
				thumbArray.push(thumb);
				//grab actual pics for crop purposes
				picturesArray.push(picture);
			});
			//create and append crop div			
			$.ajax({
				url : globals.url + 'stacks/crop',
				dataType: 'html',
				type : 'POST',
				data: {'thumbArr' : thumbArray, 'imgArr' : picturesArray, 'id' : projects.modelID}
			}).done(function(resp){
				crop.container.html(resp).show().parent().fadeIn();
			});
		}else{
			alert('Please select at least one photo to crop');
		}		
	},

	selectImage: function(list){
		//get image from list and initialise crop object
		var $list = crop.container.find(list);

		$list.on('click', 'a', function(e){
			e.preventDefault();
			var mainImage = $('#jcrop'),
			jcrop_api,
			width = parseInt($('.main_crop_area').width());
			$list.addClass('dim');

			mainImage.attr('src', $(this).attr('href')).one('load', function(){
				cropDelayed(width);
			});

			function cropDelayed(width){
				mainImage.Jcrop({
					boxWidth: width,
					onSelect: crop.showCoords,
			        bgColor:     'black',
		            bgOpacity:   0.4,
		            keySupport: false,
		            aspectRatio: 200 / 250
				}, function(){
					jcrop_api = this;
					//setup cancel crop
					crop.removeCrop(jcrop_api, $list);
				});
			}
		});
	},

	showCoords: function(c){
		var form = $('#hidden_crop_form'),
		imageSrc = $('#jcrop').attr('src');
		form.find('#x_cor').val(c.x);
		form.find('#y_cor').val(c.y);
		form.find('#w_cor').val(c.w);
		form.find('#h_cor').val(c.h);
		form.find('#image_cor').val(imageSrc);
	},

	removeCrop: function(cropObject, list){
		var form = crop.container.find('#hidden_crop_form'),
		$jcrop = crop.container.find('#jcrop');

		crop.container.find('#cancel_crop').show().off().on('click', function(){
			cropObject.destroy();
			list.removeClass('dim');
			$jcrop.attr('style', '').attr('src', '');	
			$(this).hide().siblings('button').hide();
		});

		crop.container.find('#submit_crop').show().off().on('click', function(){
			list.removeClass('dim');
			$(this).hide().siblings('button').hide();
			cropObject.destroy();
			$jcrop.attr('style', '').attr('src', '');
			form.find('input[type="submit"]').trigger('click');
		});
	},

	submitForm: function(form){
		form.submit(function(e){
			e.preventDefault();
			var $this = $(this),
			data = new FormData(document.forms.namedItem($this.attr('name')));

			$.ajax({
				url : $this.attr('action'),
				data : data,
				type : 'POST',
				processData: false,
  				contentType: false,
  				dataType : 'json'
			}).done(function(resp){

				if(!resp.error){
					crop.reviewSteps(resp.value);
				}else if(resp.error){
					if(resp.replace){
						var $img = $('#stacksScrollContainer').find('a[data-id="'+projects.modelID+'"] > img');
						setTimeout(function(){
							$img.attr('src' , resp.img);
						}, 400);
					}
					crop.close();
				}
			}).fail(function(resp){
				alert(resp.responseText);
			});
		});
		
	},

	reviewSteps: function(newImage){
		var holder = crop.container.find('#crop_comparrison');
		holder.find('#new_image').attr('src', newImage);
		holder.fadeIn();
	},

	finalise: function(button){
		var $button = $(button),
		$form = $button.siblings('form');
		$form.find('input[type="submit"]').trigger('click');
	},

	close: function(){
		crop.container.hide();
		crop.container.parent().fadeOut();

		setTimeout(function(){
			crop.container.empty().hide();
		}, 1000);
	},

	closeFinal : function(){
		crop.container.find('#crop_comparrison').fadeOut();
	}
};

var temp = {

	tmpNonImg : $('<div></div><p>Non-image selected</p>'),
	tmpNonSelect : $('<div></div><p>Nothing Selected</p>'),
	mvFileList : null,

	selectFiles: function(){
		//select and store DOM elements then clone and change. These will get destroyed hence the cloning. 
		var contentCont = globals.body.find('#tempMessageCont');

		globals.body.find('#file_list').off().on('click', 'a', function(e){
			e.preventDefault();

			//cache link and data attributes
			var $this = $(this),
			dataOriginal = $this.find('img').attr('data-original'),
			notFile = $this.find('span').find('img').attr('data-notfile');

			$this.toggleClass('activeTemp');
			//get file location and list of currently selected files
			var tempImage = '<img src="'+decodeURIComponent(dataOriginal)+'">',
			fileList = globals.body.find('#file_list').find('a.activeTemp');

			//check if anything is selected then check if it isn't a file and react accordingly
			if(fileList.length > 0){
				if(!$this.hasClass('activeTemp')){
					//don't add image if already selected
				}
				else if(notFile === 'true'){
					contentCont.html(temp.tmpNonImg);
				}else{
					contentCont.html(tempImage);
				}
			}else{
				contentCont.html(temp.tmpNonSelect);
			}
			
		});
	},

	tempFunction: function(moveOrDelete){
		var fileList = globals.body.find('#file_list').find('a.activeTemp'),
		fileArray = [],
		postString = moveOrDelete,
		ProjectID = globals.body.find('#dir_list').find('select').find('option:selected').val();
		this.mvFileList = fileList.parent();

		//create array to pass as post data to php file
		fileList.each(function(){
			fileArray.push(decodeURIComponent($(this).find('img').data('original')));
		});

		//construct form data to post
		var data = new FormData();
		data.append("ProjectID", ProjectID);
		data.append("fileArray", fileArray);

		//check if we're actually sending anything and then post
		if(fileArray.length){
			var go, url;
		    //double check deletion is wanted
		    if(postString === 'delete'){
		    	go = confirm('Are you sure you want to delete selected files? Regrets?');
		    	url = globals.url + 'stacks/deleteTemp';
		    }else if(postString === 'move'){
		    	go = true;
		    	url = globals.url + 'stacks/addTemp';
		    }
		    
		    if(go){
		    	formSubmission.postForms(url, data);
		    }
		//otherwise throw an error	
		}else {
			alert('Please select at least one file!');
		}
	},

	tempRemoveFiles : function(flag){
		//addclass to hide files, wait, then remove from the DOM and empty the file list
		function wait(){
			flag.mvFileList.remove();	
			flag.mvFileList = null;
			if(typeof(flag.tempMessage) !== "undefined"){
				flag.tempMessage.find('p').text(flag.tmpNonSelect);
				globals.body.find('#tempMessageCont').html(flag.tempMessage);
			}
		}

		if(flag.mvFileList !== null){
			flag.mvFileList.addClass('bookmark_remove');
			setTimeout(wait, 700);
		}	
	}

};

var wallpapers = {
	init: function(){
		var wallPapers = globals.body.find('#wallpapers'),
		wallPapersControl = wallPapers.find('#wallpaperControl'),
		backgroundImg = $('#bg').find('img');

		wallPapers.toggleClass('active');
		if(wallPapersControl.length){
			wallPapersControl.remove();
		}else{
			$.get(globals.url+'stacks/favs', function(resp){
				wallPapers.append(resp);
				$('#wallpaperControl').on('click', 'img', function(){
					var imgSrc = $(this).data('original');
					backgroundImg.attr('src', imgSrc);
					$.post(globals.url+'stacks/setWallpaperSession', {'wallpaper' : imgSrc});
				});
			});
			
		}
	},

	remove: function(button, ID){
		$button = $(button);

		function wait(){
			$button.parent().remove();
		}

		$.ajax({
			url : globals.url + 'stacks/favRemove',
			dataType : 'json',
			type : 'POST',
			data : {'PhotoID' : ID}
		}).done(function(resp){
			if(resp.error){
				alert('Error!');
			}else{
				$button.parent().addClass('bookmark_remove');
				setTimeout(wait, 700);
			}
		});
	},

	fontSelection: function(){
		globals.body.find('#wallpapers').off().on('click', 'p.font', function(){
			var fontClass = $(this).text().toLowerCase();

			if(fontClass == 'avenir'){
				globals.body.removeClass();
				$.get(globals.url+'login/style/0');
			}else{
				globals.body.removeClass().addClass(fontClass);
				$.get(globals.url+'login/style/1');
			}
		});
	}
};

var mediaView = {

	holder : null,
	slideshow : null,
	img : null,
	header : null,
	picControls : null,
	linkedDiv : null,
	favButton : null,

	init: function(container){
		//set fields of object used for other functions
		this.holder = container.find('.holder');
		this.header = container.find('#mediaControlBar');
		this.img = this.holder.find('img');
		this.picControls = this.header.find('#pictureControls');
		this.favButton = this.header.find('.fav');
	},

	show: function(link, div, flag, fav, favID){		

		if(this.header.parent().hasClass('hide')){
			this.header.parent().removeClass('hide');
		}
		this.linkedDiv = div;
		if(fav === 'true'){
			this.favButton.addClass('activeButton').attr('data-id', favID);
		}else{
			this.favButton.removeClass('activeButton').attr('data-id', favID);
		}

		function delayPicLoading(){
			mediaView.img.attr('src', link).attr('data-id', favID).fadeIn();
			mediaView.keyBoardControls();
		}
		//hide/display elements based on vids or pics and show container
		if(flag === 'pic'){
			this.picControls.show().css("display", "inline-block");
			this.img.fadeOut();
			setTimeout(delayPicLoading, 400);
		}

		this.header.parent().fadeIn();
	},

	hide: function(button){
		$button = $(button);
		$button.toggleClass('flip');
		$button.parents('#media').toggleClass('hide');
	},

	close: function(button){
		var $button = $(button),
		$container = $button.parents('div#media');
		//stop slideshows/videos playing
		mediaView.slideShowOff($container.find('button#slideShowButton'));
		//kill all content
		this.img.attr('src', '');
		
		$container.fadeOut();

		//remove keyboard short cuts from window
		$(window).off('keydown');
	},

	back: function(){
		//grab prev div to one that was clicked and trigger otherwise get last div
		if(this.linkedDiv.prev().length){
			this.linkedDiv.prev().find('a').eq(0).trigger('click');
		}else{
			this.linkedDiv.siblings().eq(-1).find('a').eq(0).trigger('click');
		}
	},

	forward: function(){

		if(mediaView.linkedDiv.next().length){
			mediaView.linkedDiv.next().find('a').eq(0).trigger('click');	
		}else{
			mediaView.linkedDiv.siblings().eq(0).find('a').eq(0).trigger('click');
		}
	},

	slideShow: function(button){
		$(button).attr('onclick', 'mediaView.slideShowOff(this)').addClass('slideShowOn');
		mediaView.slideshow = setInterval(mediaView.forward, 5000);
	},

	slideShowOff: function(button){
		$(button).removeClass('slideShowOn').attr('onclick', 'mediaView.slideShow(this)');
		clearInterval(mediaView.slideshow);
	},

	imageOriginal: function(){
		this.img.removeClass('fullWidth').addClass('original');
	},

	imageFullWidth: function(){
		this.img.removeClass('original').addClass('fullWidth');
	},

	imageToFit: function(){
		this.img.removeClass('original').removeClass('fullWidth');
	},

	favourites : function(button){
		var $button = $(button),
		photoID = $button.attr('data-id'),
		divContID = '#'+photoID+"-pic";
		divCont = $('#model_pictures').find(divContID);

		if($button.hasClass('activeButton')){
			$.ajax({
				url : globals.url + 'stacks/favRemove',
				data : {'PhotoID' : photoID},
				type : 'POST',
				dataType : 'json'
			}).done(function(resp){
				if(!resp.error){
					$button.removeClass('activeButton');
					divCont.find('a').attr('data-fav', false);
				}
			});
		}else{
			$.ajax({
				url : globals.url + 'stacks/favAdd',
				data : {'PhotoID' : photoID},
				type : 'POST',
				dataType : 'json'
			}).done(function(resp){
				if(!resp.error){
					$button.addClass('activeButton');
					divCont.find('a').attr('data-fav', true);
				}
			});
		}
	},

	trash: function(){
		//too much pain to reuse code so we just write it out here - bleh
		//get form, get ID to be deleted
		var $form = $('#file_delete_projects'),
		mediaID = mediaView.img.attr('data-id');

		$form.find('[name="IDArray"]').val(mediaID);
		var fileParent = $('#section_cat_content').find('#'+mediaID+'-pic'),
		data = new FormData($form.get(0));

		if(confirm('Are you sure?')){
			utils.pleaseWait();

			$.ajax({
				url : $form.attr('action'),
				type : 'POST',
				data : data,
				dataType : 'json',
				processData: false,
	  			contentType: false
	  		}).done(function(resp){

				function wait(){
					fileParent.remove();
				}

				if(resp.error){
					utils.error(resp);
					setTimeout(wait, 600);
				}else{
					utils.success();
					fileParent.addClass('bookmark_remove');
					mediaView.forward();
					setTimeout(wait, 600);
				}
			});	
		}
	},

	keyBoardControls: function(){
		Mousetrap.bind('space', function(){
			if($('#media').is(':visible')){
				mediaView.header.find('#slideShowButton').trigger('click');
			}
		});

		Mousetrap.bind('left', function(){ 
			if($('#media').is(':visible')){
				mediaView.header.find('.prevPic').trigger('click');
			}
		});

		Mousetrap.bind('up', function(){
			if($('#media').is(':visible')){ 
				mediaView.header.find('.imageToFit').trigger('click');
			}
		});

		Mousetrap.bind('right', function(){
			if($('#media').is(':visible')){ 
				mediaView.header.find('.nextPic').trigger('click');
			}
		});

		Mousetrap.bind('down', function(){ 
			if($('#media').is(':visible')){
				mediaView.header.find('.imageFullWidth').trigger('click');
			}
		});

		Mousetrap.bind('f', function(){ 
			if($('#media').is(':visible')){
				mediaView.header.find('.fav').trigger('click');
			}
		});

		Mousetrap.bind('x', function(){ 
			if($('#media').is(':visible')){
				mediaView.header.find('.mediaClose').trigger('click');
			}
		});
	} 
};

//simple scrollTo plugin via Timothy Perez - http://lions-mark.com/jquery/scrollTo/
$.fn.scrollTo = function( target, options, callback ){
  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
  var settings = $.extend({
    scrollTarget  : target,
    offsetTop     : 50,
    duration      : 500,
    easing        : 'swing'
  }, options);
  return this.each(function(){
    var scrollPane = $(this);
    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
      if (typeof callback == 'function') { callback.call(this); }
    });
  });
};








