<?php
class StacksModel extends MasterModel{

	public function __construct(Database $db){
		parent::__construct($db);
	}

	public function addProject($title, $category){
		$sql = "SELECT Title FROM Projects WHERE Title = :Title AND user_id = :user_id";
		$dataArr = array('Title' => $title, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);

		if(!$result){
			$sql = "INSERT INTO Projects(CatID, Title, user_id) Values(:CatID, :Title, :user_id)";
			$dataArr = array('CatID' => $category, 'Title' => $title, 'user_id' => $_SESSION['user_id']);
			$result = $this->commitDb($sql, $dataArr, false);

			return true;
		}else{
			return false;
		}
	}

	public function editProject($title, $cat, $id){
		$sql = "UPDATE Projects SET CatID = :CatID, Title = :Title WHERE ID = :ID AND user_id = :user_id";

		$dataArr = array('CatID' => $cat, 'Title' => $title, 'ID' => $id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, false);

		if($result){
			return true;
		}

		return false; 
	}

	public function deleteProject($id){
		$sql = "SELECT ID FROM Images WHERE ProjectID = :ProjectID AND user_id = :user_id";
		$dataArr = array('ProjectID' => $id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);

		if($result){
			$photoStr = '';
			foreach($result as $ids){
				$photoStr .= $ids->ID . ',';
			}

			if(strlen($photoStr) > 1){
				$this->deleteImages($photoStr);
			}
		}

		$dir = $this->cropFolder()[0]->user_creation_timestamp.'/';

		if(is_file(NON_HTTP_PATH.THUMBDIR.$dir.$id."-thumb.jpg")){
			unlink(NON_HTTP_PATH.THUMBDIR.$dir.$id."-thumb.jpg");
		}

		$sql = "DELETE FROM Projects WHERE ID = :ID AND user_id = :user_id";
		$dataArr = array('ID' => $id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, false);

		return $result;
	}

	public function getProjects(){
		$sql = "SELECT Categories.Title as catTitle, Projects.Title, Projects.ID, Projects.CatID FROM Categories INNER JOIN Projects on Categories.ID = Projects.CatID WHERE Projects.user_id = :user_id ORDER BY Projects.Title ASC ";
		$dataArr = array('user_id' => $_SESSION['user_id']);
		return $this->commitDb($sql, $dataArr, true);
	}

	public function getCategories(){
		$sql = "SELECT ID, Title FROM Categories";
		$result =  $this->db->query($sql);
		return $result->fetchAll(); 
	}

	public function getFavourites(){
		$sql = "SELECT Images.IMGname, Favourites.PhotoID FROM Images INNER JOIN Favourites ON Images.ID = Favourites.PhotoID WHERE Favourites.user_id = :user_id";
		$dataArr = array('user_id' => $_SESSION['user_id']);
		return $this->commitDb($sql, $dataArr, true);
	}

	public function favouritesAdd($id){
		$sql = "INSERT INTO Favourites(PhotoID, user_id) VALUES(:PhotoID, :user_id)";
		$dataArr = array('PhotoID' => $id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, false);
		return $result;
	}

	public function favouritesRemove($id){
		$sql = "DELETE FROM Favourites WHERE PhotoID = :PhotoID AND user_id = :user_id";
		$dataArr = array('PhotoID' => $id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, false);
		return $result;
	}

	public function addImages($files, $projectID){
		$imgExts = array('jpeg', 'jpg', 'gif', 'png', 'tiff');

		foreach($files as $key=>$value){
	    	foreach($value as $key2=>$value2){
	    		$newFileArray[$key2][$key] = $value2;
	    	}
	    }

	    foreach($newFileArray as $file){
	    	$fileName = pathinfo($file['name']);
			$fileExt = strtolower($fileName['extension']);

			if(in_array($fileExt, $imgExts)){
				$result = $this->addFile($file, $fileExt);
				if($result[0]){
					$sql = "INSERT INTO Images(ProjectID, IMGname, user_id) VALUES(:ProjectID, :IMGname, :user_id)";
					$dataArr = array('ProjectID' => $projectID, 'IMGname' => $result[1], 'user_id' => $_SESSION['user_id']);
					$result = $this->commitDb($sql, $dataArr, false);
					if(!$result){
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
	    }

	    return true;
	}

	private function addFile($file, $fileExt){
		$finalname = locatGen::nameGen($file['tmp_name'], $fileExt);
		$location = locatGen::gen($finalname);
		$thumbLocation = locatGen::thumbLocation($finalname);

		//make directories
		locatGen::createDir($location);	

		if(move_uploaded_file($file['tmp_name'], NON_HTTP_PATH.CONTENT_DIR.$location.$finalname)){
			$image = WideImage::load(NON_HTTP_PATH.CONTENT_DIR.$location.$finalname)->resize(THUMB_WIDTH, THUMB_HEIGHT, 'outside')->crop('center', 'center', THUMB_WIDTH, THUMB_HEIGHT)->saveToFile(NON_HTTP_PATH.CONTENT_DIR.$location.$thumbLocation);
			return array(true, $finalname);
		}else{
			return array(false);
		}
	}

	public function deleteImages($imageArr){

		$imgs = explode(',' , $imageArr);

		foreach($imgs as $delete){
			$sess = $_SESSION['user_id'];
			$sql = "SELECT ID, ProjectID, IMGname FROM Images WHERE ID = '$delete' AND user_id = '$sess'";
			
			foreach($this->db->query($sql) as $file){

				$sql = "SELECT Count(*) FROM Images WHERE IMGname = '$file->IMGname'";
				$result = $this->db->query($sql);
				$test = $result->fetch(PDO::FETCH_NUM)[0];

				if($test == 1){
					$link = NON_HTTP_PATH.CONTENT_DIR.locatGen::gen($file->IMGname);
					$mainFile = $link.$file->IMGname;
					$thumb = $link.locatGen::thumbLocation($file->IMGname);
					if(is_file($mainFile)){
						unlink($mainFile);
						unlink($thumb);
						//remove folders if empty
						$isDirEmpty = !(new \FilesystemIterator($link."thumb"))->valid();
						if($isDirEmpty == 1){
							if(is_file($link.'.DS_Store')){
								unlink($link.'.DS_Store');
							}
							rmdir($link."thumb");
							rmdir($link);
						}
					}
				}
				
				$sql_delete = "DELETE FROM Images WHERE ID = :ID AND user_id = :user_id";
				$dataArr = array('ID' => $delete, 'user_id' => $_SESSION['user_id']);

				$result_one = $this->commitDb($sql_delete, $dataArr, false);

				$sql_removeFav = "DELETE FROM Favourites WHERE PhotoID = :PhotoID AND user_id = :user_id";
				$dataArr = array('PhotoID' => $delete, 'user_id' => $_SESSION['user_id']);

				$result_two = $this->commitDb($sql_removeFav, $dataArr, false);

				if(!$result_one || !$result_two){
					return false;
				}
			}
		}
		return true;
	}

	public function editImages($moveID, $imgIDs){
		$imgArr = explode(',', $imgIDs);
		foreach($imgArr as $ids){
			$sql = "UPDATE Images SET ProjectID = :ProjectID WHERE ID = :ID AND user_id = :user_id";
			$dataArr = array('ProjectID' => $moveID, 'ID' => $ids, 'user_id' => $_SESSION['user_id']);
			$result = $this->commitDb($sql, $dataArr, false);

			if(!$result){
				return false;
			}
		}

		return true;
	}

	public function getImages($id){
		$sql = "SELECT IMGname, ID, ProjectID From Images WHERE ProjectID = :ProjectID AND user_id = :user_id ORDER BY IMGname ASC";
		$dataArr = array('ProjectID' => $id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);
		return $result;
	}

	public function getNotes($id){
		$sql = "SELECT notes, Projects.Title, Categories.Title as catTitle FROM Projects INNER JOIN Categories ON Projects.CatID = Categories.ID WHERE Projects.ID = :ID AND user_id = :user_id"; 
		$dataArr = array('ID' => $id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);
		return $result;

	}

	public function editNotes($id, $note){
		$sql = "UPDATE Projects SET notes = :notes WHERE ID = :ID AND user_id = :user_id";
		$dataArr = array('ID' => $id, 'notes' => $note, 'user_id' => $_SESSION['user_id']);
		return $result = $this->commitDb($sql, $dataArr, false);
	}

	public function cropFolder(){
		$sql = "SELECT user_creation_timestamp FROM users WHERE user_id = :user_id";
		$dataArr = array('user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);

		return $result;
	}

	public function cropImg($x, $y, $w, $h, $curImg){
		$dir = $this->cropFolder()[0]->user_creation_timestamp.'/';

		if(!is_dir(NON_HTTP_PATH.THUMBDIR.$dir)){
			$oldmask = umask(0);
			mkdir(NON_HTTP_PATH.THUMBDIR.$dir, 0777, true);
			umask($oldmask);
		}

		$image = WideImage::load(NON_HTTP_PATH.$curImg)->crop($x, $y, $w, $h)->resize(THUMB_WIDTH, THUMB_HEIGHT, 'outside')->saveToFile(NON_HTTP_PATH.THUMBDIR.$dir.'new_thumb.jpg');
		return array(true, URL.THUMBDIR.$dir.'new_thumb.jpg?m='.filemtime(NON_HTTP_PATH.THUMBDIR.$dir.'new_thumb.jpg'));
	}

	public function cropReview($id){
		$dir = $this->cropFolder()[0]->user_creation_timestamp.'/';

		$currentImg = NON_HTTP_PATH.THUMBDIR.$dir.$id.'-thumb.jpg';
		$newImg = NON_HTTP_PATH.THUMBDIR.$dir.'new_thumb.jpg';

		if(is_file($currentImg) || is_file($newImg)){
	    	rename($newImg, $currentImg);
	    }

	    return array(true, URL.THUMBDIR.$dir.$id.'-thumb.jpg?m='.filemtime($currentImg));
	}

	public function temp(){
		$data = array();
		$forbiddenArray = array('.DS_Store', 'category.txt', urldecode('Icon%0D'));
		$allowableExt = array('jpg', 'jpeg', 'gif', 'bmp', 'png', 'svg', 'tiff', 'webp');

		$dir = $this->cropFolder()[0]->user_creation_timestamp.'/';
		$segment_1 = substr($dir, 0, 4);
		$segment_2 = substr($dir, 4);
		$id = $_SESSION['user_id'];
		$siteSeg = "designstacks";
		$final = $siteSeg . '-' .$segment_1 . $id . $segment_2;

		if(!is_dir(NON_HTTP_PATH.TEMP_DIR.$final)){
			$oldmask = umask(0);
			mkdir(NON_HTTP_PATH.TEMP_DIR.$final, 0777, true);
			umask($oldmask);
		}

		$directory = NON_HTTP_PATH.TEMP_DIR.$final;
		$displayPath = URL.TEMP_DIR.$final;

		foreach(new DirectoryIterator($directory) as $file_or_folder){
			if($file_or_folder->isFile()){
				$fileExt = strtolower($file_or_folder->getExtension());
				if(!in_array($file_or_folder->getBasename(), $forbiddenArray)){
					if(in_array($fileExt, $allowableExt)){
						$data['files'][] = array('filePath' => $file_or_folder->getFilename(), 'fileExtension' => $fileExt, 'fileAllowable' => true);
					}else{
						$data['files'][] = array('filePath' => $file_or_folder->getFilename(), 'fileExtension' => $fileExt, 'fileAllowable' => false);
					}
				}
			}
		}
		return array(true, $data, $displayPath);
	}

	public function deleteTemp($fileArr){
		$deleteArr = explode(',', $fileArr);

		foreach($deleteArr as $remove){
			$img = explode('/', $remove);

			$removeFullPath = NON_HTTP_PATH.$img[3].'/'.$img[4].'/'.$img[5];
			//check if file exists. If not return error and exit
			if(file_exists($removeFullPath)){
				if(unlink($removeFullPath)){}
			}
		}

		return array(true, $removeFullPath);
	}

	public function addTemp($fileArr, $id){
		$pictureFileTypes = ['jpeg', 'jpg', 'gif', 'png'];
		$moveArr = explode(',', $fileArr);
		foreach($moveArr as $moving){
			$fileExt = pathinfo($moving, PATHINFO_EXTENSION);
			if(!in_array($fileExt, $pictureFileTypes)){
				$fileTypeFlag = false;
			}else{
				$fileTypeFlag = true;
			}

			if($fileTypeFlag){
				$img = explode('/', $moving);
				$newName = locatGen::nameGen(NON_HTTP_PATH.$img[3].'/'.$img[4].'/'.$img[5], $fileExt);
				$location = locatGen::gen($newName);
				$fullPathThumb = NON_HTTP_PATH.CONTENT_DIR.$location.locatGen::thumbLocation($newName);
				$fullPathIn = NON_HTTP_PATH.$img[3].'/'.$img[4].'/'.$img[5];
				$fullPathOut = NON_HTTP_PATH.CONTENT_DIR.$location.$newName;

				locatGen::createDir($location);

				if(copy($fullPathIn, $fullPathOut)){
					unlink($fullPathIn);
					$image = WideImage::load($fullPathOut)->resize(THUMB_WIDTH, THUMB_HEIGHT, 'outside')->crop('center', 'center', THUMB_WIDTH, THUMB_HEIGHT)->saveToFile($fullPathThumb);
				}

				$sql = "INSERT INTO Images(ProjectID, IMGname, user_id) VALUES(:ProjectID, :IMGname, :user_id)";
				$dataArr = array('ProjectID' => $id, 'IMGname' => $newName, 'user_id' => $_SESSION['user_id']);
				$result = $this->commitDb($sql, $dataArr, false);
			}
		}

		return true;
	}

	public function setWallpaper($url){
		$sql = "UPDATE users SET user_wallpaper = :user_wallpaper WHERE user_id = :user_id";
		$dataArr = array('user_wallpaper' => $url, 'user_id' => Session::get('user_id'));
		$result = $this->commitDb($sql, $dataArr, false);

		SESSION::set('user_wallpaper', $url);
		return $result;
	}
}

?>