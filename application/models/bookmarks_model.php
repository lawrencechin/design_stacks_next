<?php

class BookmarksModel extends MasterModel{

	public function __construct(Database $db){
		parent::__construct($db);
	}

	public function addBookmark($bk_title, $bk_url, $bk_classid){
		$sql = "INSERT INTO Bookmarks(Title, URL, ClassID, user_id) VALUES (:Title, :URL, :ClassID, :user_id)";

		$dataArr = array(
			'Title'=> $bk_title, 
			'URL'=> $bk_url, 
			'ClassID'=> $bk_classid,
			'user_id' => $_SESSION['user_id']
			);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function removeBookmark($bk_id){
		$sql = "DELETE FROM Bookmarks WHERE ID = :ID AND user_id = :user_id";
		$dataArr = array(
			'ID' => $bk_id,
			'user_id' => $_SESSION['user_id']
			);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function editBookmark($bk_id, $bk_title, $bk_url, $bk_classid){
		$sql = "UPDATE Bookmarks SET Title = :Title, URL = :URL, ClassID = :ClassID WHERE ID = :ID AND user_id = :user_id";
		$dataArr = array(
			'Title'=> $bk_title, 
			'URL'=> $bk_url, 
			'ClassID'=> $bk_classid,
			'ID' => $bk_id,
			'user_id' => $_SESSION['user_id']
			);
		return $this->commitDb($sql, $dataArr, false);
	}

	public function getBookmarks(){
		$sql = "SELECT Title, URL, ClassID, ID From Bookmarks WHERE user_id = :user_id";
		$dataArr = array('user_id' => $_SESSION['user_id']);

		return $this->commitDb($sql, $dataArr, true);
	}

	public function bookCategories(){
		$sql = "SELECT Title, ID FROM Bookmarks_Cats";

		$result =  $this->db->query($sql);
		return $result->fetchAll(); 
	}

}

?>

