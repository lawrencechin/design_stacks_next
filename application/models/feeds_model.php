<?php

class FeedsModel extends MasterModel{

	public function __construct(Database $db){
		parent::__construct($db);
	}

	public function add($catID, $url){
		$sql = "INSERT INTO Feed_URLS(CatID, URL) VALUES (:CatID, :URL)";

		$dataArr = array(
			'CatID'=>$catID, 
			'URL'=>$url
			);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function edit($catID, $url, $id){
		$sql = "UPDATE Feed_URLS SET CatID = :CatID, URL = :URL WHERE ID = :ID";
		$dataArr = array(
			'CatID' => $catID,
			'URL' => $url,
			'ID' => $id
			);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function delete($id){
		$sql = "DELETE FROM Feed_URLS WHERE ID = :ID";
		$dataArr = array('ID' => $id);
		return $this->commitDb($sql, $dataArr, false);
	}

	public function getFeeds(){
		$sql = "SELECT PostDate, Feed_List.Title, Description,Feed_List.ID, Feed_Categories.Title as CatTitle, Feed_URLS.CatID FROM Feed_List INNER JOIN Feed_URLS on URLID = Feed_URLS.ID INNER JOIN Feed_Categories on Feed_URLS.CatID = Feed_Categories.ID WHERE Feed_List.Read = 0 ORDER BY Feed_List.sortDate DESC";
		$result = $this->db->query($sql);
		return $result->fetchAll();
	}

	public function getContent($ID){
		$sql = "SELECT Title, PostDate, Content, Perma FROM Feed_List WHERE ID = :ID";
		$dataArr = array('ID' => $ID);
		return $this->commitDb($sql, $dataArr, true);
	}

	public function getCategories(){
		$sql = "SELECT ID, Title FROM Feed_Categories";
		$result = $this->db->query($sql);
		return $result->fetchAll();
	}

	public function getURLS(){
		$sql = "SELECT ID, CatID, URL FROM Feed_URLS";
		$result = $this->db->query($sql);
		return $result->fetchAll();
	}

	public function lastItem(){
		$sql ="SELECT ID FROM Feed_List WHERE Feed_List.Read = 0 ORDER BY Feed_List.sortDate DESC LIMIT 1";
		$result = $this->db->query($sql);
		return $result->fetch(PDO::FETCH_NUM)[0];
	}

	public function getCount(){
		$sql ="SELECT Count(*) FROM Feed_List WHERE Feed_List.Read = 0";
		$result = $this->db->query($sql);
		return $result->fetch(PDO::FETCH_NUM)[0];
	}

	public function markSingleRead($id){
		$sql = "UPDATE Feed_List SET Feed_List.Read = 1 WHERE ID = :ID";
		$dataArr = array('ID' => $id);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function markDayRead(){
		$yesterday  = date("Y-m-d H:i", strtotime("-1 day"));
		$sql = "UPDATE Feed_List SET Feed_List.Read = 1 WHERE sortDate < '$yesterday'";

		try{
			$this->db->exec($sql);
			return false;
		}catch(Exception $e){
			return true;
		}
	}

	public function markWeekRead(){
		$lastWeek  = date("Y-m-d H:i", strtotime("-1 day"));
		$sql = "UPDATE Feed_List SET Feed_List.Read = 1 WHERE sortDate < '$lastWeek'";

		try{
			$this->db->exec($sql);
			return false;
		}catch(Exception $e){
			return true;
		}
	}

	public function markAllRead(){
		$sql = "UPDATE Feed_List SET Feed_List.Read = 1";

		try{
			$this->db->exec($sql);
			return false;
		}catch(Exception $e){
			return true;
		}
	}

	private function truncateDescription($input, $numwords, $padding = ""){
		$output = strtok($input, " \n");
	    while(--$numwords > 0) $output .= " " . strtok(" \n");
	    if($output != $input) $output .= $padding;
	    return $output;
	}

	public function refreshFeeds(){
		$urlArr = array();
		$feedArr = array();

		foreach($this->getURLS() as $URLS){
			$urlArr[] = $URLS->URL;
			$feedArr[] = $URLS;
		}

		$feed = new SimplePie();
		$feed->set_feed_url($urlArr);
		$feed->set_cache_location(NON_HTTP_PATH . '/application/cache');
		$success = $feed->init();
		$feed->handle_content_type();

		if($success){

			foreach($feed->get_items() as $item){
				$title = $item->get_title();
				$id = $item->get_id(true);
				$date = $item->get_date();
				$sqlDate = $item->get_date("Y-m-d H:i");
				$description = $this->truncateDescription(strip_tags($item->get_description(true)), 30, "...");
				$content = $item->get_content();
				$perma = $item->get_permalink();
				//Get feed url to match against feed array
				$sourceURL = $item->get_feed()->subscribe_url();

				//Loop through feed array and match feed ID to feed item
				foreach($feedArr as $feedInfo){
					if(strcmp($sourceURL, $feedInfo->URL) === 0){
						$URLID = $feedInfo->ID;
					}
				}

				$itemArray[] = ["ID" => $id, "Title" => $title, "PostDate" => $date, "Description" => $description , "Content" => $content, 'Read' => 0, 'Perma' => $perma, 'URLID' => $URLID, 'sortDate' => $sqlDate];	

			}
		}

		$sql = "INSERT IGNORE INTO Feed_List(ID, PostDate, Title, Description, Content, Feed_List.Read, Perma, URLID, sortDate) VALUES (:ID, :PostDate, :Title, :Description, :Content, :Read, :Perma, :URLID, :sortDate)";

	    foreach($itemArray as $dbInsert){
	    	$this->commitDb($sql, $dbInsert, false);
	    }

	    $lastWeek = date("Y-m-d H:i", strtotime("-1 week"));

		$clearDb = "UPDATE Feed_List SET PostDate = '', Title = '', Description = '', Content = '', Perma = '', URLID = 0, sortDate = '' WHERE sortDate < '$lastWeek' AND Feed_List.Read = 1";

		$this->db->exec($clearDb);
	}
}

?>

