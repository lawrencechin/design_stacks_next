<?php
/**
* Login Model
*
**/
class LoginModel extends MasterModel{

    /**
	*	Constructor function
	*	@param $db requires a database object
	**/
	public function __construct(Database $db){
		parent::__construct($db);
	}

	//returns bool for success state
	public function login(){
		//check that fields are set
		if($this->login_params_set($_POST['user_name'])){
			$this->write_feedback(FEEDBACK_USERNAME_FIELD_EMPTY);
			return false;
		}elseif($this->login_params_set($_POST['user_password'])){
			$this->write_feedback(FEEDBACK_PASSWORD_FIELD_EMPTY);
			return false;
		}

		$sql = "SELECT user_id, user_name, user_email, user_password_hash, user_active, user_failed_logins, user_last_failed_login, user_edit, user_style, user_wallpaper FROM users WHERE (user_name = :user_name OR user_email = :user_email)";

		$dataArr = array(
            'user_name' => $_POST['user_name'],
            'user_email' => $_POST['user_name']
            );

		$result = $this->commitDb($sql, $dataArr, true);

		if(!$result){
			$this->write_feedback(FEEDBACK_LOGIN_FAILED);
            return false;
		}

        // block login attempt if somebody has already failed 3 times and the last login attempt is less than 30sec ago
        if(($result[0]->user_failed_logins >= 3) AND ($result[0]->user_last_failed_login > (time()-30))) {
            $this->write_feedback(FEEDBACK_PASSWORD_WRONG_3_TIMES);
            return false;
        }

        if(password_verify($_POST['user_password'], $result[0]->user_password_hash)){

        	if($result[0]->user_active != 1) {
        		$this->write_feedback(FEEDBACK_ACCOUNT_NOT_ACTIVATED_YET);
                return false;
            }

            // login process, write the user data into session
            $this->login_session_write($result[0]);

            //reset failed logins
            if($result[0]->user_last_failed_login > 0){
            	$sql = "UPDATE users SET user_failed_logins = 0, user_last_failed_login = NULL WHERE user_id = :user_id AND user_failed_logins != 0";
            	$dataArr = array(':user_id' => $result[0]->user_id);
            	$this->commitDb($sql, $dataArr, false);
            }

            //timestamp for last login
            $user_last_login_timestamp = time();
            $sql = "UPDATE users SET user_last_login_timestamp = :user_last_login_timestamp WHERE user_id = :user_id";
            $dataArr = array(':user_last_login_timestamp'=> $user_last_login_timestamp, ':user_id' => $result[0]->user_id);

            $this->commitDb($sql, $dataArr, false);

            // if user has checked the "remember me" checkbox, then write cookie
            if(isset($_POST['user_rememberme'])) {

                // generate 64 char random string
                $random_token_string = hash('sha256', mt_rand());

                // write that token into database
                $sql = "UPDATE users SET user_rememberme_token = :user_rememberme_token WHERE user_id = :user_id";
                $dataArr = array(':user_rememberme_token' => $random_token_string, ':user_id' => $result[0]->user_id);

                $this->commitDb($sql, $dataArr, false);

                // generate cookie string that consists of user id, random string and combined hash of both
                $cookie_string_first_part = $result[0]->user_id . ':' . $random_token_string;
                $cookie_string_hash = hash('sha256', $cookie_string_first_part);
                $cookie_string = $cookie_string_first_part . ':' . $cookie_string_hash;

                // set cookie
                setcookie('rememberme', $cookie_string, time() + COOKIE_RUNTIME, "/", COOKIE_DOMAIN);
            }
            //success! huzzah
            return true;
        }else{
        	$sql = "UPDATE users SET user_failed_logins = user_failed_logins+1, user_last_failed_login = :user_last_failed_login WHERE user_name = :user_name OR user_email = :user_name";
            $dataArr = array(':user_name' => $_POST['user_name'], ':user_last_failed_login' => time());
            $this->commitDb($sql, $dataArr, false);
            $this->write_feedback(FEEDBACK_PASSWORD_WRONG);
            return false;
        }

        //default return 
        return false;
	}

	//returns bool for success state
	public function loginWithCookie(){
		$cookie = isset($_COOKIE['rememberme']) ? $_COOKIE['rememberme'] : '';
		if(!$cookie){
			$this->write_feedback(FEEDBACK_COOKIE_INVALID);
			return false;
		}
		//check cookie contents and act upon
		list($user_id, $token, $hash) = explode(':', $cookie);

		if($hash !== hash('sha256', $user_id . ':' . $token)){
			$this->write_feedback(FEEDBACK_COOKIE_INVALID);
			return false;
		}

		if(empty($token)){
			$this->write_feedback(FEEDBACK_COOKIE_INVALID);
			return false;
		}

		$sql = "SELECT user_id, user_name, user_email, user_password_hash, user_active,user_failed_logins, user_last_failed_login
            FROM users WHERE user_id = :user_id
            AND user_rememberme_token = :user_rememberme_token
            AND user_rememberme_token IS NOT NULL";

        $dataArr = array(':user_id' => $user_id, ':user_rememberme_token' => $token); 

        $result = $this->commitDb($sql, $dataArr, true);
        if(!$result){
        	$this->write_feedback(FEEDBACK_COOKIE_INVALID);
			return false;
        }

    	$this->login_session_write($result[0]);
    	//login time
    	$user_last_login_timestamp = time();
    	$sql = "UPDATE users SET user_last_login_timestamp = :user_last_login_timestamp WHERE user_id = :user_id";
        $dataArr = array(':user_id' => $user_id, ':user_last_login_timestamp' => $user_last_login_timestamp);
        $this->commitDb($sql, $dataArr, false);

        $this->write_positive_feedback(FEEDBACK_COOKIE_LOGIN_SUCCESSFUL);
        return true;
	}

	public function logout(){
		$this->deleteCookie();

        // delete the session
        Session::destroy();
	}

    //Deletes the (invalid) remember-cookie to prevent infinitive login loops
    public function deleteCookie(){
        // set the rememberme-cookie to ten years ago (3600sec * 365 days * 10).
        // that's obviously the best practice to kill a cookie via php
        // @see http://stackoverflow.com/a/686166/1114320
        setcookie('rememberme', false, time() - (3600 * 3650), '/', COOKIE_DOMAIN);
    }

    /**
     * Returns the current state of the user's login
     * @return bool user's login status
     */
    public function isUserLoggedIn(){
        return Session::get('user_logged_in');
    }

    /**
     * Edit the user's name, provided in the editing form
     * @return bool success status
     */
    public function editUserName(){
        // new username provided ?
        if($this->login_params_set($_POST['user_name'])) {
            $this->write_feedback(FEEDBACK_USERNAME_FIELD_EMPTY);
            return false;
        }

        // new username same as old one ?
        if($_POST['user_name'] == $_SESSION["user_name"]) {
            $this->write_feedback(FEEDBACK_USERNAME_SAME_AS_OLD_ONE);
            return false;
        }

        // username cannot be empty and must be azAZ09 and 2-64 characters
        if (!preg_match("/^(?=.{2,64}$)[a-zA-Z][a-zA-Z0-9]*(?: [a-zA-Z0-9]+)*$/", $_POST['user_name'])) {
            $this->write_feedback(FEEDBACK_USERNAME_DOES_NOT_FIT_PATTERN);
            return false;
        }

        // clean the input
        $user_name = substr(strip_tags($_POST['user_name']), 0, 64);

        // check if new username already exists
        $sql = "SELECT user_id FROM users WHERE user_name = :user_name";
        $dataArr = array(':user_name' => $user_name);
        $result = $this->commitDb($sql, $dataArr, true);

        if($result){
        	$this->write_feedback(FEEDBACK_USERNAME_ALREADY_TAKEN);
            return false;
        }

        $sql = "UPDATE users SET user_name = :user_name WHERE user_id = :user_id";
        $dataArr = array(':user_name' => $user_name, ':user_id' => $_SESSION['user_id']);
        $result = $this->commitDb($sql, $dataArr, false);

        if(!$result){
        	$this->write_feedback(FEEDBACK_UNKNOWN_ERROR);
        	return false;
        }

        Session::set('user_name', $user_name);
        $this->write_positive_feedback(FEEDBACK_USERNAME_CHANGE_SUCCESSFUL);
       	return true;
    }

    /**
     * Edit the user's name, provided in the editing form
     * @return bool success status
     */
    public function editUserEmail(){
        // new username provided ?
        if($this->login_params_set($_POST['user_email'])) {
            $this->write_feedback(FEEDBACK_EMAIL_FIELD_EMPTY);
            return false;
        }

        // new username same as old one ?
        if($_POST['user_email'] == $_SESSION["user_email"]) {
            $this->write_feedback(FEEDBACK_USERNAME_SAME_AS_OLD_ONE);
            return false;
        }

        // user's email must be in valid email format
        if (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
        	$this->write_feedback(FEEDBACK_EMAIL_DOES_NOT_FIT_PATTERN);
            return false;
        }

        // clean the input
        $user_email = substr(strip_tags($_POST['user_email']), 0, 64);

        // check if new username already exists
        $sql = "SELECT user_id FROM users WHERE user_email = :user_email";
        $dataArr = array(':user_email' => $_POST['user_email']);
        $result = $this->commitDb($sql, $dataArr, true);

        if($result){
        	$this->write_feedback(FEEDBACK_USER_EMAIL_ALREADY_TAKEN);
            return false;
        }

        $sql = "UPDATE users SET user_email = :user_email WHERE user_id = :user_id";
        $dataArr = array(':user_email' => $user_email, ':user_id' => $_SESSION['user_id']);
        $result = $this->commitDb($sql, $dataArr, false);

        if(!$result){
        	$this->write_feedback(FEEDBACK_UNKNOWN_ERROR);
        	return false;
        }

        Session::set('user_email', $user_email);
        $this->write_positive_feedback(FEEDBACK_EMAIL_CHANGE_SUCCESSFUL);
       	return true;
    }

    /**
     * handles the entire registration process for DEFAULT users
     * @return boolean Gives back the success status of the registration
     */
    public function registerNewUser(){
        // perform all necessary form checks
        if(empty($_POST['user_name'])) {
            $this->write_feedback(FEEDBACK_USERNAME_FIELD_EMPTY);
        }else if(empty($_POST['user_password_new']) || empty($_POST['user_password_repeat'])) {
            $this->write_feedback(FEEDBACK_PASSWORD_FIELD_EMPTY);
        }else if($_POST['user_password_new'] !== $_POST['user_password_repeat']) {
            $this->write_feedback(FEEDBACK_PASSWORD_REPEAT_WRONG);
        }else if (strlen($_POST['user_password_new']) < 6) {
           $this->write_feedback(FEEDBACK_PASSWORD_TOO_SHORT);
        }else if(strlen($_POST['user_name']) > 64 || strlen($_POST['user_name']) < 2) {
            $this->write_feedback(FEEDBACK_USERNAME_TOO_SHORT_OR_TOO_LONG);
        }else if(!preg_match('/^[a-z\d]{2,64}$/i', $_POST['user_name'])) {
            $this->write_feedback(FEEDBACK_USERNAME_DOES_NOT_FIT_PATTERN);
        }else if(empty($_POST['user_email'])) {
            $this->write_feedback(FEEDBACK_EMAIL_FIELD_EMPTY);
        }else if(strlen($_POST['user_email']) > 64) {
            $this->write_feedback(FEEDBACK_EMAIL_TOO_LONG);
        }else if(!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
            $this->write_feedback(FEEDBACK_EMAIL_DOES_NOT_FIT_PATTERN);
        }else if(!empty($_POST['user_name'])
            && strlen($_POST['user_name']) <= 64
            && strlen($_POST['user_name']) >= 2
            && preg_match('/^[a-z\d]{2,64}$/i', $_POST['user_name'])
            && !empty($_POST['user_email'])
            && strlen($_POST['user_email']) <= 64
            && filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)
            && !empty($_POST['user_password_new'])
            && !empty($_POST['user_password_repeat'])
            && ($_POST['user_password_new'] === $_POST['user_password_repeat'])) {

            // clean the input
            $user_name = strip_tags($_POST['user_name']);
            $user_email = strip_tags($_POST['user_email']);

            // crypt the user's password with the PHP 5.5's password_hash() function, results in a 60 character
            // hash string. the PASSWORD_DEFAULT constant is defined by the PHP 5.5, or if you are using PHP 5.3/5.4,
            // by the password hashing compatibility library. the third parameter looks a little bit shitty, but that's
            // how those PHP 5.5 functions want the parameter: as an array with, currently only used with 'cost' => XX
            $hash_cost_factor = (defined('HASH_COST_FACTOR') ? HASH_COST_FACTOR : null);
            $user_password_hash = password_hash($_POST['user_password_new'], PASSWORD_DEFAULT, array('cost' => $hash_cost_factor));

            // check if username already exists
            $sql = "SELECT * FROM users WHERE user_name = :user_name";
            $dataArr = array('user_name' => $user_name);
            $result = $this->commitDb($sql, $dataArr, true);

            if($result){
            	$this->write_feedback(FEEDBACK_USERNAME_ALREADY_TAKEN);
            	return false;
            }

            // check if email already exists
            $sql = "SELECT * FROM users WHERE user_email = :user_email";
            $dataArr = array('user_email' => $user_email);
            $result = $this->commitDb($sql, $dataArr, true);

            if($result){
            	$this->write_feedback(FEEDBACK_USER_EMAIL_ALREADY_TAKEN);
            	return false;
            }

            // generate integer-timestamp for saving of account-creating date
            $user_creation_timestamp = time();

            // write new users data into database
            $sql = "INSERT INTO users(user_name, user_password_hash, user_email, user_creation_timestamp)
                VALUES (:user_name, :user_password_hash, :user_email, :user_creation_timestamp)";
            $dataArr = array('user_name' => $user_name,
            'user_password_hash' => $user_password_hash,
            'user_email' => $user_email,
            'user_creation_timestamp' => $user_creation_timestamp);

            $result = $this->commitDb($sql, $dataArr, false);

            if(!$result){
            	$this->write_feedback(FEEDBACK_ACCOUNT_CREATION_FAILED);
            	return false;
            }

            // get user_id of the user that has been created, to keep things clean we DON'T use lastInsertId() here
            $sql = "SELECT user_id FROM users WHERE user_name = :user_name";
            $dataArr = array('user_name' => $user_name);
            $result = $this->commitDb($sql, $dataArr, true);

            if(!$result){
            	$this->write_feedback(FEEDBACK_UNKNOWN_ERROR);
            	return false;
            }

            $user_id = $result[0]->user_id;


            $this->write_positive_feedback(FEEDBACK_ACCOUNT_SUCCESSFULLY_CREATED);
            return true;
            
        }else{
            $this->write_feedback(FEEDBACK_UNKNOWN_ERROR);
        }
        // default return, returns only true of really successful (see above)
        return false;
    }

    public function setStyle($style){
        $sql = "UPDATE users SET user_style = :user_style WHERE user_id = :user_id";
        $dataArr = array('user_style' => $style, 'user_id' => Session::get('user_id'));
        $this->commitDb($sql, $dataArr, false);
        Session::set('user_style', $style);
    }
}