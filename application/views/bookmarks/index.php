<div id="section_bookmarks">

	<div id="bookmark_menu">

		<?php foreach($this->bookCats as $bookCats){ ?>
		<div class="bookMenu_<?=$bookCats->ID?>">
			<?php $titleStr = strpos($bookCats->Title, " ") > 0 ? substr($bookCats->Title, 0, strpos($bookCats->Title, " ")) : $bookCats->Title; ?>
			<a href="#" data-link="<?=$bookCats->ID?>"><?=$titleStr?></a>
			<div id="section_book_<?=$bookCats->ID?>">
				<ul>
					<?php foreach($this->bookmarks as $bookmarks){ 
						if($bookmarks->ClassID == $bookCats->ID){ ?>
						<li>
							<div><a class="external" data-location="default" href="<?=$bookmarks->URL?>" data-ID="<?=$bookmarks->ID?>" data-Title="<?=$bookmarks->Title?>"><?=$bookmarks->Title?></a></div>

							<button title="Edit Bookmark" type="button" data-button="2" class="edit_bookmark" onclick="formSubmission.revealForm(this)">e</button>	
							<button title="Delete Bookmark" type="button" class="delete_bookmark" onclick="formSubmission.deleteInlineForm(this)">x</button>
						</li>
						<?php }
					} ?>
				</ul>
			</div>
		</div>
		<?php } ?>
	</div>

	<form id="delete_bookmark_form" name="delete_bookmark_form" action="<?=URL.'bookmarks/delete'?>" method="post">
	<input type="hidden" name="delete_book_ID" value="">
	<input type="submit" name="submit" value="delete">
	</form>

</div>

<div id="bookmark_content">
	<header id="book_cont_header">
		<button title="Show Main Menu" id="revealMenu" onclick="header.revealMenu()" class="mainMenu"><!--hamburger--></button>
		<button title="Add Bookmark" onclick="bookmarks.bookmarkAdd(this)" class="bookmark"><!--bookmark--></button>
		<input type="text" name="searchURL" value="http://">
		<button title="Go To Link" onclick="header.iframeChangeLocation(this)" class="search"><!--search--></button>
		<button title="Close Window" onclick="header.close()" class="close"><!--delete--></button>

	</header>
	<div id="content">
		<iframe src="about:blank" id="contentFrame" sandbox="allow-forms allow-scripts allow-same-origin"></iframe>
	</div>
</div>
<script>
formSubmission.submitForms('delete_bookmark_form', true);
header.loadExternalLinks('#section_bookmarks');
bookmarks.bookMenu();
</script>