<section id="profile" class="profile clearfix">
	<div class="stats right left_border" id="stats">
		<h1>Statistics</h1>
		<ul>
			<li class="fullWidth"><p class="bottom_border_double">Total number of movies</p>
			<strong><?=$this->totalMov?></strong></li>

			<li><p class="bottom_border_double">Watched movies</p>
			<strong><?=$this->totalWatchedMov?></strong>
			<p class="percentage top_border"><?=round(($this->totalWatchedMov/$this->totalMov)*100, 2)?>%</p>
			</li>

			<li><p class="bottom_border_double">Favourite movies</p>
			<strong><?=$this->totalFavMov?></strong>
			<i class="percentage top_border"><?=round(($this->totalFavMov/$this->totalMov)*100, 2)?>%</i>
			</li>

			<li class="show_stats fullWidth"><p class="bottom_border_double">Total number of shows</p>
			<strong><?=$this->totalShows?></strong></li>

			<li class="show_stats"><p class="bottom_border_double">Watched shows</p> 
			<strong><?=$this->totalWatchedShows?></strong>
			<i class="percentage top_border"><?=round(($this->totalWatchedShows/$this->totalShows)*100, 2)?>%</i>
			</li>

			<li class="show_stats"><p class="bottom_border_double">Favourite shows</p>
			<strong><?=$this->totalFavShows?></strong>
			<i class="percentage top_border"><?=round(($this->totalFavShows/$this->totalShows)*100, 2)?>%</i>
			</li>
		</ul>
	</div>

	<form class="profile_form left" name="edit_user_name" method="post" action="<?=URL?>login/editUsername">
		<br>
		<h2>Change User Name</h2>
		<label>
		<span>Current User Name</span>
		<input readonly="readonly" name="current_user_name" type="text" value="<?=$_SESSION['user_name']?>" />
		</label>
		<label>
		<span>New User Name</span>
		<input name="user_name" type="text" value="" placeholder="enter new name…" pattern="[a-zA-Z0-9]{2,64}" required />
		</label>
		<button class="submit" type="submit"></button>
	</form>

	
	<form class="profile_form left" name="edit_email_address" method="post" action="<?=URL?>login/editUserEmail">
		<h2>Change Email Address</h2>
		<label>
		<span>Current Email Address</span>
		<input readonly="readonly" name="current_email_address" type="text" value="<?=$_SESSION['user_email']?>">
		</label>
		<label>
		<span>New Email Address</span>
		<input name="user_email" type="text" value="" placeholder="enter new email…" required>
		</label>
		<button class="submit" type="submit"></button>
	</form>

</section>