<section class="login">
    <h1>Design Stacks</h1>
    <h2>Login</h2>
    <form action="<?=URL?>login/login" method="post">
        <label>
        <span>Username (<i>or email</i>)</span>
        <input type="text" name="user_name" placeholder="enter username or email…" required/>
        </label>

        <label>
        <span>Password</span>
        <input type="password" placeholder="enter password…"name="user_password" required type="password" />
        </label>
        <label class="remember-me-label">
        <span>Keep me logged in (<i>for 2 weeks</i>)</span>
        <input type="checkbox" name="user_rememberme" class="remember-me-checkbox" />
        </label>
        <button type="submit" class="submit"></button>
    </form>
</section>