<div id="section_feeds"> 
	<header id="feed_control">
		<!--Reveal edit menu or feed sort menu-->
		<button class="sortFeeds" title="Sort Feeds Menu" onclick="feeds.sort(this)"><!--sort--></button>

		<ul id="sortFeeds">
			<?php foreach($this->feedCats as $feedCats){ ?>
			<li class="feedCat-<?=$feedCats->ID?>"><a id="sort_<?=$feedCats->ID?>"><?=$feedCats->Title?></a></li>
			<?php } ?>
		</ul>

		<button class="editFeedsBtn" title="Open Edit Feed Menu" onclick="feeds.edit(this)"><!--edit--></button>
		<button class="markFeeds" title="Mark Feeds as Read Menu" onclick="feeds.markAllAsRead(this)"><!--submit--></button>

		<ul id="markFeeds">
			<p>Mark feeds as read:</p>
			<li><a href="#" data-option="Day">Older than a day</a></li>
			<li><a href="#" data-option="Week">Older than a week</a></li>
			<li><a href="#" data-option="All">All feeds</a></li>
		</ul>

		<button title="Get New Feeds" class="rss" onclick="feeds.getNew(this)"><!--rss--></button>
	</header>

	<div id="feed_list">
		<!--Loop through feed_list and populate-->
		<?php foreach($this->feeds as $feeds){ ?>
			
			<div class="feedArticle sort_<?=$feeds->CatID?>">
				<h3><span class="favicon feedCat-<?=$feeds->CatID?>"></span><a class="external" data-location="feedContent" data-id="<?=$feeds->ID?>" href="<?=URL.'feeds/contentFeed/'.$feeds->ID?>"><?=(strlen($feeds->Title) > 1) ? $feeds->Title : "No Title"?></a></h3>
				<p><?=$feeds->Description?></p>
				<span class="feedCat"><?=$feeds->CatTitle?></span>
				<span class="feedDate"><?=$feeds->PostDate?></span>
			</div>
		<!--end loop-->
		<?php } 
			if(count($this->feeds) <= 0){ ?>
				<div class="feedArticle">
				<h3>No Items</h3>
				<p>Try refreshing the feeds above</p>
				<span class="feedCat">No Posts</span>
			</div>
			<?php }
		?>
	</div>
</div>

<div id="feed_content">

	<header id="content_control">
		<button title="Reveal Main Menu" id="revealMenu" onclick="header.revealMenu()" class="mainMenu"><!--hamburger--></button>
		<button title="Bookmark Article" onclick="bookmarks.bookmarkAdd(this)" class="bookmark"><!--bookmark--></button>
		<input type="text" name="searchURL" value="http://">
		<button title="Go to Address" onclick="header.iframeChangeLocation(this)" class="search"><!--search--></button>
		<button title="Close Window"onclick="header.close()" class="close"><!--delete--></button>
	</header>

	<div id="content">
		<iframe src="about:blank" id="contentFrame" sandbox="allow-forms allow-scripts allow-same-origin"></iframe>
	</div>
</div>

<script>
header.loadExternalLinks('#section_feeds');
</script>