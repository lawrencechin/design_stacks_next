<div id="edit_cont">
	<div id="add_feed">
		<h4>Add Feed</h4>
		<form id="add_feed_form" class="contentForm" name="add_feed_form" action="<?=URL.'feeds/addFeed'?>" method="post">
			<select class="add_feed" name="add_feed">
			<?php foreach($this->cats as $cats){ ?>
				<option data-catColour="feedMenu_<?=$cats->ID?>" value= "<?=$cats->ID?>"><?=$cats->Title?></option>
				<?php } ?>
			</select>

			<a class="selectList" href="#">Select Category…</a>
			<input type="text" name="add_url" value="" placeholder="Enter Url…(please include http://)" required>
			<button type="submit" name="add_submit" disabled=disabled><!--submit--></button>
		</form>
	</div>

	<h4 class="editFeeds">Edit Feed</h4>
	<form id="edit_feed_form" class="contentForm" name="edit_feed_form" action="<?=URL.'feeds/editFeed'?>" method="post">
		<select class="edit_feed" name="edit_feed">
		<?php foreach($this->cats as $cats){ ?>
			<option data-catColour="feedMenu_<?=$cats->ID?>" value= "<?=$cats->ID?>"><?=$cats->Title?></option>
			<?php } ?>
		</select>

		<a class="selectList" href="#">Select Category…</a>

		<input type="hidden" name="edit_feed_ID" value="" required>
		<input type="url" name="edit_url" value="" required>
		<input type="hidden" name="feedDecision" value="edit">
		<button type="submit" name="edit_submit" disabled=disabled><!--submit--></button>
	</form>

	<?php foreach($this->cats as $cats){ ?>

		<span class="feedCat-<?=$cats->ID?>"></span>
		<h3><a href="feed-<?=$cats->ID?>"><?=$cats->Title?></a></h3>

		<ul class="feed-<?=$cats->ID?>">

		<?php foreach($this->urls as $urls){ 
			if($cats->ID == $urls->CatID){ ?>

			<li><span data-id="<?=$urls->ID?>" data-catid="<?=$urls->CatID?>"><?=$urls->URL?></span>

			<button title="Edit Feed Source" type="button" class="edit_feed" data-button="3" onclick="formSubmission.revealForm(this)">e</button>

			<button title="Delete Feed Source" type="button" data-button="3" class="delete_feed" onclick="formSubmission.deleteInlineForm(this)">x</button></li>

			<?php }
		} ?> 

		</ul>
	<?php } ?>

	<form id="delete_feed_form" name="delete_feed_form" action="<?=URL.'feeds/removeFeed'?>" method="post">
		<input type="hidden" name="delete_feed_ID" value="">
		<input type="submit" name="submit" value="delete">
	</form>
</div>

<script>
formSubmission.submitForms('feed_form');
formSubmission.selectLists($('#add_feed_form'),true);
feeds.formValidate('[name="add_url"]');
feeds.formValidate('[name="edit_url"]');
</script>