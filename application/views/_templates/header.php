<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Design Stacks</title>
    <meta name="description" content="Design Stacks">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- CSS -->
    <link rel="stylesheet" href="<?=URL?>public/css/screen-prefixed.css" />
    <link rel="shortcut icon" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon.ico">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-180x180.png">
    <meta name="apple-mobile-web-app-title" content="Design Stacks">
    <link rel="icon" type="image/png" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon-196x196.png" sizes="196x196">
    <link rel="icon" type="image/png" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon-32x32.png" sizes="32x32">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=URL.PUBLIC_IMAGES?>favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="<?=URL.PUBLIC_IMAGES?>favicons/browserconfig.xml">
    <meta name="application-name" content="Design Stacks">
</head>
<?php 
$class="";
if((int)Session::get('user_style') === 1){$class="deutch";}?>
<body id="home" class="<?=$class?>">
<div id="expose_menus">
    <div class="expose_main"><a href="#">
        <!--hamburger-->
    </a></div>
    <div class="expose_feeds"><a href="#"><!--rss--></a></div>
    <div class="expose_books"><a href="#"><!--book--></a></div>
    <div class="expose_stacks"><a href="#"><!--stack--></a></div>
</div>
<div id="bg">
    <img src="<?=SESSION::get('user_wallpaper')?>" alt="fullscreen Background">
</div>
<header id="mainHeader">
    <h1 id="logo">
        <!--design_logo-->   
    </h1>
    <nav id="index">
        <div id="stack">
            <a class='navigation' href="<?=URL.'stacks'?>">Design</a>
        </div>
        <div id="rss">
            <a data-feedCount="0" class='navigation' href="<?=URL.'feeds'?>">Feeds</a>
        </div>
        <div id="bookmarks">
            <a class='navigation' href="<?=URL.'bookmarks'?>">Bookmarks</a>
        </div>

        <div id="globalForms">

            <div class="bookmarks">

                <button title="Add Bookmark(Shortcut: b-o-o-k)" class="add_bookmark" data-button="1" onclick="formSubmission.revealForm(this)"><!--book--></button>

                <form id="add_bookmark_form" name="add_bookmark_form" class="headerForm" action="<?=URL.'bookmarks/add'?>" method="post">
                    <select name="bookCat">
                        <?php foreach($this->bookCats as $bookCats){
                            echo '<option data-catColour="bookMenu_'.$bookCats->ID.'" value = "'.$bookCats->ID.'">'.$bookCats->Title.'</option>';
                        } ?>
                    </select>
                    <p>Add Bookmark</p>
                    <a class="selectList" href="#">Select Category…</a>

                    <input type="text" name="title" placeholder="Title…" required>
                    <input type="url" name="url" placeholder="URL…" required>
                    <button type="submit" name="edit_submit"><!--submit--></button>
                    <button type="button" name="cancel_submit" data-cancel="global" onclick="formSubmission.closeForm(this)"><!--delete--></button>
                </form>
            </div>

            <div class="models">

                <button title="Add Files(Shortcut: u-p)" class="add_file" data-button="1" onclick="formSubmission.revealForm(this)"><!--file--></button>

                <form id="add_file_form" class="headerForm" name="add_file_form" enctype="multipart/form-data" action="<?=URL.'stacks/addImages'?>" method="POST">
                    <input type="hidden" name="MAX_FILE_SIZE" value="5242880000" />
                    
                    <select name="add_model_ID">
                        <?php foreach($this->stacks as $stacks){ ?>
                        <option data-catSort="<?=$stacks->catTitle?>" value = "<?=$stacks->ID?>"><?=$stacks->Title?></option>
                        <?php } ?>
                    </select>
                    <p>Add Files</p>
                    <a class="selectList" href="#">Select Subsection…</a>

                    <input id="fileUpload" type="file" multiple="true" name="files[]" accept="image/jpeg, image/jpg, image/gif, image/png, image/tiff">
                    <button type="submit" name="edit_submit"><!--submit--></button>
                    <button type="button" data-cancel="global" name="cancel_submit" onclick="formSubmission.closeForm(this)"><!--delete--></button>
                </form>
            </div>

            <div class="add_models">

                <button title="Add New Project(Shortcut: n-e-w)" class="add_model" data-button="1" onclick="formSubmission.revealForm(this)"><!--add--></button>

                <form id="add_model_form" class="headerForm" name="add_model_form" action="<?=URL.'stacks/add'?>" method="post">
                    <select name="add_model_cat">
                        <?php foreach($this->cats as $cats){ ?>
                        <option data-catColour="<?=$cats->Title?>" value = "<?=$cats->ID?>">
                        <?=$cats->Title?></option>
                        <?php } ?>
                    </select>
                    <p>Add Design Project</p>
                    <a class="selectList" href="#">Select Category…</a>

                    <input type="text" name="add_model_title" placeholder="Enter Name…" required>
                    <button type="submit" name="edit_submit"><!--submit--></button>
                    <button type="button" data-cancel="global" name="cancel_submit" onclick="formSubmission.closeForm(this)"><!--delete--></button>
                </form>
            </div>

            <form id="edit_model_form" class="headerForm" name="edit_model_form" action="<?=URL.'stacks/edit'?>" method="post">
                <select name= "edit_model_cat">
                    <?php foreach($this->cats as $cats){ ?>
                        <option data-catColour="<?=$cats->Title?>" value="<?=$cats->ID?>">
                        <?=$cats->Title?></option>
                    <?php } ?>
                </select>
                <p>Edit Project</p>
                <a class="selectList" href="#">Select Category…</a>
                
                <input type="text" name="edit_model_title" value="" required>
                <input type="hidden" name="edit_model_ID"value="">
                <button type="submit" name="edit_submit"><!--submit--></button>
                <button type="button"  data-cancel="midForm" name="cancel_submit" onclick="formSubmission.closeForm(this)"><!--delete--></button>
            </form>

            <form id="edit_bookmark_form" class="headerForm" name="edit_bookmark_form" action="<?=URL.'bookmarks/edit'?>" method="post">
                <p>Edit Bookmark</p>
                <select class="edit_bookCat" name="edit_bookCat">
                <?php foreach($this->bookCats as $bookCats){
                    echo '<option data-catColour="bookMenu_'.$bookCats->ID.'" value = "'.$bookCats->ID.'">'.$bookCats->Title.'</option>';
                } ?>
                </select>

                <a class="selectList" href="#">Select Category…</a>

                <input type="hidden" name="edit_book_ID" value="" required>
                <input type="text" name="edit_title" value="" required>
                <input type="url" name="edit_link" value="" required>
                <button type="submit" name="edit_submit"><!--submit--></button>
                <button type="button" name="cancel_submit" data-cancel="midForm" onclick="formSubmission.closeForm(this)"><!--delete--></button>
            </form>
        </div>
        
        <span id="tempFiles"><a class="navigation" href="<?=URL.'stacks/temp'?>">Temp Files</a></span><span id="logout"><a href="<?=URL.'login/logout'?>">Log Out</a></span>
    </nav>
</header>