<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Design Stacks</title>
    <meta name="description" content="Design Stacks">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- CSS -->
    <link rel="stylesheet" href="<?=URL?>public/css/screen-prefixed.css" />
    <link rel="shortcut icon" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon.ico">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=URL.PUBLIC_IMAGES?>favicons/apple-touch-icon-180x180.png">
    <meta name="apple-mobile-web-app-title" content="Design Stacks">
    <link rel="icon" type="image/png" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon-196x196.png" sizes="196x196">
    <link rel="icon" type="image/png" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?=URL.PUBLIC_IMAGES?>favicons/favicon-32x32.png" sizes="32x32">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=URL.PUBLIC_IMAGES?>favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="<?=URL.PUBLIC_IMAGES?>favicons/browserconfig.xml">
    <meta name="application-name" content="Design Stacks">
</head>

<body id="home">