<div id="crop_container">

<button title="Close Window" onclick="crop.close();" id="close_crop"><!--delete--></button>
<div class="thumbnail_area">
  <h4>Select a thumbnail to crop</h4>
  <ul>
  <?php foreach($this->thumbArr as $k => $v){?>
    <li><a target="_blank" href="<?=$this->imgArr[$k]?>"><img src="<?=$v?>"/></a></li>
  <?php } ?>
  </ul>
</div>

<div class="main_crop_area">
  <img src="" id="jcrop"/>
  <button id="cancel_crop">cancel</button>
  <button id="submit_crop">submit</button>
</div>

<form id="hidden_crop_form" action="<?=URL.'stacks/cropImg/'?>" name="hidden_crop_form" style="display:none;">
  <input type="hidden" id="x_cor" name="x_cor" />
  <input type="hidden" id="y_cor" name="y_cor" />
  <input type="hidden" id="w_cor" name="w_cor" />
  <input type="hidden" id="h_cor" name="h_cor" />
  <input type="hidden" id="image_cor" name="image_cor" />
  <input type="hidden" id="crop_cor" name="crop_cor" value="crop"/>
  <input type="submit" />
</form>

<div id="crop_comparrison">
  <h4>Review Images</h4>
  <div>

  <?php if(is_file(URL.THUMBDIR.$this->folder[0]->user_creation_timestamp.'/'.$this->id.'-thumb.jpg')){
    $imgThumb = URL.THUMBDIR.$this->folder[0]->user_creation_timestamp.'/'.$this->id.'-thumb.jpg?m='.filemtime(NON_HTTP_PATH.THUMBDIR.$this->folder[0]->user_creation_timestamp.'/'.$this->id.'-thumb.jpg');
  }else{
    $imgThumb = URL.PUBLIC_IMAGES.'no_thumb.jpg';
  }?>

    <img src="<?=$imgThumb?>" width="200px" height="250px"/>
    <h4>Current Image</h4>
  </div>

  <div>
    <img src="" id="new_image" width="200px" height="250px"/>
    <h4>New Image</h4>
  </div>
  <button onclick="crop.closeFinal();" id="cancel_thumb">Cancel</button>
  <button onclick="crop.finalise(this);" id="confirm_thumb">Accept new thumb?</button>

  <form id="accept_crop_form" action="<?=URL.'stacks/acceptCrop'?>" name="accept_crop_form" style="display:none;">
    <input type="hidden" id="projectID" name="projectID" value="<?=$this->id?>"/>
    <input type="submit" />
  </form>
</div>

<script src="<?=URL.'public/js/min/jquery.Jcrop.min.js'?>"></script>

<script>
  crop.selectImage('.thumbnail_area');
  crop.submitForm($('#hidden_crop_form'));
  crop.submitForm($('#accept_crop_form'));
</script>

</div>







