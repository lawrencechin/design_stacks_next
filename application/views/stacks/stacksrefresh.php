<header id="stacks_control">
	<button class="sortFeeds" title="Show Sort-Project Menu" onclick="feeds.sort(this)"><!--sort--></button>
	<ul id="sortStacks">
		<?php foreach($this->cats as $cats){ ?>
		<li class="stacksCat <?=$cats->Title?>"><a class="<?=$cats->Title?>"><?=$cats->Title?></a></li>
		<?php } ?>
	</ul>

	<span><strong>sort:</strong> <a href="asc">asc</a> | <a href="desc">desc</a> | <a href="rand">rand</a></span>

	<input id="searchStacks" type="text" name="searchStacks" placeholder="Search Name…">
	<button title="Search Projects" onclick="stacks.showSearch(this)" class="search"><!--search--></button>
</header>

<div id="stacksScrollContainer">
	<?php foreach($this->stacks as $stacks){ 
		if(is_file(NON_HTTP_PATH.THUMBDIR.$this->folder.$stacks->ID.'-thumb.jpg')){
			$img = URL.THUMBDIR.$this->folder.$stacks->ID.'-thumb.jpg?m='.filemtime(NON_HTTP_PATH.THUMBDIR.$this->folder.$stacks->ID.'-thumb.jpg');
		}else{
			$img = URL.PUBLIC_IMAGES.'loader.svg';
		}?>

	<div class="stacked <?=$stacks->catTitle?>">

		<a class="external" data-id="<?=$stacks->ID?>" data-location="stacks" href="<?=URL.'stacks/project/'.$stacks->ID?>" data-title="<?=$stacks->Title?>">

	    	<img class="lazy" src="<?=URL.PUBLIC_IMAGES.'loader.svg'?>" data-original="<?=$img?>" width="200px">
	    </a>

		<a class="external" data-id="<?=$stacks->ID?>" data-location="stacks" href="<?=URL.'stacks/project/'.$stacks->ID?>" data-title="<?=$stacks->Title?>">

	    	<h3><?=$stacks->Title?></h3>
	    	
	    </a>
	    	
    	<button title="Edit Project" type="button" data-button="2" class="edit_model" onclick="formSubmission.revealForm(this)">e</button>	
		<button title="Delete Project" type="button" class="delete_models" data-stacks = "true" onclick="formSubmission.deleteInlineForm(this)">x</button>
	</div>

	<?php } ?>
</div>