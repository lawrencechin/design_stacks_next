<div id="temp_files">
	<div id="tempFilesContainer">

		<ul id="file_list">
			<!--check if there are any files in folder-->
			<?php if(empty($this->files['files'])){ ?>
				<div><span>Nothing to see here, move along</span></div>
			<!-- process files -->
			<?php }else{
				foreach($this->files['files'] as $files){
					echo '<li><a href="#">';
					if(!$files['fileAllowable']){ ?>
					<span><img data-notfile="true" data-original="<?=$this->path.urlencode($files["filePath"])?>" style="display:none;"/> <?=$files['filePath']; ?></span>
					<?php }else{ ?>
					<img data-original="<?=$this->path.urlencode($files["filePath"])?>" src="<?=URL.LIBS_PATH.'Timthumb.php?src='.$this->path.urlencode($files["filePath"]).'&w=200'?>" />
				<?php } ?>

				</a></li>	
			<?php }
			} ?>	
		</ul>
		<div id="display_panel">
			<ul id="dir_list">
				<select name="add_model_ID">
				    <?php foreach($this->stacks as $stacks){ ?>
				    <option data-catSort="<?=$stacks->catTitle?>" value = "<?=$stacks->ID?>"><?=$stacks->Title?></option>
				    <?php } ?>
				</select>

				<div class="selectListContainer"><a href="#" class="selectList">Select Subsection…</a></div>

				<li><button title="Move Selected Files" class="move_photo" onclick="temp.tempFunction('move');"><!--move--></button></li>

				<li><button title="Delete Selected Files" class="file_delete" onclick="temp.tempFunction('delete');"><!--trash--></button></li>

				<li><button title="Close Temp Files" onclick="header.removeContent();"class="close_window_temp"><!--delete--></button></li>
			</ul>
			<div id="tempMessageCont">
				<div><!--stack--></div>
				<p>Nothing selected</p>
			</div>
		</div>
	</div>
</div>	

<script>
temp.selectFiles();
formSubmission.selectLists($('#dir_list'), true);
</script>