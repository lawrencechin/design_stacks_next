<div id="section_models">

	<div id="model_pictures">
		<div class="modelPictures markdown">
		<button class="md_expand" onclick="projects.mdExpand(this)"></button>
		<button class="md_syntax" onclick="projects.mdSyntax(this)"><span>i</span></button> 
		<h3 class="md_title <?=$this->notes[0]->catTitle?>"><?=$this->notes[0]->Title?></h3>
		<article class="noteTxt">
			<?php $Parsedown = new Parsedown();
			if(empty($this->notes[0]->notes)){
				echo $Parsedown->text('Add some *notes* here. Double click to edit. Uses \****Markdown**\** syntax.');
			}else{
				$text = htmlentities($this->notes[0]->notes, ENT_QUOTES, 'UTF-8');
				echo $Parsedown->text($text);

			}
		echo "</article>";
		echo "</div>";

	if(empty($this->project)){
		echo '<div class="modelPictures noImgs">No Images, sorry!</div>';
	}else{
		foreach($this->project as $project)
		{
			$favStatus = 'false';
			//check if image is a favourite
			foreach($this->favs as $fav){
				
				if($fav->PhotoID === $project->ID){
					$favStatus = 'true';
					break;
				}
			}

			$location = locatGen::gen($project->IMGname);
			$link = CONTENT_DIR.$location;
			$thumb = $link.locatGen::thumbLocation($project->IMGname);
		?>
		<!--set data-attr for fav pic-->
		<div id="<?=$project->ID?>-pic" class="modelPictures">
			<a data-id="<?=$project->ID?>" data-fav="<?=$favStatus?>" class="onlyThis" href="<?=URL.$link.$project->IMGname?>" target="_blank">
			<img class="lazy" src="<?=URL.PUBLIC_IMAGES.'loader.png'?>" data-original="<?=URL.$thumb?>" width="200px" height="250px"/>
			</a>
		</div>
		<?php } ?>
	<?php } ?>
	</div>

	<div id="media">
		<header id="mediaControlBar">
			<button title="Shift Window Right" class="shiftWindow" onclick="mediaView.hide(this)"><!--back--></button>

			<button title="Close Window(Keyboard Shortcut: X)" class="mediaClose close" onclick="mediaView.close(this)"><!--delete--></button>
			<button title="Previous File(Keyboard Shortcut: &larr;)" class="prevPic" onclick="mediaView.back()"><!--browserBack--></button>
			<button title="Next File(Keyboard Shortcut: &rarr;)" class="nextPic" onclick="mediaView.forward()"><!--browserForward--></button>

			<div id="pictureControls">
				<button title="Slideshow(Keyboard Shortcut: Space)" id="slideShowButton" onclick="mediaView.slideShow(this)"><!--play--></button>
				<button title="Add/Remove Favourite(Keyboard Shortcut: F)" data-id="" class="fav" onclick="mediaView.favourites(this)"><!--favourite--></button>
				<button title="Delete File" class="picTrash close" onclick="mediaView.trash()"><!--trash--></button>
				<button title="Image Original Size" class="imageOriginal" onclick="mediaView.imageOriginal(this)"><!--originalSizenobox--></button>
				<button title="Image Fit Width(Keyboard Shortcut: &darr;)" class="imageFullWidth" onclick="mediaView.imageFullWidth(this)"><!--fullWidthnobox--></button>
				<button title="Image Fit Height(Keyboard Shortcut: &uarr;)" class="imageToFit" onclick="mediaView.imageToFit(this)"><!--fullHeightnobox--></button>
			</div>
		</header>

		<div class="holder">
			<img data-id="" src="">
		</div>

	</div>

</div>

<script>
projects.loadContent('#section_models');
lazyLoad.init('img.lazy', '.modelPictures', '', false);
projects.sorting();
projects.modelID = "<?=$this->projectID?>";
projects.formIDs();
formSubmission.selectLists($('#move_photo_projects'), true);
projects.noteEdit($('.markdown'));
</script>