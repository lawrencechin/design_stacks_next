<?php
	$text = "## Markdown Syntax  
*** 
*No **HTML** tags allowed*
***
* \#\#\# : Headers (h3), *hashes denote header level*  
* \* : Enclose word/paragraph in *asterisks* for *italics*  
* \*\* : Encase word/paragraph in double **asterisks** for **bold** text  
* \_\*\* : _**Bold italics**_  
* TAB : Press the *TAB* to enter code  
* \` : Use a *back tick* to enter **code** inline  
* Paragraph : Simply write lines of text. End line with a *double  space* to create a line-break  
* \*\*\* : Horizontal line  
* Link : \[Text to make into link]\(url)  
* Escape : Escape special characters with a \* backslash \*  
* Images : \!\[Alt Text]\(url)

1. Unordered List : \* List Item  
2. Ordered List : 1\. List Item  
 1. Nested List : Add an additional space before the *asterisk* to add a nested list. **Ordered** and **Unordered** can be mixed.  
 1. Code in List : Use a *double* TAB space to add code to a list";
?>

<article class="noteTxt guide">
	<?php $Parsedown = new Parsedown();
	echo $Parsedown->text($text); ?>
</article>