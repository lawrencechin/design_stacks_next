<div id="section_categories">
	<header id="stacks_control">
		<button class="sortFeeds" title="Show Sort-Project Menu" onclick="feeds.sort(this)"><!--sort--></button>
		<ul id="sortStacks">
			<?php foreach($this->cats as $cats){ ?>
			<li class="stacksCat <?=$cats->Title?>"><a class="<?=$cats->Title?>"><?=$cats->Title?></a></li>
			<?php } ?>
		</ul>

		<span><strong>sort:</strong> <a href="asc">asc</a> | <a href="desc">desc</a> | <a href="rand">rand</a></span>

		<input id="searchStacks" type="text" name="searchStacks" placeholder="Search Name…">
		<button title="Search Projects" onclick="stacks.showSearch(this)" class="search"><!--search--></button>
	</header>

	<div id="stacksScrollContainer">
		<?php if(count($this->stacks) > 0){

			foreach($this->stacks as $stacks){ 
				if(is_file(NON_HTTP_PATH.THUMBDIR.$this->folder.$stacks->ID.'-thumb.jpg')){
					$img = URL.THUMBDIR.$this->folder.$stacks->ID.'-thumb.jpg?m='.filemtime(NON_HTTP_PATH.THUMBDIR.$this->folder.$stacks->ID.'-thumb.jpg');
				}else{
					$img = URL.PUBLIC_IMAGES.'loader.svg';
				}?>

			<div class="stacked <?=$stacks->catTitle?>">

				<a class="external" data-id="<?=$stacks->ID?>" data-location="stacks" href="<?=URL.'stacks/project/'.$stacks->ID?>" data-title="<?=$stacks->Title?>">

			    	<img class="lazy" src="<?=URL.PUBLIC_IMAGES.'loader.svg'?>" data-original="<?=$img?>" width="200px">
			    </a>

				<a class="external" data-id="<?=$stacks->ID?>" data-location="stacks" href="<?=URL.'stacks/project/'.$stacks->ID?>" data-title="<?=$stacks->Title?>">

			    	<h3><?=$stacks->Title?></h3>
			    	
			    </a>
			    	
		    	<button title="Edit Project" type="button" data-button="2" class="edit_model" onclick="formSubmission.revealForm(this)">e</button>	
				<button title="Delete Project" type="button" class="delete_models" data-stacks = "true" onclick="formSubmission.deleteInlineForm(this)">x</button>
			</div>

		<?php } 
		}else{ ?>
			<div class="add_new_project_guide">
				<article class="noteTxt">
					<p>Add New Projects</p>
					<div><img src="<?=URL.PUBLIC_IMAGES.'add_project.jpg'?>" width="600px" height="422px">
					<img class="imgOverlay" src="" width="600px" height="422px"></div>

					<ol id="guide_one">
						<li data-img="<?=URL.PUBLIC_IMAGES.'proj1.png'?>">Click the <em>Add New Project</em> button in the <em>sidebar</em>.<br>Keyboard shortcut: <strong>new</strong>(<em>type word</em>)</li>
						<li data-img="<?=URL.PUBLIC_IMAGES.'proj2.png'?>">Select a <strong>Category</strong> for the <strong>project</strong>.</li>
						<li data-img="<?=URL.PUBLIC_IMAGES.'proj3.png'?>">Give the <strong>project</strong> a name.</li>
						<li data-img="<?=URL.PUBLIC_IMAGES.'proj4.png'?>">Submit! Or cancel if you don't fancy it.</li>
					</ol>
				</article>
			</div>

			<div class="add_images_guide">
				<article class="noteTxt">
					<p>Add Images to Projects</p>
					<div><img src="<?=URL.PUBLIC_IMAGES.'add_images.jpg'?>" width="600px" height="422px">
					<img class="imgOverlay" src="" width="600px" height="422px"></div>
					<ol id="guide_two">
						<li data-img="<?=URL.PUBLIC_IMAGES.'addImg1.png'?>">Click the <em>Add Files</em> button in the <em>sidebar</em>.<br>Keyboard shortcut: <strong>up</strong>(<em>type word</em>)</li>
						<li data-img="<?=URL.PUBLIC_IMAGES.'addImg2.png'?>">Select the <strong>Project</strong> to add images to.</li>
						<li data-img="<?=URL.PUBLIC_IMAGES.'addImg3.png'?>">Select your images using the <em>file picker</em>.</li>
						<li data-img="<?=URL.PUBLIC_IMAGES.'addImg4.png'?>">Upload!</li>
					</ol>
				</article>
			</div>
		<?php }?>
	</div>
</div>

<div id="section_cat_content">
		<form id="delete_models_form" name="delete_models_form" action="<?=URL.'stacks/delete'?>" method="post">
			<input type="hidden" name="delete_model_ID" value="">
			<input type="submit" name="submit" value="delete">
		</form>
	<header id="models_header">
		<!--reveal menu-->
		<button title="Reveal Main Menu" id="revealMenu" onclick="header.revealMenu()" class="mainMenu"><!--hamburger--></button>
		<!--add files-->
		<button title="Add Files To This Project" class="add_file_models" data-add="true" onclick="projects.showForm(this)"><!--add--></button>
		<!--close menu-->
		<button title="Close Window" onclick="header.close(true)" class="close"><!--delete--></button>
		<!--sort options-->
		<span><strong>sort:</strong> <a href="asc">asc</a> | <a href="desc">desc</a> | <a href="rand">rand</a></span>

		<!--move files to another project-->
		<button title="Toggle Edit Mode" class="editProjectFiles" onclick="projects.editMode(this)"><!--edit--></button>
		<!--move files to another project-->
		<button title="Move Selected Files" disabled="disabled" class="move_photo_projects" onclick="projects.showForm(this)"><!--move--></button>
		<!--check all/deselct all-->
		<button title="Select/Deselect All Files" disabled="disabled" onclick="projects.select(this)" class="deselect_checks"><!--checkbox--></button>
		<!--set project main thumb-->
		<button title="Generate Project Thumb for Selected Files" disabled="disabled" onclick="crop.init(this)" class="image_crop"><!--crop--></button>
		<!--delete files-->
		<button title="Delete Selected Files" disabled="disabled" class="file_delete_projects close" onclick="projects.triggerSubmit('file_delete_projects')"><!--trash--></button>

		<form id="file_delete_projects" name="file_delete_projects" action="<?=URL.'stacks/deleteImages'?>" method="post">
		    <input type="hidden" name="IDArray" value="">
			<input type="submit" name="delete_photo_submit">
		</form>

		<form id="move_photo_projects" name="move_photo_projects" action="<?=URL.'stacks/editImages'?>" method="post">
		        <select name="select_model">
		        	<?php foreach($this->stacks as $stacks){ ?>
		        	<option data-catSort="<?=$stacks->catTitle?>" value="<?=$stacks->ID?>"><?=$stacks->Title?></option>
		        	<?php } ?>
		        </select>

		        <a class="selectList" href="#">Select Subsection…</a>

		        <input type="hidden" name="IDArray" value="">
				<button type="submit" name="edit_submit"><!--submit--></button>
				<button type="button" name="cancel_submit" onclick="projects.removeForm(this)"><!--delete--></button>
		</form>
		
		<form id="add_file_models" name ="add_file_models" enctype="multipart/form-data" action="<?=URL.'stacks/addImages'?>" method="POST">
		    <!-- MAX_FILE_SIZE must precede the file input field -->
		    <input type="hidden" name="MAX_FILE_SIZE" value="5242880000" />
		    <!-- Name of input element determines name in $_FILES array -->

			<input id="fUpload" type="file" multiple="true" name="files[]" accept="image/jpeg, image/jpg, image/gif, image/png, image/tiff">
			<input type="hidden" name="add_model_ID" value="">
			<button type="submit" name="edit_submit"><!--submit--></button>
			<button type="button" name="cancel_submit" onclick="projects.removeForm(this)"><!--delete--></button>
		</form>
	</header>

	<div id="content"></div>
</div>

<script>
formSubmission.submitForms('_models', true);
formSubmission.submitForms('_projects', false);
formSubmission.checkFileTypes('#fUpload');
lazyLoad.init('img.lazy', 'a.external', '#section_categories', true);
header.loadExternalLinks('#section_categories');
stacks.init();
stacks.guides($('#guide_one'));
stacks.guides($('#guide_two'));
</script>