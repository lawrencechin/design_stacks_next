<?php
/**
* Class View
**/

class View{
	//renders the specified view
	public function render($filename, $render_without_header_and_footer = false, $login = false){
		//page without header or footer
		if($render_without_header_and_footer == true){
			require VIEWS_PATH . $filename . '.php';
		}else if($login){
			require VIEWS_PATH . '_templates/header_login.php';
			require VIEWS_PATH . $filename . '.php';
			require VIEWS_PATH . '_templates/footer_login.php';
		}else{
			require VIEWS_PATH . '_templates/header.php';
			require VIEWS_PATH . $filename . '.php';
			require VIEWS_PATH . '_templates/footer.php';
		}
	}

	//checks if passed string is currently active controller
	private function checkForActiveController($filename, $navigation_controller){
		$split_filename = explode("/", $filename);
		$active_controller = $split_filename[0];

		if($active_controller == $navigation_controller){
			return true;
		}
		return false;
	}

	//Checks if the passed string is the currently active controller and controller-action.
	private function checkForActiveControllerAndAction($filename, $navigation_controller_and_action){
        $split_filename = explode("/", $filename);
        $active_controller = $split_filename[0];
        $active_action = $split_filename[1];

        $split_filename = explode("/", $navigation_controller_and_action);
        $navigation_controller = $split_filename[0];
        $navigation_action = $split_filename[1];

        if($active_controller == $navigation_controller && $active_action == $navigation_action) {
            return true;
        }
        // default return of not true
        return false;
    }

    //feedback messages
	private function renderFeedBackMessages(){
		//show error/success messages(held in $_SESSION["feedback_postive/negative"])
		require VIEWS_PATH . '_templates/feedback.php';
		//clear session messages after display
		Session::set('feedback_positive', null);
		Session::set('feedback_negative', null);
	}
}