<?php
//all models extend from this one allowing us to utilise helper functions used throughout 
class MasterModel{

	public function __construct(Database $db){
		$this->db = $db;
	}

	//helper methods available to all extended models

	protected function write_feedback($feedback){
		$_SESSION['feedback_negative'][] = $feedback;
	}

	protected function write_positive_feedback($feedback){
		$_SESSION['feedback_positive'][] = $feedback;
	}

	//write to database and return PDO object
	protected function commitDb($sql, $dataArr, $bool){
		$sth = $this->db->prepare($sql);
		try{
			$sth->execute($dataArr);
			//if $bool is set to true then return PDO object to be used in other functions
			if($bool){
				return $result = $sth->fetchAll();
			}
			return true;

		}catch(PDOException $e){
			//$errorMsg = $e->getMessage();
			//return $errorMsg;
			return false;
		}
	}

	protected function login_params_set($postVar){
		if(!isset($postVar) || empty($postVar)){
			return true;
		}
        return false;
	}

	protected function login_session_write($result){
		//start and write session data 
		Session::init();
        Session::set('user_logged_in', true);
        Session::set('user_id', $result->user_id);
        Session::set('user_name', $result->user_name);
        Session::set('user_email', $result->user_email);
        Session::set('user_style', $result->user_style);
        Session::set('user_edit', $result->user_edit);
        Session::set('user_wallpaper', $result->user_wallpaper);
	}	
}

?>