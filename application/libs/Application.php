<?php
/**
* The heart of the app: Application
*
**/

class Application{
	/** @var null The controller part of the URL */
    private $url_controller;
    /** @var null The method part (of the above controller) of the URL */
    private $url_action;
    /** @var null Parameter one of the URL */
    private $url_parameter_1;
    /** @var null Parameter two of the URL */
    private $url_parameter_2;
    /** @var null Parameter three of the URL */
    private $url_parameter_3;

    public function __construct(){
    	$this->splitURL();
    	//check for controller: is url_controller not empty
        if($this->app_contr()){
            //check method
            if($this->app_method()){
                //run method with or without params
                $this->app_param();
            }else{
                //Call default/fallback index method()
                $this->url_controller->index();
            }
        }else{
            //controller empty, show home/index
            require CONTROLLER_PATH . 'index.php';
            $controller = new Index();
            $controller->index();
        }
    }
    //private internal functions
    private function app_contr(){
        //object field initialised?
        if($this->url_controller){
            //controller exists at path?
            if(file_exists(CONTROLLER_PATH . $this->url_controller . '.php')){
                //load controller 
                require CONTROLLER_PATH . $this->url_controller . '.php';
                $this->url_controller = new $this->url_controller();
                return true;
            }
        }

        return false;
    }

    private function app_method(){
        //method field initialised
        if($this->url_action){
            //method exists in controller
            if(method_exists($this->url_controller, $this->url_action)){
                return true;
            }else{
                //method doesn't exist, redirect user to error page
                Application::app_new_location('error/index');
                return false;
            }
        }
        return false;
    }

    private function app_param(){
        //pass arguments to method
        //got to be a better way to write this
        if(isset($this->url_parameter_3)){
        $this->url_controller->{$this->url_action}($this->url_parameter_1, $this->url_parameter_2, $this->url_parameter_3);
        }else if(isset($this->url_parameter_2)) {
            $this->url_controller->{$this->url_action}($this->url_parameter_1, $this->url_parameter_2);
        }else if(isset($this->url_parameter_1)) {
            $this->url_controller->{$this->url_action}($this->url_parameter_1);
        }else{
            // if no parameters given, just call the method without arguments
            $this->url_controller->{$this->url_action}();
        }
    }
    //splits URL into controller/method/params
    private function splitUrl(){
    	if(isset($_GET['url'])){
    		$url = rtrim($_GET['url'], '/');
    		$url = filter_var($url, FILTER_SANITIZE_URL);
    		$url = explode('/', $url);
    		//populate fields of class/object

    		$this->url_controller = (isset($url[0]) ? $url[0] : null);
    		$this->url_action = (isset($url[1]) ? $url[1] : null);
    		$this->url_parameter_1 = (isset($url[2]) ? $url[2] : null);
    		$this->url_parameter_2 = (isset($url[3]) ? $url[3] : null);
    		$this->url_parameter_3 = (isset($url[4]) ? $url[4] : null);
    	}
    }
    
    //set location - this will likely change in the future so better to wrap it in a static function call here rather than hunting around for every line in the app
    public static function app_new_location($location){
        header('location: ' . URL . $location);
    }
}