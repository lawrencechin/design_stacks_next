<?php
class locatGen{
	//take file name and split into year, month and first two characters of hashed name
	static public function gen($file){
		$location = substr($file, 0, 4)."/".substr($file, 5, 2)."/".substr($file, 11, 2)."/";
		return $location;
	}

	static public function thumbLocation($file){
		$thumbLocation = "thumb/".substr($file, 0, strripos($file, '.')).".jpg";
		return $thumbLocation;
	}

	static public function nameGen($file, $fileExt){
		$today = date("Y-m-d-");
		$shaName = hash_file('sha1', $file);
		$finalname = $today.$shaName.".".$fileExt;
		return $finalname;
	}

	static public function createDir($location){
		if(!is_dir(NON_HTTP_PATH.CONTENT_DIR.$location) && !is_dir(NON_HTTP_PATH.CONTENT_DIR.$location."thumb/")){
			$oldmask = umask(0);
			mkdir(NON_HTTP_PATH.CONTENT_DIR.$location, 0777, true);
			mkdir(NON_HTTP_PATH.CONTENT_DIR.$location."thumb/", 0777, true);
			umask($oldmask);
		}
	}
}
?>