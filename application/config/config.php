<?php
/**
* Configuration File
*
**/
date_default_timezone_set('Europe/London');
//Error reporting during production
error_reporting(E_ALL);
ini_set("display_errors", 1);

//Base URL(with trailing slash)
define('URL', '');

//Folder locations
define('LIBS_PATH', 'application/libs/');
define('CONTROLLER_PATH', 'application/controllers/');
define('MODELS_PATH', 'application/models/');
define('VIEWS_PATH', 'application/views/');
define('CONTENT_DIR', 'content/');
define('THUMBDIR', CONTENT_DIR.'thumb/');
define('TEMP_DIR', 'temp/');
define('PUBLIC_IMAGES', 'public/images/');
define('NON_HTTP_PATH', $_SERVER['DOCUMENT_ROOT'].'/');

define('THUMB_WIDTH', 200);
define('THUMB_HEIGHT', 250);

//Cookie config
// 1209600 seconds = 2 weeks
define('COOKIE_RUNTIME', 1209600);
// the domain where the cookie is valid for, for local development ".127.0.0.1" and ".localhost" will work
// IMPORTANT: always put a dot in front of the domain, like ".mydomain.com" !
define('COOKIE_DOMAIN', '');

//Database config
define('DB_TYPE', 'mysql');
define('DB_USER', '');
define('DB_PASS', '');
define('DB_PATH', '');

//Hashing Config
/**
 * To get more information about the best cost factor please have a look here
 * @see http://stackoverflow.com/q/4443476/1114320
 */
define("HASH_COST_FACTOR", "10");
?>

