<?php 

/**
* Handles calls to tvdb library
*
**/

class Feeds extends Controller{
	public function __construct(){
		Auth::handleLogin();
		parent::__construct();
	}

	public function index(){
		$feeds_model = $this->loadModel('Feeds');
		//$book_model = $this->loadModel('Bookmarks');
		//$stacks_model = $this->loadModel('Stacks');
		//$this->view->cats = $stacks_model->getCategories();
		//$this->view->stacks = $stacks_model->getProjects();
		//$this->view->bookCats = $book_model->bookCategories();
		$this->view->feeds = $feeds_model->getFeeds();
		$this->view->feedCats = $feeds_model->getCategories();
		$this->view->render('feeds/index', true);
	}

	public function refreshFeeds(){
		$feeds_model = $this->loadModel('Feeds');
		$count = $feeds_model->getCount();
		if($this->edit_able){
			
			$feeds_model->refreshFeeds();
			
			echo json_encode(array('error' => false, 'msg' => 'feeds refreshed', 'count' => $count));
		}else{
			echo json_encode(array('error' => false, 'msg' => 'feeds refreshed', 'count' => $count));
		}
	}

	public function contentFeed($ID){
		if(isset($ID) && !empty($ID)){
			$feeds_model = $this->loadModel('Feeds');
			$this->view->content = $feeds_model->getContent($ID);
			$this->view->render('feeds/content', true);
		}else{
			//announce error
		}
	}	

	public function getCount(){
		$feeds_model = $this->loadModel('Feeds');
		$response = json_encode(array('count' => $feeds_model->getCount(), 'error' => false));
		echo $response;
	}

	public function lastItem(){
		$feeds_model = $this->loadModel('Feeds');
		echo json_encode(array('error' => false, 'item' => $feeds_model->lastItem()));
	}

	public function editFeeds(){
		$feeds_model = $this->loadModel('Feeds');
		$this->view->cats = $feeds_model->getCategories();
		$this->view->urls = $feeds_model->getURLS();
		$this->view->render('feeds/editfeeds', true);
	}

	public function markasRead($ID){
		if($this->edit_able){
			if(isset($ID) && !empty($ID)){
				$feeds_model = $this->loadModel('Feeds');
				$result = $feeds_model->markSingleRead($ID);
				if($result){
					echo json_encode(array('error' => false));
				}else{
					echo json_encode(array('error' => true));
				}
			}else{
				echo json_encode(array('error' => true));
			}
		}else{
			echo json_encode(array('error' => false));
		}
		
	}

	public function markmultiRead($option){
		if($this->edit_able){
			if(isset($option) && !empty($option)){
				$feeds_model = $this->loadModel('Feeds');
				if($option == 'Day'){
					echo json_encode(array('error' =>$feeds_model->markDayRead(), 'msg' => 'Marked'));
				}else if($option == 'Week'){
					echo json_encode(array('error' => $feeds_model->markWeekRead(), 'msg' => 'cool'));
				}else if($option == 'All'){
					echo json_encode(array('error' => $feeds_model->markAllRead(), 'msg' => 'cool'));
				}
			}
		}else{
			echo json_encode(array('error' => true, 'msg' => 'Cannot mark feeds as read in demo'));
		}
		
	}

	public function addFeed(){
		$add_catID = htmlspecialchars($_POST['add_feed']);
		$add_url = htmlspecialchars($_POST['add_url']);

		if($this->edit_able){
			if(isset($add_url) && strlen($add_url) > 0 && isset($add_catID) && is_numeric($add_catID)){
				$feeds_model = $this->loadModel('Feeds');
				$result = $feeds_model->add($add_catID, $add_url);
				if($result){
					echo json_encode(array('msg'=>'FeedChanged', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Adding feeds not accessible in demo.', 'error'=>true));
		}
		
	}

	public function editFeed(){
		if($this->edit_able){
			$edit_catID = htmlspecialchars($_POST['edit_feed']);
			$edit_url = htmlspecialchars($_POST['edit_url']);
			$edit_id = htmlspecialchars($_POST['edit_feed_ID']);

			if(isset($edit_url) && strlen($edit_url) > 0 && isset($edit_catID) && is_numeric($edit_catID) && isset($edit_id) && is_numeric($edit_id)){
				$feeds_model = $this->loadModel('Feeds');
				$result = $feeds_model->edit($edit_catID, $edit_url,$edit_id);
				if($result){
					echo json_encode(array('msg'=>'FeedChanged', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Cannot edit feeds in demo mode', 'error'=>true));
		}	
	}

	public function removeFeed(){
		if($this->edit_able){
			$delete_ID = htmlspecialchars($_POST['delete_feed_ID']);
			if(isset($delete_ID) && is_numeric($delete_ID)){
				$feeds_model = $this->loadModel('Feeds');
				$result = $feeds_model->delete($delete_ID);
				if($result){
					echo json_encode(array('msg'=>'FeedDeleted', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'FeedDeleted', 'error'=>false));
		}
	}
}