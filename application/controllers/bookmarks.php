<?php 

class Bookmarks extends Controller{
	public function __construct(){
		Auth::handleLogin();
		parent::__construct();
	}

	public function index(){
		$book_model = $this->loadModel('Bookmarks');
		//$stacks_model = $this->loadModel('Stacks');
		//$this->view->cats = $stacks_model->getCategories();
		//$this->view->stacks = $stacks_model->getProjects();
		$this->view->bookCats = $book_model->bookCategories();
		$this->view->bookmarks = $book_model->getBookmarks();
		$this->view->render('bookmarks/index', true);
	}

	public function checkXFrame(){
		$url = htmlspecialchars_decode($_POST['url']);

		if(isset($url)){

	        $error=false;
	        $ch = curl_init();

	        $options = array(
	                CURLOPT_URL            => $url,
	                CURLOPT_RETURNTRANSFER => true,
	                CURLOPT_HEADER         => true,
	                //CURLOPT_FOLLOWLOCATION => true,
	                CURLOPT_ENCODING       => "",
	                CURLOPT_AUTOREFERER    => true,
	                CURLOPT_CONNECTTIMEOUT => 120,
	                CURLOPT_TIMEOUT        => 120,
	                CURLOPT_MAXREDIRS      => 10,
	        );
	        curl_setopt_array($ch, $options);
	        $response = curl_exec($ch);
	        $httpCode = curl_getinfo($ch);
	        $headers = substr($response, 0, $httpCode['header_size']);

	        if(stripos($headers, 'X-Frame-Options: deny')>-1||strpos($headers, 'X-Frame-Options: SAMEORIGIN')>-1) {
	                $error = true;
	        }

	        $httpcode= curl_getinfo($ch, CURLINFO_HTTP_CODE);
	        curl_close($ch);
	        echo json_encode(array('httpcode'=>$httpcode, 'error'=>$error));
		}else{
		    echo json_encode(array('httpcode'=>'Empty', 'error'=>true));
		}
	}

	public function delete(){
		$delete_book_ID = htmlspecialchars($_POST['delete_book_ID']);
		if($this->edit_able){
			if(isset($delete_book_ID) && is_numeric($delete_book_ID)){

				$book_model = $this->loadModel('Bookmarks');
				$result = $book_model->removeBookmark($delete_book_ID );
				if($result){
					echo json_encode(array('msg'=>'Bookmark Deleted', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
				}
				
			}else{
				echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
			}	
		}else{
			echo json_encode(array('msg'=>'Bookmark Deleted', 'error'=>false));
		}
		
	}

	public function add(){
		$book_title = htmlspecialchars($_POST['title']);
		$book_url = htmlspecialchars($_POST['url']);
		$book_classid = htmlspecialchars($_POST['bookCat']);
		if($this->edit_able){
			if(isset($book_title) && !empty($book_title) && isset($book_url) && !empty($book_url) && isset($book_classid) && is_numeric($book_classid)){

				$book_model = $this->loadModel('Bookmarks');
				$result = $book_model->addBookmark($book_title, $book_url, $book_classid);

				if($result){
					echo json_encode(array('msg'=>'BookmarkChanged', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Adding bookmarks disabled in demo', 'error'=>true));
		}
		
	}

	public function edit(){
		$edit_book_ID = htmlspecialchars($_POST['edit_book_ID']);
		$edit_book_title = htmlspecialchars($_POST['edit_title']);
		$edit_book_url = htmlspecialchars($_POST['edit_link']);
		$edit_book_classid = htmlspecialchars($_POST['edit_bookCat']);
		if($this->edit_able){
			if(isset($edit_book_title) && !empty($edit_book_title) && isset($edit_book_url) && !empty($edit_book_url) && isset($edit_book_classid) && is_numeric($edit_book_classid)) {

				$book_model = $this->loadModel('Bookmarks');
				$result = $book_model->editBookmark($edit_book_ID, $edit_book_title, $edit_book_url, $edit_book_classid);

				if($result){
					echo json_encode(array('msg'=>'BookmarkChanged', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
				}
				
			}else{
				echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Editing bookmarks disabled in demo', 'error'=>true));
		}	
	}
}