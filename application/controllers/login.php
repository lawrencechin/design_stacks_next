<?php

/**
 * Login Controller
 *
**/

class Login extends Controller{
	function __construct(){
		parent::__construct();
	}

	public function index(){
		$login_model = $this->loadModel('Login');
		$this->view->render('login/index', false, true);
	}

	public function login(){
		$login_model = $this->loadModel('Login');
		//return true or false
		$login_successful = $login_model->login();

		if($login_successful){
			//success! redirect to index/homepage
            Application::app_new_location('index/index');
		}else{
			//boo-hiss, failure. Log in again
            Application::app_new_location('login/index');
		}
	}

	public function demo(){
		$_POST['user_name'] = "test12User";
		$_POST['user_password'] = "test12User";
		$login_model = $this->loadModel('Login');
		//return true or false
		$login_successful = $login_model->login();

		if($login_successful){
			//success! redirect to index/homepage
            Application::app_new_location('index/index');
		}else{
			//boo-hiss, failure. Log in again
            Application::app_new_location('login/index');
		}
	}

	public function logout(){
		$login_model = $this->loadModel('Login');
		$login_model->logout();
        Application::app_new_location('');
	}

	public function loginWithCookie(){
		$login_model = $this->loadModel('Login');
		$login_successful = $login_model->loginWithCookie();

		if($login_successful){
            Application::app_new_location('bookmarks/index');
		}else{
			$login_model->deleteCookie();
            Application::app_new_location('login/index');
		}
	}

    public function register(){
    	$login_model = $this->loadModel('Login');
    	$this->view->render('login/index', false, true);
    	//$this->view->render('login/register', false, true);
    }

    public function register_action(){
    	$login_model = $this->loadModel('Login');
    	$registration_successful = $login_model->registerNewUser();

    	if($registration_successful){
            Application::app_new_location('login/index');
    	}else{
            Application::app_new_location('login/register');
    	}
    }

    public function style($style = 0){
    	$style = (int)$style;
    	//must be set, numeric and either 1 or 0
    	if(is_numeric($style) && ($style === 1 || $style === 0)){
    		$login_model = $this->loadModel('Login');
    		$login_model->setStyle($style);
    	}
    }
}






