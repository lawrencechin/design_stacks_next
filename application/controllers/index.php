<?php 

class Index extends Controller{
	function __construct(){
		Auth::handleLogin();
		parent::__construct();
	}

	function index(){
		$index_model = $this->loadModel('Index');
		$book_model = $this->loadModel('Bookmarks');
		$stacks_model = $this->loadModel('Stacks');
		$this->view->cats = $stacks_model->getCategories();
		$this->view->stacks = $stacks_model->getProjects();
		$this->view->bookCats = $book_model->bookCategories();
		$this->view->render('index/index');
	}
}

?>