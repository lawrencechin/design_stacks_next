<?php 

class Stacks extends Controller{

	public function __construct(){
		Auth::handleLogin();
		parent::__construct();
	}

	public function index(){
		$stacks_model = $this->loadModel('Stacks');
		$this->view->stacks = $stacks_model->getProjects();
		$this->view->cats = $stacks_model->getCategories();
		$this->view->folder = $stacks_model->cropFolder()[0]->user_creation_timestamp.'/';
		$this->view->render('stacks/index', true);
	}

	public function stacksRefresh(){
		$stacks_model = $this->loadModel('Stacks');
		$this->view->stacks = $stacks_model->getProjects();
		$this->view->cats = $stacks_model->getCategories();
		$this->view->folder = $stacks_model->cropFolder()[0]->user_creation_timestamp.'/';
		$this->view->render('stacks/stacksrefresh', true);
	}

	public function refreshHeader(){
		$stacks_model = $this->loadModel('Stacks');
		$this->view->stacks = $stacks_model->getProjects();
		$this->view->render('stacks/refresh', true);
	}

	public function add(){
		if($this->edit_able){
			$addModelTitle = htmlspecialchars($_POST['add_model_title']);
			$addModelCat = htmlspecialchars($_POST['add_model_cat']);

			if(isset($addModelCat) && is_numeric($addModelCat) && !empty($addModelTitle)){

				$stacks_model = $this->loadModel('Stacks');
				$result = $stacks_model->addProject($addModelTitle,$addModelCat);
				if($result){
					echo json_encode(array('msg'=>'ModelChanged', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'That name already exists. Please change it.', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
			}
		}else{echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));}
	}

	public function edit(){
		if($this->edit_able){
			$editModelTitle = htmlspecialchars($_POST['edit_model_title']);
			$editModelCat = htmlspecialchars($_POST['edit_model_cat']);
			$editModelID = htmlspecialchars($_POST['edit_model_ID']);

			if(isset($editModelCat) && is_numeric($editModelCat) && isset($editModelTitle) && !empty($editModelTitle) &&isset($editModelID) && !empty($editModelID)){

				$stacks_model = $this->loadModel('Stacks');
				$result = $stacks_model->editProject($editModelTitle, $editModelCat, $editModelID);

				if($result){
					echo json_encode(array('msg'=>'ModelChanged', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
				}
			}else{
				//if variables aren't set
				echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));
		}	
	}

	public function delete(){
		if($this->edit_able){
			$deleteModelID = htmlspecialchars($_POST['delete_model_ID']);
			if(isset($deleteModelID) && is_numeric($deleteModelID)){
				$stacks_model = $this->loadModel('Stacks');
				$result = $stacks_model->deleteProject($deleteModelID);

				if($result){
					echo json_encode(array('msg'=>'ModelDeleted', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'1.There was an error communicating with database.', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'2.There was an error communicating with database.', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));
		}
	}

	public function project($id){
		if(isset($id) && is_numeric($id)){
			$stacks_model = $this->loadModel('Stacks');
			$this->view->project = $stacks_model->getImages($id);
			$this->view->notes = $stacks_model->getNotes($id);
			$this->view->projectID = $id;
			$this->view->favs = $stacks_model->getFavourites();
			$this->view->render('stacks/project', true);
		}
	}

	public function getNotes($id, $raw = false, $stacksMd = false){
		if(isset($id) && is_numeric($id)){

			if(!$raw){
				$stacks_model = $this->loadModel('Stacks');
				$notes = $stacks_model->getNotes($id);
				echo $notes[0]->notes;
			}else{
				$notes = $stacksMd->getNotes($id);
				$Parsedown = new Parsedown();
				$text = htmlentities($notes[0]->notes, ENT_QUOTES, 'UTF-8');
				return $Parsedown->text($text);
			}
		}
	}

	public function noteSyntax(){
		$this->view->render('stacks/noteSyntax', true);
	}

	public function editNotes(){
		if($this->edit_able){
			$id = $_POST['edit_note_ID'];
			if(isset($id) && is_numeric($id)){
				$stacks_model = $this->loadModel('Stacks');
				$notes = strip_tags(htmlspecialchars($_POST['noteval']));
				$result = $stacks_model->editNotes($id, $notes);
				if($result){
					echo json_encode(array('error' => false, 'msg' => 'NoteChanged', 'note' => $this->getNotes($id, true, $stacks_model)));
				}else{
					echo json_encode(array('error' => true, 'msg' => 'NoteNotChanged'));
				}
			}
		}else{
			echo json_encode(array('error' => true, 'msg' => 'NoteNotChanged'));
		}
	}

	public function addImages(){
		if($this->edit_able){
			$uploadedFiles = $_FILES['files'];
			$photo_add_id = htmlspecialchars($_POST['add_model_ID']);

			if(isset($uploadedFiles) && count($uploadedFiles) > 0 && isset($photo_add_id) && is_numeric($photo_add_id)){

				$stacks_model = $this->loadModel('Stacks');
				$result = $stacks_model->addImages($uploadedFiles, $photo_add_id);
				if($result){
					echo json_encode(array('msg'=>"FilesAdded", 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));
		}
	}

	public function editImages(){
		if($this->edit_able){
			$photo_move_model_id = htmlspecialchars($_POST['add_model_ID']);
			$photo_array = $_POST['IDArray'];

			if(isset($photo_array) && count($photo_array) > 0 && is_numeric($photo_move_model_id)){
				$stacks_model = $this->loadModel('Stacks');
				$result = $stacks_model->editImages($photo_move_model_id, $photo_array);
				if($result){
					echo json_encode(array('msg'=>'FilesChanged', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'No files selected. Please try again', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'No files selected. Please try again', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));
		}
	}

	public function deleteImages(){
		if($this->edit_able){
			$photo_array = $_POST['IDArray'];
			if(isset($photo_array) && !empty($photo_array)){
				$stacks_model = $this->loadModel('Stacks');
				$result = $stacks_model->deleteImages($photo_array);
				if($result){
					echo json_encode(array('msg'=>'FilesChanged', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'1.There was an error with your form. Please try again.', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'2.There was an error with your form. Please try again.', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));
		}
	}

	public function crop(){
		$thumbArr = $_POST['thumbArr'];
		$imgArr = $_POST['imgArr'];
		$id = $_POST['id'];

		if(isset($thumbArr) && !empty($thumbArr) && isset($imgArr) && !empty($imgArr) && isset($id) && !empty($id)){
			$stacks_model = $this->loadModel('Stacks');
			$this->view->folder = $stacks_model->cropFolder();
			$this->view->thumbArr = $thumbArr;
			$this->view->imgArr = $imgArr;
			$this->view->id = $id;
			$this->view->render('stacks/crop', true);
		}
	}

	public function cropImg(){
		$cropX = htmlspecialchars($_POST['x_cor']);
		$cropY = htmlspecialchars($_POST['y_cor']);
		$cropW = htmlspecialchars($_POST['w_cor']);
		$cropH = htmlspecialchars($_POST['h_cor']);
		$currentImage = htmlspecialchars($_POST['image_cor']);

		if(isset($cropX) && !empty($cropX) && isset($cropY) && !empty($cropY) && isset($cropH) && !empty($cropH) && isset($currentImage) && !empty($currentImage) && isset($cropW) && !empty($cropW)){

			$imgArr = explode('/', $currentImage);

			$img = $imgArr[3].'/'.$imgArr[4].'/'.$imgArr[5].'/'.$imgArr[6].'/'.$imgArr[7];

			$stacks_model = $this->loadModel('Stacks');
			$result = $stacks_model->cropImg($cropX, $cropY, $cropW, $cropH, $img);
			
			if($result[0]){
				echo json_encode(array('error' => false, 'value' => $result[1]));
			}else{
				echo json_encode(array('error' => true, 'msg' => 'There was an error.'));
			}
		}
	}

	public function acceptCrop(){
		$projectID = htmlspecialchars($_POST['projectID']);

		if(isset($projectID) && !empty($projectID)){

			$stacks_model = $this->loadModel('Stacks');
			$result = $stacks_model->cropReview($projectID);
			if($result[0]){
				echo json_encode(array('error' => true, 'img' => $result[1], 'replace' => true));
			}else{
				echo json_encode(array('msg' => 'There was an error'));
			}
		}
	}

	public function temp(){
		$stacks_model = $this->loadModel('Stacks');
		$result = $stacks_model->temp();
		if($result[0]){
			$this->view->files = $result[1];
			$this->view->stacks = $stacks_model->getProjects();
			$this->view->path = $result[2];
			$this->view->render('stacks/temp', true);
		}
	}

	public function favs(){
		$stacks_model = $this->loadModel('Stacks');
		$this->view->favs = $stacks_model->getFavourites();
		$this->view->render('stacks/wallpaper', true);
	}

	public function getBckImg(){
		$stacks_model = $this->loadModel('Stacks');
		$result = $stacks_model->getFavourites();
		if(count($result) > 0){
			$wallpaper = array_rand($result, 1);
			if(NULL === (SESSION::get('user_wallpaper'))){
				SESSION::set('user_wallpaper', URL.CONTENT_DIR.locatGen::gen($result[$wallpaper]->IMGname).$result[$wallpaper]->IMGname);
			}

			echo SESSION::get('user_wallpaper');
		}else{
			SESSION::set('user_wallpaper', URL.PUBLIC_IMAGES.'loader.png');
			echo SESSION::get('user_wallpaper');
		}
		
	}

	public function setWallpaperSession(){
		if($_POST['wallpaper'] !== NULL){
			$stacks_model = $this->loadModel('Stacks');
			$result = $stacks_model->setWallpaper($_POST['wallpaper']);
			if($result){
				echo 'Changed: yes : ' . Session::get('user_wallpaper');
				return false;
			}
			echo 'Changed: yes : ' . Session::get('user_wallpaper');
		}
	}

	public function favAdd(){
		$photoID = htmlspecialchars($_POST['PhotoID']);
		if(isset($photoID) && is_numeric($photoID)){
			$stacks_model = $this->loadModel('Stacks');
			$result = $stacks_model->favouritesAdd($photoID);
			if($result){
				echo json_encode(array('error' => false));
			}
		}else{
			echo json_encode(array('error' => true));
		}	
	}	

	public function favRemove(){
		if($this->edit_able){
			$photoID = htmlspecialchars($_POST['PhotoID']);
			if(isset($photoID) && is_numeric($photoID)){
				$stacks_model = $this->loadModel('Stacks');
				$result = $stacks_model->favouritesRemove($photoID);
				if($result){
					echo json_encode(array('error' => false));
				}
			}else{
				echo json_encode(array('error' => true));
			}
		}else{echo json_encode(array('error' => true));}
	}

	public function deleteTemp(){
		if($this->edit_able){
			$fileArr = htmlspecialchars($_POST['fileArray']);
			if(isset($fileArr) && !empty($fileArr)){
				$stacks_model = $this->loadModel('Stacks');
				$result = $stacks_model->deleteTemp($fileArr);
				if($result[0]){
					echo json_encode(array('msg'=>'FilesDeleted', 'error'=>false, 'desc' => $result[1]));
				}
			}else{
				echo json_encode(array('msg'=>'Could not delete files', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));
		}
	}

	public function addTemp(){
		if($this->edit_able){
			$fileArr = $_POST['fileArray'];
			$ProjectID = $_POST['ProjectID'];
			if(isset($fileArr) && !empty($fileArr) && isset($ProjectID) && is_numeric($ProjectID)){
				$stacks_model = $this->loadModel('Stacks');
				$result = $stacks_model->addTemp($fileArr, $ProjectID);
				if($result){
					echo json_encode(array('msg'=>'FilesDeleted', 'error'=>false));
				}
			}else{}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));
		}	
	}
}


