//simple scrollTo plugin via Timothy Perez - http://lions-mark.com/jquery/scrollTo/
$.fn.scrollTo = function( target, options, callback ){
  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
  var settings = $.extend({
    scrollTarget  : target,
    offsetTop     : 50,
    duration      : 500,
    easing        : 'swing'
  }, options);
  return this.each(function(){
    var scrollPane = $(this);
    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
      if (typeof callback == 'function') { callback.call(this); }
    });
  });
};

var globals = {

	globalHeader: $('#mainHeader'),
	globalLogo: $('#logo'),
	globalModelLink: null,
	globalWallPaperStuff: null,
	//store last clicked button based on location - used to trigger close buttons
	headerButton : null,
	midButton: null,
	contentButton: null,
	body: $('body'),
	loading : $('#loading_and_thumbs'),
	acceptableExt : ['mp4', 'm4v', 'jpeg', 'jpg', 'gif', 'png', 'tiff'],
	mediaQuery : window.matchMedia( "(max-width : 31.25em)" ),
	mobileNav : $('#expose_menus')

};

//Contains all functionality regarding forms. Nb. We previously used a form plugin for file uploads and a seperate object containing functions. Now we're using formData and doing everything here sans plugin. Cool.
var formSubmission = {

	cancelBtn : null,

	revealForm: function(button){
		//wrap button in 'query, use button to find corresponding form, remove active class from button
		var $button = $(button).removeClass('activeButton'),
		formID = '#'+ $button.attr('class')+'_form',
		buttonType = parseInt($button.attr('data-button')),
		form = globals.body.find(formID),

		buttonGlobal = buttonType === 1 ? 'headerButton' : buttonType === 2 ? 'midButton' : buttonType === 3 ? 'contentButton' : '';

		showForm(form, $button, buttonGlobal);

		if(buttonType === 2 || buttonType === 3){
			formSubmission.fillOutForm($button, form);
		}

		function hideForm(form, buttonG, buttonC, buttonGlobal){
			//toggle form display and toggle active button and remove selectList menu

			if(buttonG === null){
				//global not set
				return true;
			}else if(buttonG[0] === buttonC[0]){
				//if global and clicked are same
				form.removeClass('displayForm').removeClass('focusForm');
				buttonG.removeClass('activeButton');
				globals.body.find('.selectConvert').remove();
				setGlobal(button, buttonGlobal, false);
				return false;
			}else{
				buttonG.removeClass('activeButton');
				var formOldId = '#'+buttonG.attr('class')+'_form';
				globals.body.find(formOldId).removeClass('displayForm');
				globals.body.find('.selectConvert').remove();

				return true;
			}	
		}

		function showForm(form, button, buttonGlobal){
			if(hideForm(form, globals[buttonGlobal], button, buttonGlobal)){
				//check for mediaquery, check if a midbutton is clicked
				if(globals.mediaQuery.matches){
					if(buttonType === 2){
						globals.globalHeader.removeClass('mobileShiftMenu');
					}
					
				}

				form.addClass('displayForm').addClass('focusForm');
				button.addClass('activeButton');
				setGlobal(button, buttonGlobal, true);
				//convert select lists into a nicer looking form
				formSubmission.selectLists(form, true);
				header.revealMenu();
			}
		}

		function setGlobal(button, buttonGlobal, flag){
			//store last clicked button in global variable if different from previous click
			if(flag){
				globals[buttonGlobal] = button;
			}else{
				globals[buttonGlobal] = null;
			}
		}	
	},

	fillOutForm : function(button, form){
		var dataHolder = button.siblings()[0],
		dataF;
		if(dataHolder.tagName.toLowerCase() === 'div'){
			dataF = $(dataHolder).children();
		}else{
			dataF = $(dataHolder);
		}

		if(dataF.get(0).tagName.toLowerCase() === 'form'){
		}else{
			var dataAttr = dataF.data();
			if(dataAttr.location === 'stacks'){
				stacks.formEntry(dataF, dataAttr, form);
			}else if(dataAttr.location === 'default'){
				bookmarks.formEntry(dataF, dataAttr, form);
			}else{
				feeds.formEntry(dataF, dataAttr, form);
			}
		}
	},

	selectLists: function(form, flag){
		//HTML fragments to build the node
		var selectToList = $('<ul class="selectConvert">'),
		count = 0,
		searchbox = $('<div class="searchBar"><input type="text" name="search_models" placeholder="Search Name…"><button></button></div>');

		if(flag){
			form.find('select option').each(function(){
				var $this = $(this);

				if($this.data('catsort') === undefined){
					selectToList.append('<li class="'+$this.data('catcolour')+'"><a href="'+ $this.val()+'">'+ $this.text() +'</a></li>');
					count++;

				}else{
					selectToList.append('<li class="'+$this.data('catsort')+'"><a href="'+ $this.val()+'">'+ $this.text() +'</a><span>'+$this.data('catsort')+'</span></li>');
					count++;
				}
			});

			if(count > 12){
				selectToList.prepend(searchbox); 
				selectToList.liveFilter(searchbox.find('input'), 'li');
				selectToList.find('button').on('click', function(e){
					e.preventDefault();
					globals.body.find('.selectConvert').empty().remove();
					formSubmission.selectLists(form, true);
				});
			}

			form.find('.selectList').off().on('click', function(e){
				e.preventDefault();
				if(count > 12){
					selectToList.addClass('largeList');
				}
				
				globals.body.append(selectToList);
				globals.body.find('.selectConvert').off().on('click', 'a', function(e){

					e.preventDefault();
					var selectOption = $(this).attr('href'),
					selectText = $(this).text();

					form.find('a.selectList').text(selectText);
					formSubmission.selectLists(form, true);

					form.find('select').val(selectOption);
					globals.body.find('.selectConvert').empty().remove();
				});
			});
		}
	},

	deleteInlineForm: function(button){
		var $button = $(button),
		btnClass = $button.attr('class'),
		formID = '#'+btnClass+"_form",
		link = $button.parent().find('a:first'),
		deleteID = link.attr('data-id');

		if(btnClass.indexOf('_feed') > 0){
			link = $button.parent().find('span');
			deleteID = link.attr('data-id');
		}

		function removeBookmark(){
			$button.parent().remove();
		}

		if(confirm('Are you sure?')){
			globals.body.find(formID).find('[name*="_ID"]').val(deleteID).end().find('[type="submit"]').trigger('click');
			$button.addClass('activeButton');
			$button.parent().addClass('bookmark_remove');
			setTimeout(removeBookmark, 700);
		}
	},

	submitForms: function(nameFragment, flag){
		globals.body.find('form[id*="'+nameFragment+'"]').submit(function(e){
			e.preventDefault();
			var $this = $(this),
			action = $this.attr('action'),
			data;
			formSubmission.cancelBtn = $this.find('[name^="cancel_"]');
			if(flag){
				data = new FormData(document.forms.namedItem($this.attr('name')));
			}else{
				data = projects.formData($this);
			}

			formSubmission.postForms(action, data);
			
		});
	},

	postForms : function(url, data){
		var snp = globals.loading.find('#shallNotPass');
		snp.show().addClass('loading');
		globals.loading.fadeIn();

		$.ajax({
		  	type: "POST",
		  	url: url,
			data: data,
			processData: false,  // tell jQuery not to process the data
  			contentType: false })  // tell jQuery not to set contentType
		  	//check for errors
			.done(function(response){
				snp.removeClass('loading').addClass('success');

				setTimeout(function(){
					snp.hide().removeClass();
					globals.loading.fadeOut();
					snp.find('p').text('Please Wait');
				}, 600);
				console.log(response);
			  	var json = $.parseJSON(response);
			  	if(json.error){
			  		snp.removeClass('loading').addClass('error');
					alert(json.msg);
				}else{
					//make the form vanish upon success
					if(formSubmission.cancelBtn !== null){
						formSubmission.cancelBtn.trigger('click');
					}
					//actions to take depending on response
					formSubmission.formSuccess(json);
				}
			});
	},

	formSuccess : function(json){
		switch(json.msg){
			case 'BookmarkChanged' :
				if(globals.body.find('#section_bookmarks').length){
					globals.globalHeader.find('#bookmarks').find('a:first').trigger('click');
				}
			break;

			case 'ModelChanged' :
				if(globals.body.find('#section_categories').length){
					globals.globalHeader.find('#stack').find('a:first').trigger('click');
					header.refreshHeader('#move_photo_projects');
				}else if(globals.body.find('#temp_files').length){
					header.refreshHeader('#dir_list');
					formSubmission.selectLists($('#dir_list'), true);
				}
				header.refreshHeader('#add_file_form');
			break;

			case 'ModelDeleted' : 
				header.refreshHeader('#add_file_form');
				if(globals.body.find('#section_categories').length){
					header.refreshHeader('#add_file_form');
				}
			break;

			case 'FilesAdded' : 
				if(globals.body.find('#section_cat_content').length && globals.globalModelLink !== null){
					globals.globalModelLink.trigger('click');
				}else if(globals.body.find('#temp_files').length){
					temp.tempRemoveFiles(temp);
				}
				if(json.info.length > 0){
					alert(json.info); 
				}
			break;

			case 'FilesChanged' : 
				temp.tempRemoveFiles(projects);
			break;

			case 'FilesDeleted' : 
				temp.tempRemoveFiles(temp);
				if(json.info.length > 0){
					alert(json.info); 
				}
			break;

			case 'FeedChanged' : 
			$('#feed_control').find('button:nth-child(3)').trigger('click');
			break;
		}
	},

	closeForm : function(button){
		var $button = $(button),
		buttonToTrigger = $button.attr('data-cancel') === 'global' ? 'headerButton' : $button.attr('data-cancel') === 'midForm' ? 'midButton' : $button.attr('data-cancel') === 'contentForm' ? 'contentButton' : false;
		if(globals[buttonToTrigger] === null){

		}else{
			globals[buttonToTrigger].trigger('click');
		}
		
	},

	checkFileTypes : function(input){
		function hasExtension(fileName, exts) {
		    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
		}

		$input = $(input);
		$input.off().on('change', function(){
			var inputRaw = $input.get(0),
			fileArray = [];

			//for every file...
			for(var x = 0; x < inputRaw.files.length; x++) {
				if(!hasExtension(inputRaw.files[x].name, globals.acceptableExt)){
					fileArray.push(inputRaw.files[x].name);
				}
			}
			if(fileArray.length > 0){
				alert('You have selected incompatible file types. Acceptable file types are: '+globals.acceptableExt+'. The following files did not match: '+fileArray);
			}
		});	
	}

};

//Contains functions that control basic page loading
var header = {
	clickedStack : '',
	//functions that run on page load
	init : function(){
		header.loadContent();
		header.homeButton();
		formSubmission.submitForms('_form', true);
		wallpapers.fontSelection();
		header.refreshFeedCount(true);
		formSubmission.checkFileTypes('#fileUpload');
		Mousetrap.bind('b o o k', function() { 
			globals.globalHeader.find('.add_bookmark').trigger('click');
		});
		Mousetrap.bind('u p', function() { 
			globals.globalHeader.find('.add_file').trigger('click');
		});
		Mousetrap.bind('n e w', function() { 
			globals.globalHeader.find('.add_model').trigger('click');
		});

		//checks if we're in a specific media query
		if(globals.mediaQuery.matches){
			globals.globalHeader.find('#globalForms').after($('#wallpapers'));
			header.mobileMenu();
		}else{
			//grab the background image. Don't want to load this if on mobile
			$.get('./php/wallpapers.php?bckImg=true', function(data){
				$('img', '#bg').attr('src', data);
			});
		}
	},

	homeButton : function(){
		globals.globalLogo.on('click', function(){
			//globals.body.
			header.removeContent();
			header.mobileControl('','', true);
			//remove styles specfic to sections
			globals.body.removeClass('stacksSidebar');
			header.revealMenu(true);
			globals.body.find('.selectConvert').remove();

			mediaView.slideShowOff($('#slideShowButton'));
			mediaView.killVideo();
		});	
	},

	loadContent : function(flag, link){

		function showContent(data){
			if(globals.mediaQuery.matches){
				globals.globalHeader.after(data);
				globals.globalHeader.addClass('mobileShiftMenu');
				header.mobileControl('','', true);
			}else{
				$('#wallpapers').after(data);
			}
			globals.globalLogo.find('svg').removeClass('logo_animation');
			globals.body.find('.selectConvert').remove();
		}

		function finalLoad(link){
			$.ajax({
				type : 'GET',
				url : link })
				.done(function(data){
					showContent(data);
				});
		}

		if(flag){
			header.removeContent(true);
			finalLoad(link);
		}else{
			//load content into main section
			globals.globalHeader.on('click', 'a.navigation', function(e){
				e.preventDefault();
				//remove previous content
				header.removeContent();

				mediaView.slideShowOff($('#slideShowButton'));
				mediaView.killVideo();

				//remove styles specfic to sections
				globals.body.removeClass('stacksSidebar');
				header.revealMenu(true);

				//add class for logo animation
				globals.globalLogo.find('svg').addClass('logo_animation');

				finalLoad($(this).attr('href'));
			});
		}
	},

	loadExternalLinks : function(container){
		globals.body.find(container).on('click', 'a.external', function(e){
			e.preventDefault();
			var $this = $(this),
			location = $this.data();

			//internal function to load content through different mechanisms
			function loadContent(cont, link, location, data){
				var url = $(link).attr('href'),
				$container = $(cont).next('div').find('#content');
				$container.load(url, data, function(){
					globals.globalLogo.find('svg').removeClass('logo_animation');
					globals.body.find('button.display_photos').trigger('click');
					globals.body.scrollTo(0);

					if(feeds.editBtn !== null){
						feeds.editBtn.removeClass();
					}

					if(globals.mediaQuery.matches){

						if(location === 'stacks'){
							header.mobileControl(cont, '.expose_stacks');
						}else if(location === 'feedContent'){
							header.mobileControl(cont, '.expose_feeds');
						}
					}
				});
			}

			//add loading animation
			globals.globalLogo.find('svg').addClass('logo_animation');
			if(location.location === "stacks"){
				var data = "ModelID="+location.id;
				loadContent(container, $this, location.location, data);
				var editbtn = $(container).siblings('div').find('button.editProjectFiles');
				if(editbtn.hasClass('activeButton')){
					editbtn.trigger('click');
				}
				globals.globalModelLink = $this;
				globals.body.addClass('stacksSidebar');
				//store parent location so we can scroll to it upon clicking and when we close the window
				header.clickedStack = $this.parent('div');
				setTimeout(function(){
					$(container).scrollTo(header.clickedStack);
				}, 1000);

			}else if(location.location === "feedContent"){
				loadContent(container, $this, location.location);
				feeds.markAsRead($this);
			}else{
				header.loadExternal($this.attr('href'));
				if(globals.mediaQuery.matches){
					header.mobileControl(container, '.expose_books');
				}
				
			}
		});
	},

	loadExternal : function(link){
		//first we check the x-frame status of the website. If positive we then load link in iframe or throw an error message
		var url = 'php/bookmarks/getHeader.php';

		$.ajax({
			url: url,
		  	data: {'url' : link}, 
		  	dataType: 'json',
		  	timeout: 10000 })
		  	.done(function(response){
			  	if(response.error){
			  		alert('Page unable to load in iframe');
			  		globals.globalLogo.find('svg').removeClass('logo_animation');
			  	}else{
			  		var iframe = $('#contentFrame');
			  		iframe.attr('src', link);
			  		globals.body.addClass('stacksSidebar');
			  		globals.globalLogo.find('svg').removeClass('logo_animation');
			  		var input = iframe.parent('div').siblings('header').find('input');
			  		input.val(iframe.attr('src'));
			  	}
			})
			.fail(function(){
			  	alert('There was an error fetching the site');
			});
	},

	mobileMenu : function(){
		globals.mobileNav.on('click', 'a', function(e){
			e.preventDefault();
			var $this = $(this),
			showClass = 'mobileShiftMenu';

			if($this.parent().hasClass('expose_main')){
				globals.globalHeader.toggleClass(showClass);
			}else if($this.parent().hasClass('expose_books')){
				$('#section_bookmarks').toggleClass(showClass);
			}else if($this.parent().hasClass('expose_stacks')){
				$('#section_categories').toggleClass(showClass);
			}else{
				$('#section_feeds').toggleClass(showClass);
			}
		});
	},

	mobileControl : function(shiftCont, button, flag){
		//function to hide secondary menu and reveal the, um, reveal button
		var exposedBtns = globals.mobileNav.find('.revealExposeBtn');
		if(flag){
			exposedBtns.removeClass('revealExposeBtn');
		}else{
			$(shiftCont).addClass('mobileShiftMenu');
			exposedBtns.removeClass('revealExposeBtn').end().find(button).addClass('revealExposeBtn');
		}
		
	},


	//Update Feeds badge every five minutes by callling a php page that returns the Feed_List count
	refreshFeedCount : function(flag){
		var feedLink = globals.globalHeader.find("#rss").find("a"),
		url = "./php/rss/feedCount.php";

		$.ajax({
		  type: 'GET',
		  url: url })
		  .done(function(resp){
	  		feedLink.attr('data-feedcount', resp);
	  		if(flag){
	  			setTimeout(header.refreshFeedCount, 300000, true);
	  		}
		  })
		  .fail(function() {
		  	alert('Could not retrieve unread feed count');
		});
	},
	//when adding/editing new design projects in various places the global add file forms select lists will not be updated. This function fixes that. 
	refreshHeader : function(form){
		//we have a part of the header pertaining to the select lists in a php file
		function headerStyle(data){
		    globals.body.find(form).find('select').replaceWith(data);
		}
		//we grab that data, store it in a div and replace it in the form. Simple. 	
		$.get('./php/headerRefresh.php', headerStyle);
	},

	removeContent : function(flag){
		if(flag){
		}else{
			globals.globalHeader.removeClass('mobileShiftMenu');
		}
		if(globals.mediaQuery.matches){
			loadedContent = globals.body.contents(':gt(2)');
		}else{
			loadedContent = globals.body.contents(':gt(3)');
		}
		
		//loop through list and remove everything upto footer or nothing if only footer exists
		function remove(){
			if(loadedContent.length > 1){
				for(var i = 0; i < loadedContent.length-2; i++){
					loadedContent[i].remove();
				}
			}	
		}
		
		setTimeout(remove, 300);
	},

	revealMenu : function(flag){
		var $button = $('#revealMenu');

		if(flag){
			globals.body.removeClass('reveal');
			$button.addClass('activeButton');
		}else if(!globals.body.hasClass('stacksSidebar')){
			//don't add class in this event
			$button.removeClass('activeButton');
		}else{
			globals.body.toggleClass('reveal');
			$button.toggleClass('activeButton');
		}
	},

	close : function(flag){
		mediaView.slideShowOff($('#slideShowButton'));
		mediaView.killVideo();

		if(flag){
			globals.body.find('#content').empty();
			globals.globalModelLink = '';
			if(header.clickedStack !== ''){
				setTimeout(function(){
					$('#section_categories').scrollTo(header.clickedStack);
				}, 600);
			}
		}
		globals.body.find('#content').find('#contentFrame').attr('src', "about:blank");

		globals.body.removeClass('stacksSidebar');
	},

	iframeChangeLocation : function(button){
		var $button = $(button),
		locations = $button.siblings('input').val();

		header.loadExternal(locations);
	}
};

//lazyload object 
var lazyLoad = {

	midContainer : '',
	midImage : '',
	midImgCont : '',
	contentImage: '',
	contentImgCont: '',

	init : function(image, imageContainer, container, flag){
		if(flag){
			this.midContainer = $(container);
			this.midImage = image;
			this.midImgCont = imageContainer;

			$(this.midImage, this.midImageContainer).lazyload({
				container : lazyLoad.midContainer
			});
		}else{
			this.contentImage = image;
			this.contentImgCont = imageContainer;

			$(this.contentImage, this.contentImgCont).lazyload({
				skip_invisible : false,
				container : globals.body
			});
		}
		
	},
	//when changing the order of images lazyload stops working correctly so we hit it up again
	triggerLoad : function(flag){
		if(flag){
			lazyLoad.init(this.midImage, this.midImgCont, this.midContainer, flag);
		}else{
			lazyLoad.init(this.contentImage, this.contentImgCont, '', flag);
		}
	}

};

var bookmarks = {
	//toggle classes of divs
	bookMenu : function(){
		var bookMenu = $('#bookmark_menu');
		bookMenu.on('click', 'a:not(.external)', function(e){
			e.preventDefault();
			var $this = $(this);
			//check if anything else has active class and remove except itself
			function loopList(container, link){
				container.find('div[class^="bookMenu_"]').each(function(){
					if(link.is($(this))){
					}else if($(this).hasClass('activeBook')){
						$(this).removeClass('activeBook');
						$(this).parents('#section_bookmarks').toggleClass('noScroll');
					}
				});
			}

			loopList(bookMenu, $this.parent('div'));

			//toggle class and scroll to top
			$this.parent('div').toggleClass('activeBook').end().parents('div#section_bookmarks').toggleClass('noScroll').scrollTo(0);
		});
	},
	//grab potential name and url from iframe and trigger the add bookmark button in header with those details
	bookmarkAdd : function(button){
		var $button = $(button),
		url = $button.siblings('input').val(),
		worldwide = url.indexOf('www'),
		index1 = null, index2 = null;

		if(worldwide > -1){
			index1 = url.indexOf('.')+1;
			index2 = url.indexOf('.', index1);
		}else{
			index1 = 7;
			index2 = url.indexOf('.', index1);
		}
		
		var name = url.substring(index1, index2);

		globals.globalHeader.find('#globalForms').find('.add_bookmark').trigger('click');

		globals.globalHeader.find('#add_bookmark_form').find('input[name="title"]').val(name).end().find('input[name="url"]').val(url);
	},

	formEntry : function(dataF, dataAttr, form){
		form.find('[name="edit_book_ID"]').val(dataAttr.id);
		form.find('[name="edit_title"]').val(dataAttr.title);
		form.find('[name="edit_link"]').val(dataF.attr('href'));
	}
};

var feeds = {
	editBtn : null,

	sort : function(button){
		var $button = $(button),
		menu = $button.siblings('ul:first'),
		stacksInput = $button.siblings('input');

		menu.toggleClass('show');
		if(stacksInput.hasClass('show')){
			stacksInput.removeClass('show').addClass('showUl');
		}else if(stacksInput.hasClass('showUl')){
			stacksInput.removeClass('showUl').addClass('show');
		}
		//remove click handler first otherwise we get multiple handlers each time the sort button is clicked, not ideal!
		menu.off().on('click', 'a', function(e){
			e.preventDefault();
			var $this = $(this),
			container = $button.parent('header').siblings('div');

			//removes all active classes
			function removeClass(){
				menu.find('.feedCat-1, .Graphics').removeClass('active').end().find('a').removeClass('active');
			}
			//hides all elements then displays the relevant items
			function displayFeeds(displayClass, container){
				container.find('div').hide().end().find('div'+displayClass).show();
				stacks.init();
				//trigger lazyload only if it has already been initialised
				if(lazyLoad.container !== ''){
					lazyLoad.triggerLoad(true);
				}
			}
			//different events if link clicked is already active: remove the sort
			if($this.hasClass('active')){
				removeClass();
				displayFeeds('', container);
				$button.removeClass('activeButton');
				menu.removeClass('show');
			}else{
				removeClass();
				$button.addClass('activeButton');
				if($this.attr('id') === undefined){
					displayFeeds('.'+$this.attr('class'), container);
				}else{
					displayFeeds('.'+$this.attr('id'), container);
				}
				
				$this.addClass('active');

				if($this.parent('li').hasClass('feedCat-1') || $this.parent('li').hasClass('Graphics')){
					$this.parent('li').addClass('active');
				}
			}
			
		});
	},

	edit : function(button){
		if(globals.mediaQuery.matches){
			header.mobileControl('#section_feeds', '.expose_feeds');
		}
		
		var $button = $(button),
		url = './php/rss/editFeeds.php',
		content = $('div#content');

		this.editBtn = $button;

		$button.addClass('activeButton');

		globals.body.removeClass('stacksSidebar');

		content.off().load(url, function(){
			var editCont = content.find('#edit_cont');
			editCont.append(editCont.find('ul[class*="feed-"]'));
			editCont.off().on('click', 'h3 > a', function(e){
				e.preventDefault();
				editCont.find('ul[class*="feed-"]').hide().end().find('ul.'+$(this).attr('href')).show();
			});
		});
	},

	formValidate : function(input){
		var $input = $(input);
		if($input.attr('name') === 'edit_url'){
			$input.siblings('[name*="submit"]').prop('disabled', false);
		}
		$input.off().on('keyup', function(){
			var testValue = $(this).val();
			if(testValue.indexOf('http://') < 0){
				$input.siblings('[name*="submit"]').prop('disabled', true);
			}else{
				$input.siblings('[name*="submit"]').prop('disabled', false);
			}
		});
	},

	markAsRead : function(link){
		var ID = link.attr('data-id'),
		container = link.parents('.feedArticle'),
		url = "./php/rss/markasread.php?ID="+ID;

		feeds.readArticle();

		$.ajax({
			url : url,
			dataType : 'json',
			type : "GET" })
			.done(function(resp){
				//var json = $.parseJSON(resp);
				if(resp.error){
					alert(resp.msg);
				}else{
					container.addClass('markedRead');
					var feedCount = globals.globalHeader.find('#rss').find('a'),
					fCount = feedCount.attr('data-feedcount'),
					newCount = parseInt(fCount - 1);
					feedCount.attr('data-feedcount', newCount);
				}
			})
			.fail(function(){
				alert('There was an error marking item as read.');	
			});
	},

	getNew : function(button){
		//cache button, feed link in header, and get the last feed id displayed
		var $button = $(button),
		feedLink = globals.globalHeader.find("#rss").find('a'),
		url = "./php/rss/lastItem.php",
		lastFeedID = $button.parents('header').siblings('div').find('.feedArticle:first-child').find('h3').find('a').attr('data-ID');

		//start animation
		$button.addClass('animated');
		//go to lastItem.php and check what the last item is on the server. 
		function repeatGetNew(){
			$.ajax({
				type: 'GET',
				url: url })
				.done(function(resp) {
				  	testFeed(resp);
				})
			  	.fail(function() {
				  	alert('Could not retrieve unread feed count');
				  	$button.addClass('animated');
				});
		}
		//Compare IDs and run functions based on result
		function testFeed(resp){
			//no response - all feeds read
			if(!resp.length){
				getLess();
			//nothing more current in db so hit the feeds 	
			}else if(resp === lastFeedID){
				getLess();
			//more in database so simply load them in rather than an expensive call to feeds	
			}else{
				getMore();
			}
		}
		//If there are more items in database then simply trigger a click on feed link
		function getMore(){
			feedLink.trigger('click');
		}
		//Otherwise run the index.php and refresh the feeds
		function getLess(){
			var url = "./php/rss/index.php?password=chang01ChanG",
			snp = globals.loading.find('#shallNotPass');

			snp.show().addClass('loading');
			globals.loading.fadeIn();

			$.ajax({
				type: 'GET',
				url: url })
				.done(function(resp){
				  	var json = $.parseJSON(resp);
				  	snp.removeClass().addClass('success');

				  	setTimeout(function(){
						snp.hide().removeClass();
						globals.loading.fadeOut();
					}, 600);

				  	//check for error
				  	if(json.error){
				  		$button.removeClass('animated');
				  		alert('Something went awry :(. Msg: ' +resp.msg);
				  	//if the response is greater than current feed count then refresh 		
				  	}else if(parseInt(json.count) > parseInt(feedLink.attr('data-feedcount'))){
			  			getMore();
			  			header.refreshFeedCount(false);
				  	}else{
				  		alert('No new feeds');
				  		$button.removeClass('animated');
				  	}
			  	})
			  	.fail(function(){
				  	alert('Could not retrieve unread feed count');
				  	$button.removeClass('animated');
				});
		}

		//first check if there are any new feeds in the database
		repeatGetNew();
	},

	markAllAsRead : function(button){
		var $button = $(button),
		menu = $button.siblings('#markFeeds');

		$button.toggleClass('activeButton');
		menu.toggleClass('show');

		menu.off().on('click', 'a', function(e){
			e.preventDefault();
			url = './php/rss/markasread.php?All='+$(this).attr('data-option');
			var confirmation = confirm('Are sure?');
			getRead(url, confirmation);
		});

		function getRead(url, confirm){
			if(confirm){
				$.ajax({
					url : url,
					type : "GET" })
				.done(function(resp){
					var json = $.parseJSON(resp);
					if(json.error){
						alert(json.msg);
					}else{
						header.refreshFeedCount(false);
						globals.globalHeader.find("#rss").find('a').trigger('click');
					}
				})
				.fail(function(){
						alert('There was an error marking item as read.');
				});
				
			}
		}	
	},

	readArticle : function(){
		//store iframe in variable to insert, cache content div
		var iframe = $('<iframe src="about:blank" id="contentFrame" sandbox="allow-forms allow-scripts allow-same-origin"></iframe>'),
		content = $('#content');
		//add click handler to content links
		content.off().on('click', 'a', function(e){
			e.preventDefault();
			//insert iframe into content
			content.empty().append(iframe);
			header.loadExternal($(this).attr('href'));
		});
	},

	formEntry : function(dataF, dataAttr, form){
		form.find('[name="edit_feed_ID"]').val(dataAttr.id);
		form.find('[name="edit_url"]').val(dataF.text());
	}
};

var stacks = {

	init : function(){
		//grab elements needed and run functions
		var searchStacks = $('#searchStacks'),
		$container = $('#stacksScrollContainer');

		stacks.searchProjects(searchStacks, $container);
		stacks.filterProjects('#stacks_control');
	},

	filterProjects: function(header){
		var $header = $(header);
		//needs to be switched off before on as will run many times use tsort to sort asc,desc or rand on whole list of projects
		$header.find('span').off().on('click', 'a', function(e){
			e.preventDefault();
			var ordered = $(this).attr('href');

			$header.siblings('div').find('div').tsort('h3', {
				order : ordered
			});
			
			lazyLoad.triggerLoad(true);
		});

	},

	searchProjects: function(input, container){
		//grab list of visible divs. Probably a slow and costly way to do this. This must be changed each time another event occurs so we can search a filtered list and not mess up the sort by function
		var visibleStacks = container.find('div:visible');
		//function to delay keyup so we wait until a more usable string is collected
		var delay = (function(){
		  var timer = 0;
		  return function(callback, ms){
		    clearTimeout (timer);
		    timer = setTimeout(callback, ms);
		  };
		})();
		//bind the event, add the delay and finally run the function where the real stuff happens
		input.off().keyup(function() {
		    delay(function(){
		      filterItems(input.val(), visibleStacks);
		     lazyLoad.triggerLoad(true);
		    }, 600 );
		});
		//grab search query and list of visible divs. Check for blank and show all otherwise search h3 element for string and hide anything that doesn't match
		var filterItems = function(inputVal, visibleStacks){
			if(inputVal === ' '){
				visibleStacks.each(function(){
					$(this).show();
				});
			}else{
				visibleStacks.each(function(){
					var $this = $(this);
					if ($this.find('h3').text().search(new RegExp(inputVal, "i")) < 0) {
		                $this.hide();
			        } else {
			            $this.show();
			        }
				});
			}
		};
	},

	showSearch : function(button){
		var $button = $(button),
		ul = $button.siblings('ul'),
		input = $button.siblings('input');

		if(globals.body.hasClass('stacksSidebar')){
			if(ul.hasClass('show')){
				if(input.hasClass('showUl')){
					input.removeClass('show').removeClass('showUl');
				}else{
					input.removeClass('show').addClass('showUl');
				}
			}else{
				if(input.hasClass('show')){
					input.removeClass('showUl').removeClass('show');
				}else{
					input.removeClass('showUl').addClass('show');
				}
				
			}
			$button.toggleClass('activeButton');
		}
	},

	formEntry : function(dataF, dataAttr, form){
		form.find('[name="edit_model_ID"]').val(dataAttr.id);
		form.find('[name="edit_model_title"]').val(dataAttr.title);
	}
};

var projects = {
	//stores modeId when opening a project(variable sent from php page)
	modelID : null,
	//when submiting form we need to remove the elements from the DOM upon sucess. This is handled in another function we store them here until ready to be removed.
	mvFileList : null,
	//alternate between images and videos - videos needs to display as flex so can't use .show()
	reveal : function(button){
		var $button = $(button),
		fileType = $button.attr('data-filetype');

		if(fileType === "images"){
			globals.body.find('#model_pictures').show().end().find('#model_videos').removeClass('displayFlex');
			$button.addClass('activeButton').next('button').removeClass('activeButton');
		}else{
			globals.body.find('#model_videos').addClass('displayFlex').end().find('#model_pictures').hide();
			$button.addClass('activeButton').prev('button').removeClass('activeButton');
			lazyLoad.init('img', 'div.modelVideos', '', false);
		}
	},
	//seperate function for showing project forms - too much work to get the other form function to work
	showForm : function(button){
		var $button = $(button),
		spacePos = $button.attr('class').indexOf(" ");
		if(spacePos > 0){
			classBtn = $button.attr('class').substr(0, spacePos);	
		}else{
			classBtn = $button.attr('class');
		}
		
		$button.toggleClass('activeButton');

		$('#section_cat_content').find('#'+classBtn).toggleClass('show');
	},
	//seperate show form requires seperate remove
	removeForm : function(button){
		var $button = $(button),
		btnClass = $button.parent('form').attr('id');

		$('#section_cat_content').find('button.'+btnClass).trigger('click');	
	},

	loadContent : function(container){
		//kill the slideshow which will persist if the cancel button isn't triggered
		mediaView.slideShowOff($('#slideShowButton'));

		var $container = $(container),
		$mediaContainer = $container.find('#media');
		//setup the mediaView variables when loading projects
		mediaView.init($mediaContainer);

		$container.off().on('click', 'a.onlyThis', function(e){
			e.preventDefault();
			var $this = $(this),
			link = $this.attr('href'),
			holder = $this.parent('div'),
			idFlag = holder.attr('id').substr(-3),
			fav = $this.attr('data-fav'),
			favID = $this.attr('data-id');
			mediaView.show(link, holder, idFlag, fav, favID);
		});
	},

	triggerSubmit : function(form){
		//delete form is hidden but uses the same logic as move form so we simply trigger the submit button in the form that in turns runs formData()
		if(confirm('Are you sure?')){
			globals.body.find('#'+form).find('input[type="submit"]').trigger('click');
		}
		
	},

	sorting : function(){
		var pics = globals.body.find('#model_pictures'),
		vids = pics.siblings('#model_videos');

		globals.body.find('#models_header').find('span').off().on('click', 'a', function(e){
			e.preventDefault();

			var $this = $(this),
			sortingCriteria = $this.attr('href');

			function sort(container){
				container.find('div').tsort({
					attr: 'id',
					order: sortingCriteria
				});
				lazyLoad.triggerLoad(false);
			}

			if(pics.is(':visible')){
				sort(pics);
			}else{
				sort(vids);
			}
		});
	},

	editMode : function(button){
		var $button = $(button);
		$button.toggleClass('activeButton');
		//grab buttons and enable disabled buttons
		var btn = $button.siblings('button'),
		btnLength = btn.length;
		disenableBtn();

		function disenableBtn(){
			if($button.hasClass('activeButton')){
				for(var i=1; i <= 4; i++){
					$(btn[btnLength -i]).prop('disabled', false);
				}
			}else{
				for(var j=1; j <= 4; j++){
					$(btn[btnLength -j]).prop('disabled', true);
				}
			}
		}
		//change the click handler if in edit mode or return the click handler back to normal state
		if($button.hasClass('activeButton')){
			projects.editContent('#section_models');
		}else{
			//remove active class from links if done editing
			$button.parents('#section_cat_content').find('.activeTemp').removeClass('activeTemp');
			projects.loadContent('#section_models');
		}
	},

	editContent : function(container){
		var $container = $(container);

		$container.off().on('click', 'a.onlyThis', function(e){
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass('activeTemp');
		});
	},

	select : function(button){
		var $button = $(button),
		$parentContainer = $button.parents('#section_cat_content'),
		$imgContainer = $parentContainer.find('#model_pictures'),
		$vidContainer = $imgContainer.siblings('#model_videos');

		$button.toggleClass('activeButton');

		function imgOrVid(flag){
			if($imgContainer.is(':visible')){
				if(flag){
					$imgContainer.find('.onlyThis').addClass('activeTemp');
					$vidContainer.find('.onlyThis').removeClass('activeTemp');
				}else{
					$imgContainer.find('.onlyThis').removeClass('activeTemp');
					$vidContainer.find('.onlyThis').removeClass('activeTemp');
				}
				
			}else{
				if(flag){
					$imgContainer.find('.onlyThis').removeClass('activeTemp');
					$vidContainer.find('.onlyThis').addClass('activeTemp');
				}else{
					$imgContainer.find('.onlyThis').removeClass('activeTemp');
					$vidContainer.find('.onlyThis').removeClass('activeTemp');
				}
			}
		}

		if($button.hasClass('activeButton')){
			imgOrVid(true);
		}else{
			imgOrVid(false);
		}
	},

	formIDs : function(){
		$('#section_cat_content').find('#add_file_models').find('[name="add_model_ID"]').val(this.modelID);
	},

	formData : function(form){
		//we need to fill out the form using selected DOM elements frst then wrap this up in a FormData object
		
		var $parentContainer = $('#section_cat_content'),
		$imgContainer = $parentContainer.find('#model_pictures'),
		$vidContainer = $imgContainer.siblings('#model_videos');

		if($imgContainer.is(':visible')){
			$cont = $imgContainer;
			form.find('[name="videoFlag"]').val(false);
		}else{
			$cont = $vidContainer;
			form.find('[name="videoFlag"]').val(true);
		}

		$selectedItems = $cont.find('.activeTemp').map(function(){
			  return $(this).attr('data-id');
			}).get();

		projects.mvFileList = $cont.find('.activeTemp').parent();

		form.find('[name="IDArray"]').val($selectedItems);

		data = new FormData(document.forms.namedItem(form.attr('name')));

		return data;
	}

};

var crop = {
	container : '',

	init: function(button){
		crop.container = globals.loading.find('#thumbs');
		//grab all selected files and send to crop.php
		var $button = $(button),
		$container = $button.parents('#section_cat_content'),
		picturesToCrop = $container.find('#model_pictures').find('.activeTemp'),
		thumbArray = [],
		picturesArray = [];
		//check if anything was selected
		if(picturesToCrop.length > 0){
			picturesToCrop.each(function(){
				var $this = $(this),
				thumb = $this.find('img').attr('src'),
				picture = $this.attr('href');
				//grab thumbs for display purposes
				thumbArray.push(encodeURIComponent(thumb));
				//grab actual pics for crop purposes
				picturesArray.push(encodeURIComponent(picture));
			});
			//create and append crop div			
			crop.container.load('./php/thumbs/crop.php?tArr='+thumbArray+'&pArr='+picturesArray+'&modelID='+projects.modelID);
			crop.container.show().parent().fadeIn();
		}else{
			alert('Please select at least one photo to crop');
		}		
	},

	selectImage: function(list){
		//get image from list and initialise crop object
		var $list = crop.container.find(list),
		containerWidth = parseInt($('.main_crop_area').width()),
		aspRatio, convertedHeight;

		$list.on('click', 'a', function(e){
			e.preventDefault();
			var mainImage = $('#jcrop');
			var jcrop_api;
			$list.addClass('dim');

			mainImage.attr('src', $(this).attr('href')).one('load', function(){
				//get actual image dimensions
				aspRatio = ($(this)[0].naturalWidth/$(this)[0].naturalHeight);
				//check if it's landscape or portrait
				if($(this)[0].naturalWidth > $(this)[0].naturalHeight){
					convertedHeight = Math.round(containerWidth / aspRatio);
				}else{
					convertedHeight = Math.round(containerWidth * aspRatio);
				}
				//run jcrop with new image dimensions
				cropDelayed();
			});

			function cropDelayed(){
				mainImage.Jcrop({
					//trueSize: [containerWidth, convertedHeight],
					boxWidth: containerWidth,
					//boxHeight: convertedHeight,
					onSelect: crop.showCoords,
			        bgColor:     'black',
		            bgOpacity:   0.4,
		            keySupport: false,
		            aspectRatio: 200 / 250
				}, function(){
					jcrop_api = this;
					//setup cancel crop
					crop.removeCrop(jcrop_api, $list);
				});
			}
		});
	},

	showCoords: function(c){
		var form = globals.loading.find('#hidden_crop_form'),
		imageSrc = $('#jcrop').attr('src');
		form.find('#x_cor').val(c.x);
		form.find('#y_cor').val(c.y);
		form.find('#w_cor').val(c.w);
		form.find('#h_cor').val(c.h);
		form.find('#image_cor').val(imageSrc);
	},

	removeCrop: function(cropObject, list){
		var form = crop.container.find('#hidden_crop_form'),
		$jcrop = crop.container.find('#jcrop');

		crop.container.find('#cancel_crop').show().off().on('click', function(){
			cropObject.destroy();
			list.removeClass('dim');
			$jcrop.attr('style', '').attr('src', '');	
			$(this).hide().siblings('button').hide();
		});

		crop.container.find('#submit_crop').show().off().on('click', function(){
			list.removeClass('dim');
			$(this).hide().siblings('button').hide();
			cropObject.destroy();
			$jcrop.attr('style', '').attr('src', '');
			form.find('input[type="submit"]').trigger('click');
		});
	},

	submitForm: function(form, flag){
		form.submit(function(e){
			e.preventDefault();
			var $this = $(this),
			data = new FormData(document.forms.namedItem($this.attr('name')));
			if(flag){
				data.append('modelID', projects.modelID);
			}

			$.ajax({
				url : $this.attr('action'),
				data : data,
				type : 'POST',
				processData: false,
  				contentType: false
			}).done(function(resp){
				var json = $.parseJSON(resp);
				if(json.msg == 'true'){
					crop.reviewSteps(json.value);
				}else if(json.msg == 'false'){
					crop.close();
				}
			}).fail(function(resp){
				alert(resp);
			});
		});
		
	},

	reviewSteps: function(newImage){
		var holder = crop.container.find('#crop_comparrison');
		holder.find('#new_image').attr('src', newImage);
		holder.fadeIn();
	},

	finalise: function(button){
		var $button = $(button),
		$form = $button.siblings('form');
		$form.find('input[type="submit"]').trigger('click');
		
	},

	close: function(){
		crop.container.hide();
		crop.container.parent().fadeOut();

		setTimeout(function(){
			crop.container.empty().hide();
		}, 1000);
	},

	closeFinal : function(){
		crop.container.find('#crop_comparrison').fadeOut();
	}
};

var temp = {

	tempMessage : null,
	tmpNonImg : "Non-image selected",
	tmpNonSelect : "Nothing Selected",
	mvFileList : null,

	switchFolders: function(){
		globals.body.find('#dir_list').off().on('click', 'a.folders', function(e){
			e.preventDefault();
			header.loadContent(true, $(this).attr('href'));
		});
	},

	selectFiles: function(){
		//select and store DOM elements then clone and change. These will get destroyed hence the cloning. 
		var contentCont = globals.body.find('#display_panel');
		this.tempMessage = contentCont.find('#tempMessageCont');

		globals.body.find('#file_list').off().on('click', 'a', function(e){
			e.preventDefault();

			//cache link and data attributes
			var $this = $(this),
			dataOriginal = $this.find('img').attr('data-original'),
			notFile = $this.find('span').find('img').attr('data-notfile');

			$this.toggleClass('activeTemp');
			//get file location and list of currently selected files
			var tempImage = '<img src="DesignNxt/'+decodeURIComponent(dataOriginal)+'">',
			fileList = globals.body.find('#file_list').find('a.activeTemp');

			//check if anything is selected then check if it isn't a file and react accordingly
			if(fileList.length > 0){
				if(!$this.hasClass('activeTemp')){
					//don't add image if already selected
				}
				else if(notFile === 'true'){
					temp.tempMessage.find('p').text(temp.tmpNonImg);
					contentCont.html(temp.tempMessage);
				}else{
					contentCont.html(tempImage);
				}
			}else{
				temp.tempMessage.find('p').text(temp.tmpNonSelect);
					contentCont.html(temp.tempMessage);
			}
			
		});
	},

	tempFunction: function(moveOrDelete){
		var fileList = globals.body.find('#file_list').find('a.activeTemp'),
		fileArray = [],
		postString = moveOrDelete,
		modelID = globals.body.find('#dir_list').find('select').find('option:selected').val();
		this.mvFileList = fileList.parent();

		//create array to pass as post data to php file
		fileList.each(function(){
			fileArray.push(decodeURIComponent($(this).find('img').data('original')));
		});

		//construct form data to post
		var data = new FormData();
		data.append("move", postString);
		data.append("modelID", modelID);
		data.append("fileArray", fileArray);

		//check if we're actually sending anything and then post
		if(fileArray.length){
			var go;
		    //double check deletion is wanted
		    if(postString === 'delete'){
		    	go = confirm('Are you sure you want to delete selected files? Regrets?');
		    }else if(postString === 'move'){
		    	go = true;
		    }
		    var url = './db/temp_actions.php';
		    if(go){
		    	formSubmission.postForms(url, data);
		    }
		//otherwise throw an error	
		}else {
			alert('Please select at least one file!');
		}
	},

	tempRemoveFiles : function(flag){
		//addclass to hide files, wait, then remove from the DOM and empty the file list
		function wait(){
			flag.mvFileList.remove();	
			flag.mvFileList = null;
			if(typeof(flag.tempMessage) !== "undefined"){
				flag.tempMessage.find('p').text(flag.tmpNonSelect);
				globals.body.find('#display_panel').html(flag.tempMessage);
			}
		}

		if(flag.mvFileList !== null){
			flag.mvFileList.addClass('bookmark_remove');
			setTimeout(wait, 700);
		}	
	}

};

var wallpapers = {
	init: function(){
		var wallPapers = globals.body.find('#wallpapers'),
		wallPapersControl = wallPapers.find('#wallpaperControl'),
		backgroundImg = $('#bg').find('img');

		wallPapers.toggleClass('active');
		if(wallPapersControl.length){
			wallPapersControl.remove();
		}else{
			$.get('./php/wallpapers.php?bckImg=false', function(data){
				wallPapers.append(data);
				$('#wallpaperControl').on('click', 'img', function(){
					var imgSrc = $(this).data('original');
					backgroundImg.attr('src', imgSrc);
				});
			});
			
		}
	},

	remove: function(button, ID){
		$button = $(button);

		function wait(){
			$button.parent().remove();
		}

		$.get('./db/fav_actions.php?decision=remove&PhotoID='+ID, function(data){
			var json = $.parseJSON(data);
			if(json.error){
				alert(json.msg);
			}else{
				$button.parent().addClass('bookmark_remove');
				setTimeout(wait, 700);
			}
		});
	},

	fontSelection: function(){
		globals.body.find('#wallpapers').off().on('click', 'p.font', function(){
			var fontClass = $(this).text().toLowerCase();

			if(fontClass == 'avenir'){
				globals.body.removeClass();
			}else{
				globals.body.removeClass().addClass(fontClass);
			}
		});
	}
};

var videoThumbs = {
	init: function(button){
		var $button = $(button),
		link = $button.siblings('a'),
		linkage = link.attr('href'),
		linkThumb = link.find('img').data('original'),
		url = './php/thumbs/videoThumbs.php?linkage='+encodeURIComponent(linkage)+'&linkThumb='+encodeURIComponent(linkThumb);

		//please wait
		globals.loading.fadeIn().find('#shallNotPass').show().addClass('loading');

		function hideWait(){
			setTimeout(function(){
				globals.loading.find('#shallNotPass').hide().removeClass();
			}, 600);
		}

		var $container = globals.loading.find('#videoThumbnailselect');

		$container.load(url+'&decision=false', function(){
				globals.loading.find('#shallNotPass').removeClass().addClass('success');
				$container.fadeIn();
				hideWait();
				videoThumbs.selectVIDThumb($container, url, link);
		});
	},

	selectVIDThumb : function($container, url, link){
		$container.off().on('click', 'a', function(){
			var $this = $(this),
			newImage = $this.find('img').data('original');

			$this.parent().parent().find('a.activeTemp').removeClass('activeTemp').end().end().end().addClass('activeTemp');

			$container.find('#selectVidThumb').text('Accept new thumb?').off().on('click', function(){

				$.get(url+'&decision=true&newThumb='+encodeURIComponent(newImage), function(data){

					var json = $.parseJSON(data);

					if(!json.error){
						var refreshIMG = link.find('img'),
						refreshAttr = refreshIMG.attr('src'),
						seconds = new Date().getTime() / 1000,
						finalRefresh = refreshAttr+'?'+seconds;

						refreshIMG.attr('src', finalRefresh).attr('data-original', finalRefresh);

						$('#close_crop').trigger('click');

					}else{
						alert(json.msg);
					}
				});	
			});
		});
	},

	cancelAndClose: function(){
		globals.loading.fadeOut().find('#shallNotPass').hide().removeClass().end().find('#videoThumbnailselect').hide().empty();
	}
};

var mediaView = {

	holder : null,
	slideshow : null,
	video : null,
	img : null,
	header : null,
	picControls : null,
	vidControls : null,
	linkedDiv : null,
	favButton : null,

	init: function(container){
		//set fields of object used for other functions
		this.holder = container.find('.holder');
		this.header = container.find('#mediaControlBar');
		this.img = this.holder.find('img');
		this.video = this.holder.find('video');
		this.picControls = this.header.find('#pictureControls');
		this.vidControls = this.header.find('#videoControls');
		this.favButton = this.header.find('.fav');
	},

	show: function(link, div, flag, fav, favID){
		this.keyBoardControls();

		if(this.header.parent().hasClass('hide')){
			this.header.parent().removeClass('hide');
		}
		this.linkedDiv = div;
		if(fav === 'true'){
			this.favButton.addClass('activeButton').attr('data-id', favID);
		}else{
			this.favButton.removeClass('activeButton').attr('data-id', favID);
		}

		function delayPicLoading(){
			mediaView.img.attr('src', link).attr('data-id', favID).fadeIn();
		}
		//hide/display elements based on vids or pics and show container
		if(flag === 'pic'){
			this.video.hide();
			this.vidControls.hide();
			this.picControls.show().css("display", "inline-block");
			this.img.fadeOut();
			setTimeout(delayPicLoading, 400);
		}else{
			this.video.fadeIn().attr('src', link).attr('data-id', favID);
			this.vidControls.show().css("display", "inline-block");
			this.picControls.hide();
			this.img.hide();
			this.videoInit();
		}

		this.header.parent().fadeIn();
	},

	hide: function(button){
		$button = $(button);
		$button.toggleClass('flip');
		$button.parents('#media').toggleClass('hide');
	},

	close: function(button){
		var $button = $(button),
		$container = $button.parents('div#media');
		//stop slideshows/videos playing
		mediaView.slideShowOff($container.find('button#slideShowButton'));
		mediaView.killVideo();
		//kill all content
		this.img.attr('src', '');
		this.video.attr('src', '');
		
		$container.fadeOut();

		//remove keyboard short cuts from window
		$(window).off('keydown');
	},

	killVideo : function(){
		if(this.video === null){
			//do nothing
		}else{
			this.video[0].pause();
			$('#play-pause').removeClass('playActive');
		}
	},

	back: function(){
		//grab prev div to one that was clicked and trigger otherwise get last div
		if(this.linkedDiv.prev().length){
			this.linkedDiv.prev().find('a').eq(0).trigger('click');
		}else{
			this.linkedDiv.siblings().eq(-1).find('a').eq(0).trigger('click');
		}
	},

	forward: function(){
		//same as back function but obviously checks next div. Note: previously I used the keyword 'this' but when calling the function from slideShow it returns undefined. Curious.

		if(mediaView.linkedDiv.next().length){
			mediaView.linkedDiv.next().find('a').eq(0).trigger('click');	
		}else{
			mediaView.linkedDiv.siblings().eq(0).find('a').eq(0).trigger('click');
		}
	},

	slideShow: function(button){
		$(button).attr('onclick', 'mediaView.slideShowOff(this)').addClass('slideShowOn');
		mediaView.slideshow = setInterval(mediaView.forward, 5000);
	},

	slideShowOff: function(button){
		$(button).removeClass('slideShowOn').attr('onclick', 'mediaView.slideShow(this)');
		clearInterval(mediaView.slideshow);
	},

	imageOriginal: function(){
		this.img.removeClass('fullWidth').addClass('original');
	},

	imageFullWidth: function(){
		this.img.removeClass('original').addClass('fullWidth');
	},

	imageToFit: function(){
		this.img.removeClass('original').removeClass('fullWidth');
	},

	favourites : function(button){
		var $button = $(button),
		photoID = $button.attr('data-id'),
		divContID = '#'+photoID+"-pic";
		divCont = $('#model_pictures').find(divContID);

		if($button.hasClass('activeButton')){
			$.ajax({
				url : './db/fav_actions.php?decision=remove&PhotoID='+photoID,
				type : 'GET'})
				.done(function(){
					$button.removeClass('activeButton');
					divCont.find('a').attr('data-fav', false);
				});
		}else{
			$.ajax({
				url : './db/fav_actions.php?decision=add&PhotoID='+photoID,
				type : 'GET' })
				.done(function(){
					$button.addClass('activeButton');
					divCont.find('a').attr('data-fav', true);
				});
		}
	},

	trash: function(){
		//too much pain to reuse code so we just write it out here - bleh
		//get form, get ID to be deleted
		var $form = $('#file_delete_projects'),
		mediaID = mediaView.img.is(':visible') ? mediaView.img.attr('data-id') : mediaView.video.attr('data-id'),
		fileFlag = null;

		//set form input and grab file from list to remove after deletion
		if(this.img.is(':visible')){
			$form.find('[name="videoFlag"]').val(false);
			fileFlag = 'pic';
		}else{
			$form.find('[name="videoFlag"]').val(true);
			fileFlag = 'vid';
		}
		$form.find('[name="IDArray"]').val(mediaID);
		var fileParent = $('#section_cat_content').find('#'+mediaID+'-'+fileFlag+''),
		data = new FormData($form.get(0));

		if(confirm('Are you sure?')){
			snp = globals.loading.find('#shallNotPass');
			snp.show().addClass('loading');
			globals.loading.fadeIn();

			$.ajax({
				url : $form.attr('action'),
				type : 'POST',
				data : data,
				processData: false,
	  			contentType: false })

			.done(function(resp){

				function wait(){
					snp.hide().removeClass();
					globals.loading.fadeOut();
					fileParent.remove();
				}

				var json = $.parseJSON(resp);
				if(json.error){
					snp.removeClass().addClass('error');
					alert(json.msg);
					setTimeout(wait, 600);
				}else{
					snp.removeClass().addClass('success');
					fileParent.addClass('bookmark_remove');
					mediaView.forward();
					setTimeout(wait, 600);
				}
			});	
		}
		

		
	},

	videoInit: function(){
		var seekBar = document.getElementById("seek-bar");
  		var volumeBar = document.getElementById("volume-bar");

  		//nb: we need a plain javascript object so we need [0] on all calls to mediaView.video, sucks

		seekBar.addEventListener("change", function() {
  		// Calculate the new time
		var time = mediaView.video[0].duration * (seekBar.value / 100);

		// Update the video time
		mediaView.video[0].currentTime = time;
		});

		// Update the seek bar as the video plays
		mediaView.video[0].addEventListener("timeupdate", function() {
		  // Calculate the slider value
		  var value = (100 / mediaView.video[0].duration) * mediaView.video[0].currentTime;

		  // Update the slider value
		  seekBar.value = value;
		});

		// Pause the video when the slider handle is being dragged
		seekBar.addEventListener("mousedown", function() {
		  mediaView.video[0].pause();
		});

		// Play the video when the slider handle is dropped
		seekBar.addEventListener("mouseup", function() {
		  mediaView.video[0].play();
		});

		// Event listener for the volume bar
		volumeBar.addEventListener("change", function() {
		  // Update the video volume
		  mediaView.video[0].volume = volumeBar.value;
		});
	},

	play: function(button){
		if(mediaView.video[0].paused === true) {
 			mediaView.video[0].play();
 			$(button).addClass('playActive');
 		}else{
 			mediaView.video[0].pause();
 			$(button).removeClass('playActive');
 		}
	},

	mute: function(button){
		if(mediaView.video[0].muted === false) {
		 	mediaView.video[0].muted = true;
		 	$(button).addClass('muted');
		}else{
		 	mediaView.video[0].muted = false;
		 	$(button).removeClass('muted');
		}
	},

	fullscreen: function(){
		if(mediaView.video[0].requestFullscreen){
		    mediaView.video[0].requestFullscreen();
		}else if(mediaView.video[0].mozRequestFullScreen){
		    mediaView.video[0].mozRequestFullScreen();
		}else if(mediaView.video[0].webkitRequestFullscreen){
		    mediaView.video[0].webkitEnterFullscreen();
		}
	},

	keyBoardControls: function(){
		Mousetrap.bind('space', function() { 
			if(mediaView.vidControls.is(':visible')){
					mediaView.header.find('#play-pause').trigger('click');
			}else{
				mediaView.header.find('#slideShowButton').trigger('click');
			}
		});

		Mousetrap.bind('left', function() { 
			mediaView.header.find('.prevPic').trigger('click');
		});

		Mousetrap.bind('up', function() { 
			mediaView.header.find('.imageToFit').trigger('click');
		});

		Mousetrap.bind('right', function() { 
			mediaView.header.find('.nextPic').trigger('click');
		});

		Mousetrap.bind('down', function() { 
			mediaView.header.find('.imageFullWidth').trigger('click');
		});

		Mousetrap.bind('f', function() { 
			mediaView.header.find('.fav').trigger('click');
		});

		Mousetrap.bind('x', function() { 
			mediaView.header.find('.mediaClose').trigger('click');
		});
	} 
};










