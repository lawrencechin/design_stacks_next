module.exports = function(grunt){
	'use strict';
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		uglify: {
			options: {
				mangle: false,
				preserveComments: false,
				compress : true
			},
				
			compile: {
				files: {
			        'public/js/min/script.min.js': 'public/js/script.js'
			      }
			}
		},

		sass: {
			dist: {
				options: {
					style : 'expanded',
					trace : true
				},

				files:[{
					expand: true,
					cwd: 'public/css/sass/',
					src: '*.scss',
					dest: 'public/css/',
					ext: '.css'
				}]
			}
		},

		autoprefixer: {
			dist: {
				files: {
					'public/css/screen-prefixed.css' : 'public/css/screen.css'
				}
			}
		},

		cssmin: {
			options: {
		    	shorthandCompacting: false,
		    	roundingPrecision: -1
			},
			target: {
			    files: {
			       'public/css/screen.min.css' : 'public/css/screen-prefixed.css'
			    }
		  	}
		},

		watch: {
            sass: {
                files: ['public/css/sass/*.scss'],
                tasks: ['sass', 'autoprefixer', 'cssmin']
            },

            uglify: {
            	files: ['public/js/script.js'],
            	tasks: ['uglify']
            },

            options: {
            	spawn : false,
            	event : 'changed'
            }
        }
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	//Default task(s)
	grunt.registerTask('compile', 'uglify');
};