<?php

//load config file(error reporting, database, paths)
if(file_exists('vendor/autoload.php')){
	require 'vendor/autoload.php';
}


require 'application/config/config.php';
require 'application/config/feedback.php';
require 'application/config/autoload.php';
require 'application/libs/locatGen.php';

//Start your engines

$app = new Application;

?>