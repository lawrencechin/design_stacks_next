<?php 
ini_set('display_errors',1);
error_reporting(E_ALL);
//include database handle & thumbnail functions
include_once './database.php';
include_once '.././php/thumbs/thumbnails.php';
include_once './delete_file_function.inc.php';
//Post variable determining the function to run(add/move/delete). All other post variables are in each function as needed to suppress previous errors about undefined indexes 
$photoDecision = htmlspecialchars($_POST['photoDecision']);
$error = true;

//if statement to test against which function we should run
if($photoDecision == 'add'){
	addPhoto();
}elseif($photoDecision == 'move'){
	movePhoto();
}elseif($photoDecision == 'delete'){
	$photo_array = $_POST['IDArray'];
	$videoFlag = $_POST['videoFlag'];
	if(deletePhoto($photo_array, $videoFlag)){
		$error = false;
		echo json_encode(array('msg'=>'FilesChanged', 'error'=>$error));
	}
}else{
	echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
}

//move function
function movePhoto(){
	global $dbh;
	global $error;

	//ID of model to move files to
	$photo_move_model_id = htmlspecialchars($_POST['select_model']);
	//files to be moved
	$photo_array = $_POST['IDArray'];
	//flag determining if we are working with images or videos
	$videoFlag = $_POST['videoFlag'];
	$pArr = explode(',', $photo_array);

	if(isset($photo_array) && count($photo_array) > 0 && is_numeric($photo_move_model_id)){
		$table = $videoFlag === 'true' ? 'Videos' : 'Photos';
		foreach($pArr as $ids){
			//update modelIDs of all selected files
			$moveQuery = "UPDATE $table SET ModelID = '$photo_move_model_id'WHERE ID = '$ids';";
			try{
				$dbh->exec($moveQuery);
			}catch(PDOException $e){
				locationGenerator::debug_to_console($e->getMessage());
				echo json_encode(array('msg'=>'Could not communicate with database. '.$e->getMessage(), 'error'=>$error));
				exit();
			}
		}
		//success! 
		$error = false;
		echo json_encode(array('msg'=>'FilesChanged', 'error'=>$error));
		
	}else{
		echo json_encode(array('msg'=>'No files selected. Please try again', 'error'=>$error));
	}

};

//add function
function addPhoto(){
	date_default_timezone_set('Europe/London');

	global $dbh;
	global $error;

	$movieArray = array('mp4', 'm4v');
	$pictureArray = array('jpeg', 'jpg', 'gif', 'png', 'tiff');

	$uploadedFiles = $_FILES['files'];
	$photo_add_id = htmlspecialchars($_POST['add_model_ID']);

	function addFile(&$file, &$fileFlag, &$fileExtension){
		global $error;
		//set variables to define name and path
		$finalname = locationGenerator::nameGen($file['tmp_name'], $fileExtension);
		$location = locationGenerator::gen($finalname);
		$thumbLocation = locationGenerator::thumbLocation($finalname);

		//make directories
		locationGenerator::createDir($location);
		
		//set variable depending on fileType
		$functionToExec = $fileFlag === 'Videos' ?  'videoThumbs' : 'makeThumbnail';
		//move files and generate thumbs
		if(move_uploaded_file($file['tmp_name'], ROOT.PAGEURL.CONTENTDIR.$location.$finalname)){
			$in = '..'.CONTENTDIR.$location.$finalname;
			$out = '..'.CONTENTDIR.$location.$thumbLocation;
			$functionToExec($in, $out);
			return array(true, $finalname);
		}else{
			echo json_encode(array('msg'=>'Could not move uploaded files. Please try again.', 'error'=>$error));
			return array(false);
		}
	}

	function writeToDb(&$dbh, &$fileFlag, &$test, &$photo_add_id){
		global $error;
		$fType = $fileFlag === 'Videos'? 'VIDname' : 'IMGname';
		//PDO seems to REALLY hate looping insert queries that aren't prepared…
		$query = "INSERT INTO $fileFlag(ModelID, $fType) VALUES(:ModelID, :$fType)";
		$insertQuery = $dbh->prepare($query);
		$data = array('ModelID'=>$photo_add_id, $fType=>$test[1]);

		try{
			$insertQuery->execute($data);
		}catch(PDOException $e){
			$_SESSION["PERCENTAGE_DONE"] = 100;
			echo json_encode(array('msg'=>'Could not communicate with database. '.$e->getMessage(), 'error'=>$error));
			exit();
		}
	}

	if(isset($uploadedFiles) && count($uploadedFiles) > 0){
		//rearrange file array into something more palpable
	    foreach($uploadedFiles as $key=>$value){
	    	foreach($value as $key2=>$value2){
	    		$newFileArray[$key2][$key] = $value2;
	    	}
	    }

		$count = 0;
		$errorFileArr = array();

	    foreach($newFileArray as $file){ 
	    	$count ++;
	    	//get extension of file
			$fileName = pathinfo($file['name']);
			$fileExtension = strtolower($fileName['extension']);
	    	//check if files are in the array
			if(in_array($fileExtension, $movieArray)){
				$fileFlag = 'Videos';

				$test = addFile($file, $fileFlag, $fileExtension);
				if($test[0]){
					writeToDb($dbh, $fileFlag, $test, $photo_add_id);
				}
			}elseif(in_array($fileExtension, $pictureArray)){
				$fileFlag = 'Photos';

				$test = addFile($file, $fileFlag, $fileExtension);
				if($test[0]){
					writeToDb($dbh, $fileFlag, $test, $photo_add_id);
				}
			}else{
				//collect files that did not match accepted extensions
				$errorFileArr[] = $file['name'];
			}

		}

		$error = false;
		//check if any files did not match correct extensions, return response without $error but with warning
		if(count($errorFileArr) > 0){
			//generate string of bad files
			$errorStr = implode(', ', $errorFileArr);

			if(count($errorFileArr) === $count){
				$error = true;
				echo json_encode(array('msg'=>"The following files had incorrect extensions: ".$errorStr, 'error'=>$error));
				exit();
			}
			
			echo json_encode(array('msg'=>"FilesAdded", 'error'=>$error, 'info'=>"Files: ".$errorStr));
		}else{
			echo json_encode(array('msg'=>'FilesAdded', 'error'=>$error));
		}
	}else{
		echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
	}
};

?>