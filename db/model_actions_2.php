<?php 

include_once './database.php';

$modelDecision = htmlspecialchars($_POST['modelDecision']);

$error = true;

if($modelDecision == 'add'){
	addEditModel();
}elseif($modelDecision == 'delete'){
	deleteModel();
} elseif($modelDecision == 'edit'){
	addEditModel();
}else{
	echo json_encode(array('msg'=>'Dog.', 'error'=>$error));
}

function addEditModel(){

	global $dbh;
	global $root;
	global $pageURL;
	global $categoriesArray;
	global $error;

	$addModelTitle = htmlspecialchars($_POST['add_model_title']);
	$addModelCat = htmlspecialchars($_POST['add_model_cat']);
	$editModelTitle = htmlspecialchars($_POST['edit_model_title']);
	$editModelCat = htmlspecialchars($_POST['edit_model_cat']);
	$editModelID = htmlspecialchars($_POST['edit_model_ID']);
	$editModelLink = htmlspecialchars($_POST['edit_model_link']);
	$editModelOldCatLink = htmlspecialchars($_POST['edit_old_catLink']);
	$editModelOldLink = htmlspecialchars($_POST['edit_old_modelLink']);
	$failedTest = 0;

	if(isset($addModelCat) && is_numeric($addModelCat) && strlen($addModelTitle) > 0){
		//Get the base link for the category
		foreach($categoriesArray as $cat){
			if($cat['ID'] === $addModelCat){
				$catLink = $cat['Link'];
			}	
		}

		//Check if name is already used and set failedtest to 1
		foreach($dbh->query("SELECT Models.Title FROM Models")as $modelTitle){
				if($addModelTitle == $modelTitle['Title']){
					$failedTest = 1;
				}
		}

		//run function providing failedtest is false
		if($failedTest === 0){
			$catid = $addModelCat;
			$title = $addModelTitle;
			$link = '/' . str_replace(' ', '-', $title);
			$oldLink = $catLink.$link;

			$addModelQuery = "INSERT INTO Models(CatID, Title, Link) Values(:CatID, :Title, :Link)";

			$data = array( 'CatID' => $catid, 'Title' => $title, 'Link' => $link);

			$modelQuery = $dbh->prepare($addModelQuery);

			$modelQuery->execute($data);

			if(file_exists($root.$pageURL.$oldLink)){
				rename($root.$pageURL.$oldLink, $root.$pageURL.$catLink.$link);

				echo "modelChanged";

			} elseif (!file_exists($root.$pageURL.$oldLink)) {
				$oldmask = umask(0);
				mkdir($root.$pageURL.$catLink.$link, 0777);
				mkdir($root.$pageURL.$catLink.$link.'/_Images', 0777);
				mkdir($root.$pageURL.$catLink.$link.'/_Thumb', 0777);
				mkdir($root.$pageURL.$catLink.$link.'/_Videos', 0777);
				mkdir($root.$pageURL.$catLink.$link.'/_VideosThumbs', 0777);
				umask($oldmask);

				echo "modelChanged";
			} else {
				echo 1;
			}

		} else {
			echo 1;

		}

	} elseif(isset($editModelCat) && is_numeric($editModelCat)){

		foreach($categoriesArray as $cat){
			if($cat['ID'] === $editModelCat){
				$catLink = $cat['Link'];
			}
		}

		$catid = $editModelCat;
		$modelID = $editModelID;
		$title = $editModelTitle;
		$oldLink = $editModelOldCatLink.$editModelOldLink;
		$link = '/' . str_replace(' ', '-', $title);

		$editModelQuery = "UPDATE Models SET CatID = :CatID, Title = :Title, Link = :Link WHERE ID = $modelID";

		$data = array( 'CatID' => $catid, 'Title' => $title, 'Link' => $link);

		$modelQuery = $dbh->prepare($editModelQuery); 

		$modelQuery->execute($data);

		if(file_exists($root.$pageURL.$oldLink)){
			rename($root.$pageURL.$oldLink, $root.$pageURL.$catLink.$link);
			echo "modelChanged";
		} else {
			echo 1;

		}	

	} else {
		//Return 1 to js script to give error message if nothing is matched
		echo 1;

	}
	
};

function deleteModel(){

	global $dbh; 
	global $root;
	global $pageURL;

	$deleteModelID = htmlspecialchars($_POST['delete_model_ID']);
	$deleteModelLink = htmlspecialchars($_POST['delete_model_link']);
	$deleteModelCatLink = htmlspecialchars($_POST['delete_catLink']);

	if(isset($deleteModelID) && is_numeric($deleteModelID)){
		$deleteMQuery = "DELETE FROM Models WHERE ID = $deleteModelID; DELETE FROM Photos WHERE ModelID = $deleteModelID; DELETE FROM Videos WHERE ModelID = $deleteModelID";

		$finalLocation = $root.$pageURL.$deleteModelCatLink.$deleteModelLink;

		system('rm -rf ' . escapeshellarg($finalLocation), $retval);

		$dbh->exec($deleteMQuery);

		
		return $retval == 0; // UNIX commands return zero on success
	} else {
		echo 1;

	}

};

?>