<?php
//delete function - seperated out to own file so it can be reused for Project deletion. Saves typing!
function deletePhoto($photo_array, $videoFlag){
	global $dbh;
	global $error;

	$pArr = explode(',', $photo_array);
	$table = $videoFlag === 'true'? 'Videos' : 'Photos';
	$columnName = $videoFlag === 'true'? 'VIDname' : 'IMGname';

	if(isset($photo_array) && count($photo_array) > 0){
		//get info from DB from ID array
		foreach($pArr as $delete){
			foreach($dbh->query("SELECT ID, ModelID, $columnName FROM $table WHERE ID = $delete;", PDO::FETCH_ASSOC) as $file){
				$link = ROOT.PAGEURL.CONTENTDIR.locationGenerator::gen($file[''.$columnName.'']);
				$mainFile = $link.$file[''.$columnName.''];
				$thumb = $link.locationGenerator::thumbLocation($file[''.$columnName.'']);

				if(is_file($mainFile)){
					unlink($mainFile);
					unlink($thumb);
					//remove folders if empty
					$isDirEmpty = !(new \FilesystemIterator($link."thumb"))->valid();
					if($isDirEmpty == 1){
						if(is_file($link.'.DS_Store')){
							unlink($link.'.DS_Store');
						}
						rmdir($link."thumb");
						rmdir($link);
					}
				}

				$deleteQuery = "DELETE FROM $table WHERE $table.ID = $delete;";
				$deleteFromFavs = "DELETE FROM Favourites WHERE PhotoID = $delete";
				try{
					$dbh->exec($deleteQuery);
			    	$dbh->exec($deleteFromFavs); 
				}catch(PDOException $e){
					echo json_encode(array('msg'=>'Could not communicate with database. '.$e->getMessage(), 'error'=>$error));
					return false;
					exit();
				}
			}
		}
		return true;

	}else{
		//return error
		echo json_encode(array('msg'=>'No files selected. Please try again', 'error'=>$error));
		return false;
	}
};
?>