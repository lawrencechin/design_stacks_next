<?php 

include_once './database.php';
include_once '.././php/thumbs/thumbnails.php';

date_default_timezone_set('Europe/London');

$move = htmlspecialchars($_POST['move']);
$error = true;

if($move == 'move'){
	moveTempFiles();
}elseif($move == 'delete'){
	deleteTempFiles();
}else{
	echo json_encode(array('msg'=>'No move or delete flag sent', 'error'=>$error));
}

function moveTempFiles(){
	//post data
	$fileArray = $_POST['fileArray'];
	$modelID = $_POST['modelID'];
	$moveArray = explode(',', $fileArray);
	//global variables
	global $error;
	global $dbh;
	global $root;
	global $pageURL;
	global $foldersArray;
	//filetype arrays
	$videoFileTypes = ['mp4','mv4'];
	$pictureFileTypes = ['jpeg', 'jpg', 'gif', 'png'];
	//queries
	$getModelPath = "SELECT 
		Categories.Link as catLink,
		Models.Link 
		FROM Categories
		INNER JOIN Models on Categories.ID = Models.CatID 
		WHERE Models.ID = $modelID";
	$insertVidQuery = "INSERT INTO Videos(ModelID, Videoname, THUMBname) VALUES(:ModelID, :Videoname, :THUMBname)";
	$insertPicQuery = "INSERT INTO Photos(ModelID, IMGname, THUMBname) VALUES(:ModelID, :IMGname, :THUMBname)";
		
	foreach($dbh->query($getModelPath, PDO::FETCH_ASSOC) as $row){
		$fullPath = $root.$pageURL.$row['catLink'].$row['Link'];
		$newName = $row['Link'];
	}	

	foreach($moveArray as $moving){

		$moveFullPath = $root.$pageURL.substr($moving, 2);
		$newImageName = pathinfo($moving, PATHINFO_EXTENSION);
		$currentDate = date('dmys').'-'.rand(0,100).'-'.rand(0,100);

		//check if file is applicable file
		if(in_array($newImageName, $videoFileTypes)){
			$fileTypeFlag = 'video';
		}elseif(in_array($newImageName, $pictureFileTypes)){
			$fileTypeFlag = 'image';
		}else{
			echo json_encode(array('msg'=>'Files sent were not of the correct file type. Please upload only images or videos.' . substr($moving, 2), 'error'=>$error));
			return;
		}

		//set variables for image or video
		$extension = $fileTypeFlag === "video" ? '.jpeg' : '.jpg';
		$folder = $fileTypeFlag === "video" ? 'Video' : 'Images';
		$thumb = $fileTypeFlag === "video" ? 'VideoThumb' : 'Thumb';
		$query = $fileTypeFlag === "video" ? $insertVidQuery : $insertPicQuery;

		//set file names and paths for file + thumbnail
		$fileName = $newName. '-' . $currentDate . '.' . $newImageName;
		$thumbName = $newName. '-' . $currentDate . $extension;

		$data = $fileTypeFlag === "video" ? array('ModelID'=> $modelID, 'Videoname'=> $fileName, 'THUMBname'=> $thumbName) : array('ModelID'=> $modelID, 'IMGname'=> $fileName, 'THUMBname'=> $thumbName);

		$finalFullPathandFileName = $fullPath.$foldersArray[0][$folder].$newName. '-' . $currentDate . '.' . $newImageName;

		$finalFullThumbPathandFileName = $fullPath.$foldersArray[0][$thumb].$newName. '-' . $currentDate . $extension;

		//special video folder check
		if($fileTypeFlag === 'video'){
			$videoPathTest = $fullPath.$foldersArray[0][$folder];

			$videoThumbsPathTest = $fullPath.$foldersArray[0][$thumb];

			if (!file_exists($videoPathTest)) {
				$oldmask = umask(0);
				mkdir($videoPathTest, 0777);
				mkdir($videoThumbsPathTest, 0777);
				umask($oldmask);
			}
		}
		//check if we can move file, error otherwise
		try{
			rename($moveFullPath, $finalFullPathandFileName);
			if($fileTypeFlag === 'video'){
				videoThumbs($finalFullPathandFileName, $finalFullThumbPathandFileName);
			}else{
				makeThumbnail($finalFullPathandFileName, $finalFullThumbPathandFileName);
			}
		}catch(Exception $e){
			echo json_encode(array('msg'=>'Exception occured. ' . substr($moving, 2) . " : " . $e->getMessage(), 'error'=>$error));
			return;
		}

		$tempQuery = $dbh->prepare($query);

		$tempQuery->execute($data);
	}

	$error = false;
	echo json_encode(array('msg'=>'Files uploaded', 'error'=>$error));
};


function deleteTempFiles(){
	$fileArray = htmlspecialchars($_POST['fileArray']);
	$deleteArray = explode(',', $fileArray);
	global $root;
	global $pageURL;
	global $error;

	foreach($deleteArray as $remove){
		$removeFullPath = $root.$pageURL.substr($remove, 2);
		//check if file exists. If not return error and exit
		if(!file_exists($removeFullPath)){
			$error = true;
			echo json_encode(array('msg'=>'Files could not be deleted, check permissions', 'error'=>$error));
			return;
		}else{
			if(unlink($removeFullPath)){
			}else{
				echo json_encode(array('msg'=>'Files could not be deleted', 'error'=>$error));
				return;
			}
		}
		
	}

	$error = false;
	echo json_encode(array('msg'=>'Files deleted', 'error'=>$error));
};

?>