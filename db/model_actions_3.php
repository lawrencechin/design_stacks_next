<?php 

include_once './database.php';

$modelDecision = htmlspecialchars($_POST['modelDecision']);

$error = true;

if($modelDecision == 'add'){
	addModel();
}elseif($modelDecision == 'delete'){
	deleteModel();
} elseif($modelDecision == 'edit'){
	editModel();
}else{
	echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
}

function addModel(){

	global $dbh;
	global $root;
	global $pageURL;
	global $categoriesArray;
	global $error;
	//post variables
	$addModelTitle = htmlspecialchars($_POST['add_model_title']);
	$addModelCat = htmlspecialchars($_POST['add_model_cat']);

	$failedTest = 0;

	if(isset($addModelCat) && is_numeric($addModelCat) && strlen($addModelTitle) > 0){
		//Get the base link for the category
		foreach($categoriesArray as $cat){
			if($cat['ID'] === $addModelCat){
				$catLink = $cat['Link'];
			}	
		}

		//Check if name is already used and set failedtest to 1
		foreach($dbh->query("SELECT Models.Title FROM Models")as $modelTitle){
				if($addModelTitle == $modelTitle['Title']){
					$failedTest = 1;
				}
		}

		//run providing failedtest is false
		if($failedTest === 0){
			$catid = $addModelCat;
			$title = $addModelTitle;
			//replace spaces with hyphens
			$link = '/' . str_replace(' ', '-', $title);
			$oldLink = $catLink.$link;
			//add data to database
			$addModelQuery = "INSERT INTO Models(CatID, Title, Link) Values(:CatID, :Title, :Link)";

			$data = array( 'CatID' => $catid, 'Title' => $title, 'Link' => $link);

			$modelQuery = $dbh->prepare($addModelQuery);
			try{
				$modelQuery->execute($data);
			}catch(PDOException $e){
				echo json_encode(array('msg'=>'Could not insert into database. '.$e->getMessage(), 'error'=>$error));
			}
			

			//shouldn't need this as we already checked that the title doesn't already exist

			/**if(file_exists($root.$pageURL.$oldLink)){
				rename($root.$pageURL.$oldLink, $root.$pageURL.$catLink.$link);
				echo "modelChanged";
			}**/

			//file SHOULD NOT EXIST!
			if(!file_exists($root.$pageURL.$oldLink)) {
				$oldmask = umask(0);
				mkdir($root.$pageURL.$catLink.$link, 0777);
				mkdir($root.$pageURL.$catLink.$link.'/_Images', 0777);
				mkdir($root.$pageURL.$catLink.$link.'/_Thumb', 0777);
				mkdir($root.$pageURL.$catLink.$link.'/_Videos', 0777);
				mkdir($root.$pageURL.$catLink.$link.'/_VideosThumbs', 0777);
				umask($oldmask);

				$error = false;
				echo json_encode(array('msg'=>'ModelChanged', 'error'=>$error));
			} else {
				echo json_encode(array('msg'=>'There was an error. Please try again.', 'error'=>$error));
			}

		}else{
			//if name exists already
			echo json_encode(array('msg'=>'That name already exists. Please change it.', 'error'=>$error));
		}
	}else{
		//if variables not set
		echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
	}
	
};

function editModel(){

	global $dbh;
	global $root;
	global $pageURL;
	global $categoriesArray;
	global $error;
	//post variables
	$editModelTitle = htmlspecialchars($_POST['edit_model_title']);
	$editModelCat = htmlspecialchars($_POST['edit_model_cat']);
	$editModelID = htmlspecialchars($_POST['edit_model_ID']);
	$editModelLink = htmlspecialchars($_POST['edit_model_link']);
	$editModelOldCatLink = htmlspecialchars($_POST['edit_old_catLink']);
	$editModelOldLink = htmlspecialchars($_POST['edit_old_modelLink']);

	if(isset($editModelCat) && is_numeric($editModelCat)){

		foreach($categoriesArray as $cat){
			if($cat['ID'] === $editModelCat){
				$catLink = $cat['Link'];
			}
		}

		$catid = $editModelCat;
		$modelID = $editModelID;
		$title = $editModelTitle;
		$oldLink = $editModelOldCatLink.$editModelOldLink;
		$link = '/' . str_replace(' ', '-', $title);

		$editModelQuery = "UPDATE Models SET CatID = :CatID, Title = :Title, Link = :Link WHERE ID = $modelID";

		$data = array( 'CatID' => $catid, 'Title' => $title, 'Link' => $link);

		$modelQuery = $dbh->prepare($editModelQuery); 
		try{
			$modelQuery->execute($data);
		}catch(PDOException $e){
			echo json_encode(array('msg'=>'There was an error inserting into database. '.$e->getMessage(), 'error'=>$error));
		}

		if(file_exists($root.$pageURL.$oldLink)){
			rename($root.$pageURL.$oldLink, $root.$pageURL.$catLink.$link);
			$error = false;
			echo json_encode(array('msg'=>'ModelChanged', 'error'=>$error));
		}else{
			//if file does not exist(shouldn't happen ever)
			echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
		}	

	}else{
		//if variables aren't set
		echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
	}
};

function deleteModel(){

	global $dbh; 
	global $root;
	global $pageURL;
	global $error;

	$deleteModelID = htmlspecialchars($_POST['delete_model_ID']);
	$deleteModelLink = htmlspecialchars($_POST['delete_model_link']);
	$deleteModelCatLink = htmlspecialchars($_POST['delete_catLink']);

	if(isset($deleteModelID) && is_numeric($deleteModelID)){
		$deleteMQuery = "DELETE FROM Models WHERE ID = $deleteModelID; DELETE FROM Photos WHERE ModelID = $deleteModelID; DELETE FROM Videos WHERE ModelID = $deleteModelID";

		$finalLocation = $root.$pageURL.$deleteModelCatLink.$deleteModelLink;

		system('rm -rf ' . escapeshellarg($finalLocation), $retval);
		try{
			$dbh->exec($deleteMQuery);
		}catch(PDOException $e){
			echo json_encode(array('msg'=>'There was an error communicating with database. '.$e->getMessage(), 'error'=>$error));
		}
		
		$error = false;
		echo json_encode(array('msg'=>'Deleted!', 'error'=>$error));

		return $retval == 0; // UNIX commands return zero on success
	} else {
		echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));

	}

};

?>