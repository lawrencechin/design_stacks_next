# Design Stacks: Next

Produced using [Sketch](https://www.sketch.com).

## Homepage

![home](home.jpg)

A flatter style removing transparencies.

## Design

![categories 1](categories_1.jpg)

The sorting criteria has been moved from the homepage sidebar to the header.

![categories 2](categories_2.jpg)

A shrunken version of sort for when you enter an album.

![categories 3](categories_3.jpg)

A attempt to focus attention on the images within the album by fading the sidebar.

## Bookmarks

A work in progress!

![bookmarks 1](bookmarks_1.jpg)
![bookmarks 2](bookmarks_2.jpg)

## Feeds

![feeds 1](feeds_1.jpg)

Working on a better way to sort feeds.

![feeds 2](feeds_2.jpg)

## Temp

![temp 1](temp.jpg)

Adds a preview window on the right to review temporary images before moving them into albums.

![temp 2](temp_2.jpg)
