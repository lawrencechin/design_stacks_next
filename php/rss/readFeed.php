<?php
date_default_timezone_set('Europe/London');

include_once '../.././db/database.php';

$queryCats = "SELECT ID, Title FROM Feed_Categories";

$queryURLS = "SELECT ID, CatID, URL FROM Feed_URLS";

//Inner Join to get category in text format - easier than running loops within loops below
$queryList = "SELECT 
	PostDate, 
	Feed_List.Title, 
	Description,
	Feed_List.ID, 
	Feed_Categories.Title as CatTitle, 
	Feed_URLS.CatID
	FROM Feed_List 
	INNER JOIN Feed_URLS on URLID = Feed_URLS.ID
	INNER JOIN Feed_Categories on Feed_URLS.CatID = Feed_Categories.ID
	WHERE Read = 0 ORDER BY sortDate";

	//DESC LIMIT 200

//Shorten Feed Description
function truncateWords($input, $numwords, $padding=""){

    $output = strtok($input, " \n");
    while(--$numwords > 0) $output .= " " . strtok(" \n");
    if($output != $input) $output .= $padding;
    return $output;
}

foreach($dbh->query($queryCats, PDO::FETCH_ASSOC) as $categories){
	$feedCats[] = $categories;
}

foreach($dbh->query($queryURLS, PDO::FETCH_ASSOC) as $urls){
	$feedURLS[] = $urls;
}
?>

<!--Feed Container-->
<div id="section_feeds"> 
	<header id="feed_control">
		<!--Reveal edit menu or feed sort menu-->
		<button title="Sort Feeds Menu" onclick="feeds.sort(this)"><?php include("../../images/buttons/sort.svg"); ?></button>

		<ul id="sortFeeds">
			<?php foreach($feedCats as $cats){ ?>
			<li class="feedCat-<?php echo $cats['ID'];?>"><a id="sort_<?php echo $cats['ID'];?>"><?php echo $cats['Title'];?></a></li>
			<?php } ?>
		</ul>

		<button title="Open Edit Feed Menu" onclick="feeds.edit(this)"><?php include("../../images/buttons/edit.svg"); ?></button>
		<button title="Mark Feeds as Read Menu" onclick="feeds.markAllAsRead(this)"><?php include("../../images/buttons/submit.svg"); ?></button>

		<ul id="markFeeds">
			<p>Mark feeds as read:</p>
			<li><a href="#" data-option="Day">Older than a day</a></li>
			<li><a href="#" data-option="Week">Older than a week</a></li>
			<li><a href="#" data-option="All">All feeds</a></li>
		</ul>

		<button title="Get New Feeds" class="rss" onclick="feeds.getNew(this)"><?php include("../../images/buttons/rss.svg"); ?></button>
	</header>
	<div id="feed_list">
		<!--Loop through feed_list and populate-->
		<?php $feedList = null;

		foreach($dbh->query($queryList, PDO::FETCH_ASSOC) as $feedList){ ?>
			
			<div class="feedArticle sort_<?php echo $feedList['CatID'];?>">
				<?php $feedTitle = (strlen($feedList['Title']) > 1) ? $feedList['Title'] : "No Title"; ?>
				<h3><span class="favicon feedCat-<?php echo $feedList['CatID'];?>"></span><a class="external" data-location="feedContent" data-id="<?php echo $feedList['ID']; ?>" href="./php/rss/contentFeed.php?ID=<?php echo $feedList['ID']; ?>"><?php echo $feedTitle; ?></a></h3>
				<?php $shortenDesc = truncateWords($feedList['Description'], 30, "..."); ?>
				<p><?php echo $shortenDesc; ?></p>
				<span class="feedCat"><?php echo $feedList['CatTitle']?></span>
				<span class="feedDate"><?php echo $feedList['PostDate']; ?></span>
			</div>
		<!--end loop-->
		<?php } 
			if(count($feedList) <= 0){ ?>
				<div class="feedArticle">
				<h3>No Items</h3>
				<p>Try refreshing the feeds above</p>
				<span class="feedCat">No Posts</span>
			</div>
			<?php }
		?>
	</div>
</div>
<div id="feed_content">
	<header id="content_control">
		<button title="Reveal Main Menu" id="revealMenu" onclick="header.revealMenu()" class="mainMenu"><?php include("../../images/buttons/hamburger.svg"); ?></button>
		<button title="Bookmark Article" onclick="bookmarks.bookmarkAdd(this)" class="bookmark"><?php include("../../images/buttons/bookmark.svg"); ?></button>
		<input type="text" name="searchURL" value="http://">
		<button title="Go to Address" onclick="header.iframeChangeLocation(this)" class="search"><?php include("../../images/buttons/search.svg"); ?></button>
		<button title="Close Window"onclick="header.close()" class="close"><?php include("../../images/buttons/delete.svg"); ?></button>
	</header>
	<div id="content">
		<iframe src="about:blank" id="contentFrame" sandbox="allow-forms allow-scripts allow-same-origin"></iframe>
	</div>
</div>

<script>
header.loadExternalLinks('#section_feeds');
</script>