<?php 
date_default_timezone_set('Europe/London');
include_once '../.././db/database.php';

//check what data we were sent ID or string
if(isset($_GET['ID']) && strlen($_GET['ID']) > 5){
	$itemID = $_GET['ID'];
	//query for specific item based on ID
	$query = "UPDATE Feed_List SET Read = 1 WHERE ID = '$itemID'";
	//try to run query, catch errors then send back JSON response
	try{
		$dbh->exec($query);
		echo json_encode(array('msg'=>'All Good', 'error'=>false));
	}catch(PDOException $e){
		echo json_encode(array('msg'=>'There was an error: ' . $e->getMessage(), 'error'=>true));
	}

}else if(isset($_GET['All']) && strlen($_GET['All'])){
	$itemAll = $_GET['All'];
	//check what string has been sent, generate correct date and set query 
	if($itemAll === 'Day'){

		$yesterday  = date("Y-m-d H:i", strtotime("-1 day"));
		$query2 = "UPDATE Feed_List SET Read = 1 WHERE sortDate < '$yesterday'";

	}else if($itemAll === 'Week'){
		$lastweek = date("Y-m-d H:i", strtotime("-1 week"));
		$query2 = "UPDATE Feed_List SET Read = 1 WHERE sortDate < '$lastweek'";

	}else if($itemAll === 'All'){

		$query2 = "UPDATE Feed_List SET Read = 1";

	}else{
		echo json_encode(array('msg'=>'There was an fault in the space time continuum', 'error'=>true));
		return;
	}
	
	try{
		$dbh->exec($query2);
		echo json_encode(array('msg'=>'All Good', 'error'=>false));
	}catch(Exception $e){
		echo json_encode(array('msg'=>'There was an error: ' . $e->getMessage(), 'error'=>true));
	}

}else{
	echo json_encode(array('msg'=>'A correct ID was not supplied', 'error'=>true));
}

?>