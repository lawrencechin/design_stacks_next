<?php
date_default_timezone_set('Europe/London');

include_once '../.././db/database.php';

$query = "INSERT INTO Feed_URLS(CatID, URL) VALUES(:CatID, :URL)";

$newQuery = $dbh->prepare($query);

$array = [
	['CatId' => 5, 'URL' => 'http://www.alistapart.com/site/rss'],
	['CatId' => 3, 'URL' => 'http://www.behance.net/feeds/projects'],
	['CatId' => 5, 'URL' => 'http://www.html5rocks.com/tutorials/index.xml'],
	['CatId' => 4, 'URL' => 'feed://feeds2.feedburner.com/NiceWebType'],
	['CatId' => 5, 'URL' => 'http://rss1.smashingmagazine.com/feed/'],
	['CatId' => 3, 'URL' => 'http://feeds.feedburner.com/dspn/everyone?format=xml'],
	['CatId' => 3, 'URL' => 'http://f416.sakura.ne.jp/index.xml'],
	['CatId' => 3, 'URL' => 'http://feeds.feedburner.com/ffffound/everyone'],
	['CatId' => 3, 'URL' => 'http://feeds.feedburner.com/stuckincustoms/EmRA'],
	['CatId' => 3, 'URL' => 'http://feeds.feedburner.com/Wwwtokyotimesorg'],
	['CatId' => 1, 'URL' => 'http://feeds.feedburner.com/dezeen'],
	['CatId' => 2, 'URL' => 'http://www.underconsideration.com/fpo/atom.xml'],
	['CatId' => 4, 'URL' => 'http://feeds.feedburner.com/ILoveTypography'],
	['CatId' => 2, 'URL' => 'http://feeds.feedburner.com/LovelyPackage'],
	['CatId' => 4, 'URL' => 'http://ministryoftype.co.uk/words/rss'],
	['CatId' => 2, 'URL' => 'http://feeds2.feedburner.com/Swissmiss'],
	['CatId' => 2, 'URL' => 'http://www.thedieline.com/blog/'],
	['CatId' => 2, 'URL' => 'http://feeds.feedburner.com/Whitezinefr'],
	['CatId' => 3, 'URL' => 'http://feeds.feedburner.com/abduzeedo'],
	['CatId' => 5, 'URL' => 'http://feeds2.feedburner.com/tympanus'],
	['CatId' => 2, 'URL' => 'http://feeds.feedburner.com/designinstruct'],
	['CatId' => 5, 'URL' => 'http://feeds.feedburner.com/nettuts'],
	['CatId' => 3, 'URL' => 'http://feeds.feedburner.com/psdtuts'],
	['CatId' => 5, 'URL' => 'http://feeds.feedburner.com/vectortuts']
];


foreach($array as $item){
	$data = array('CatID' => $item['CatId'], 'URL' =>  $item['URL']);
	$newQuery->execute($data);
}

echo "It worked again!";

?>

