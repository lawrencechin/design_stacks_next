<?php 
error_reporting(E_ALL);
 ini_set("display_errors", 1);

date_default_timezone_set('Europe/London');
include_once '../../db/database.php';

//Page to edit feeds - add, remove, change category. I have decided to not allow changing the categories themselves, too much hassle for little gain

$queryCat = "SELECT ID, Title FROM Feed_Categories"; ?>
<div id="edit_cont">
	<div id="add_feed">
		<h4>Add Feed</h4>
		<form id="add_feed_form" class="contentForm" name="add_feed_form" action="./db/feed_actions.php" method="post">
			<select class="add_feed" name="add_feed">
			<?php foreach($dbh->query($queryCat, PDO::FETCH_ASSOC) as $cate){ ?>
				<option data-catColour="feedMenu_<?php echo $cate['ID']; ?>" value= "<?php echo $cate['ID']; ?>"><?php echo $cate['Title']; ?></option>
				<?php } ?>
			</select>

			<a class="selectList" href="#">Select Category…</a>
			<input type="hidden" name="feedDecision" value="add">
			<input type="text" name="add_url" value="" placeholder="Enter Url…(please include http://)" required>
			<button type="submit" name="add_submit" disabled=disabled><?php include("../../images/buttons/submit.svg"); ?></button>
		</form>
	</div>

	<h4 class="editFeeds">Edit Feed</h4>
	<form id="edit_feed_form" class="contentForm" name="edit_feed_form" action="./db/feed_actions.php" method="post">
		<select class="edit_feed" name="edit_feed">
		<?php foreach($dbh->query($queryCat, PDO::FETCH_ASSOC) as $cate){ ?>
			<option data-catColour="feedMenu_<?php echo $cate['ID'];?>" value= "<?php echo $cate['ID']; ?>"><?php echo $cate['Title']; ?></option>
			<?php } ?>
		</select>

		<a class="selectList" href="#">Select Category…</a>

		<input type="hidden" name="edit_feed_ID" value="" required>
		<input type="url" name="edit_url" value="" required>
		<input type="hidden" name="feedDecision" value="edit">
		<button type="submit" name="edit_submit" disabled=disabled><?php include("../../images/buttons/submit.svg"); ?></button>
	</form>

	<?php foreach($dbh->query($queryCat, PDO::FETCH_ASSOC) as $cat){
		$ID = $cat['ID']; 
		$queryURLS = "SELECT ID, CatID, URL FROM Feed_URLS WHERE CatID = $ID"; ?>

		<span class="feedCat-<?php echo $ID;?>"></span>
		<h3><a href="feed-<?php echo $ID;?>"><?php echo $cat['Title'];?></a></h3>

		<ul class="feed-<?php echo $ID;?>">

		<?php foreach($dbh->query($queryURLS, PDO::FETCH_ASSOC) as $urls){ ?>
			<li><span data-id="<?php echo $urls['ID'];?>" data-catid="<?php echo $urls['CatID'];?>"><?php echo $urls['URL'];?></span>

			<button title="Edit Feed Source" type="button" class="edit_feed" data-button="3" onclick="formSubmission.revealForm(this)">e</button>

			<button title="Delete Feed Source" type="button" data-button="3" class="delete_feed" onclick="formSubmission.deleteInlineForm(this)">x</button></li>
		<?php } ?> 

		</ul>
	<?php } ?>

	<form id="delete_feed_form" name="delete_feed_form" action="./db/feed_actions.php" method="post">
		<input type="hidden" name="delete_feed_ID" value="">
		<input type="hidden" name="feedDecision" value="delete">
		<input type="submit" name="submit" value="delete">
	</form>
</div>

<script>
formSubmission.submitForms('feed_form');
formSubmission.selectLists($('#add_feed_form'),true);
feeds.formValidate('[name="add_url"]');
feeds.formValidate('[name="edit_url"]');
</script>