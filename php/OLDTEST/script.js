var globals = {

	globalHeader: $('#mainHeader'),
	globalLogo: $('#logo'),
	globalContainer: $('#main'),
	globalModelID: '',
	globalWallPaperStuff: '',
	//store last clicked button based on location - used to trigger close buttons
	headerButton : '',
	midButton: '',
	contentButton: '',
	body: $('body')
};

//Contains all functionality regarding forms EXCEPT file uploads
var formSubmission = {
	init: function(findCont, nameFragment, global){
		if(global === true){
			var container = globals.globalHeader.find(findCont);
		}else{
			var container = globals.globalContainer.find(findCont);
		}
		
		this.submitForms(container, nameFragment);
	},

	revealForm: function(button){
		//wrap button in 'query, use button to find corresponding form, remove active class from button
		var $button = $(button);
		$button.removeClass('activeButton');
		formID = '#'+ $button.attr('class')+'_form',
		buttonType = parseInt($button.attr('data-button')),
		form = globals.body.find(formID);

		var buttonGlobal = buttonType === 1 ? 'headerButton' : buttonType === 2 ? 'midButton' : buttonType === 3 ? 'contentButton' : '';

		showForm(form, $button, buttonGlobal);

		if(buttonType === 2 || buttonType === 3){
			formSubmission.fillOutForm($button, form);
		}
		
		function hideForm(form, buttonG, buttonC, buttonGlobal){
			//toggle form display and toggle active button and remove selectList menu

			if(buttonG === ''){
				//global not set
				return true;
			}else if(buttonG[0] === buttonC[0]){
				//if global and clicked are same
				form.removeClass('displayForm');
				buttonG.removeClass('activeButton');
				globals.body.find('.selectConvert').remove();
				setGlobal(button, buttonGlobal, false);
				return false;
			}else{
				buttonG.removeClass('activeButton');
				var formOldId = '#'+buttonG.attr('class')+'_form';
				globals.body.find(formOldId).removeClass('displayForm');
				globals.body.find('.selectConvert').remove();

				return true;
			}
			
		}

		function showForm(form, button, buttonGlobal){

			if(hideForm(form, globals[buttonGlobal], button, buttonGlobal)){

				form.addClass('displayForm');
				button.addClass('activeButton');
				setGlobal(button, buttonGlobal, true);
				//convert select lists into a nicer looking form
				formSubmission.selectLists(form, true);
				header.revealMenu();
			}
		}

		function setGlobal(button, buttonGlobal, flag){
			//store last clicked button in global variable if different from previous click
			if(flag){
				globals[buttonGlobal] = button;
			}else{
				globals[buttonGlobal] = '';
			}
		}	
	},

	fillOutForm : function(button, form){
		var dataHolder = button.siblings()[0];
		if(dataHolder.tagName.toLowerCase() === 'div'){
			var dataF = $(dataHolder).children();
		}else{
			var dataF = $(dataHolder);
		}

		if(dataF.get(0).tagName.toLowerCase() === 'form'){

		}else{
			var attributes = dataF.data();
			console.log(attributes);
		}
		
	},

	selectLists: function(form, flag){
		//HTML fragments to build the node
		var selectToList = $('<ul class="selectConvert">'),
		count = 0,
		searchbox = $('<div class="searchBar"><input type="text" name="search_models" placeholder="Search Name…"><button></button></div>');

		if(flag){
			var body = globals.body;
			form.find('select option').each(function(){
				var $this = $(this);

				if($this.data('catsort') == undefined){
					selectToList.append('<li class="'+$this.data('catcolour')+'"><a href="'+ $this.val()+'">'+ $this.text() +'</a></li>');
					count++;

				}else{
					selectToList.append('<li class="'+$this.data('catsort')+'"><a href="'+ $this.val()+'">'+ $this.text() +'</a><span>'+$this.data('catsort')+'</span></li>');
					count++;
				}
			});

			if(count > 12){
				selectToList.prepend(searchbox); 
				selectToList.liveFilter(searchbox.find('input'), 'li');
				selectToList.find('button').on('click', function(e){
					e.preventDefault();
					body.find('.selectConvert').empty().remove();
					formSubmission.selectLists(form, true);
				});
			}

			form.find('.selectList').off().on('click', function(e){
				e.preventDefault();
				if(count > 12){
					selectToList.addClass('largeList');
				}
				
				body.append(selectToList);
				body.find('.selectConvert').off().on('click', 'a', function(e){
					e.preventDefault();
					var selectOption = $(this).attr('href'),
					selectText = $(this).text();

					form.find('a.selectList').text(selectText);
					formSubmission.selectLists(form, true);

					form.find('select').val(selectOption);
					body.find('.selectConvert').empty().remove();
				});
			});
		}
	},
	//work on!
	deleteInlineForm: function(button){
		var button = $(button),
		monster = button.attr('class'),
		formClass = 'form.'+monster,
		removeBookmark = function(){
			button.parent().remove();
		};
		
		button.parent().find(formClass).find('input[type="submit"]').trigger('click');
		button.addClass('activeButton');
		button.parent().addClass('bookmark_remove');
		setTimeout(removeBookmark, 700);

	},
	//work on this! - change PHP, use json
	submitForms: function(container, nameFragment){
		container.find('form[name*="'+nameFragment+'"]').submit(function(e){
			var $this = $(this);
			e.preventDefault();

			$.post($this.attr('action'), $this.serialize(), function(data){

				var response = data,
		    	responseCleaned = response.replace(/\s+/g, '');

				if(responseCleaned == 1 || responseCleaned == 2){
					alert('There was an error with your form');
				}else{
					$this.find('button[name^="cancel_"]').trigger('click');

					switch (responseCleaned){
						case 'bookmarkChanged':
							if(globals.globalContainer.find('#section_bookmarks').length){
								globals.globalNav.find('#bookmarks').find('a:first').trigger('click');
							}
							break;
						case 'modelChanged':
							if(globals.globalContainer.find('#section_categories').length){
								globals.globalNav.find('#stack').find('a:first').trigger('click');
							}

							if(sortTest.length){
								var wait = function(){
									sortTest.removeClass('activeFilter').find('a').trigger('click');
								};
								setTimeout(wait, 400);	
							}
							
							break;
					}
				}
			});
		});
	},

	closeForm : function(button){
		$button = $(button),
		buttonToTrigger = $button.attr('data-cancel') === 'global' ? 'headerButton' : $button.attr('data-cancel') === 'midForm' ? 'midButton' : $button.attr('data-cancel') === 'contentForm' ? 'contentButton' : false;

		globals[buttonToTrigger].trigger('click');
	}

};

var fileUpload = {
	//work on this!
    submitForm: function(form, button){
    	var bar = form.find('.smallBox'),
    	selectValue = form.find('select').val(),
    	selectValueAgain = form.find('input[name="add_model_ID"]').val(),

		finished = function(){
			button.trigger('click');
			form.resetForm();
		}
		   
		form.ajaxForm({

		    beforeSend: function(){
		    	bar.addClass('uploadProgress');
		    	var shallNotPass = $('<div id="shallNotPass"><span>Please Wait</span</div>');
		    	globals.body.append(shallNotPass.clone());
		    	shallNotPass.fadeIn();
		    },
		    
		    success: function() {
		       bar.removeClass('uploadProgress');
		       shallNotPass.remove();
		    },

		    complete: function(data){
		    	setTimeout(finished, 700);
		    	var response = data.responseText,
		    	responseCleaned = response.replace(/\s+/g, '');

		    	if(responseCleaned == 'filesChanged'){
		    		if(globals.globalStacksContainer.find('#section_models').hasClass(globals.globalModelID)){

		    			if(selectValue == globals.globalModelID || selectValueAgain == globals.globalModelID){

		    				globals.globalContainer.find('div#'+globals.globalModelID+'-jump').find('a:first').trigger('click');
		    			}
		    		}
		    		
		    	}else if(responseCleaned != 'filesChanged' &&responseCleaned.length > 0){
		    		alert(responseCleaned);
		    	}
		    	
		    }
		}); 
    },

};

//Contains functions that control basic page loading
var header = {
	//functions that run on page load
	init : function(){
		header.loadContent();
		header.homeButton();
		formSubmission.init('#globalForms','_form', true);
		fileUpload.submitForm($('#add_file'), $('button.add_file'));
		wallpapers.fontSelection();
		header.refreshFeedCount(true);
	},

	homeButton : function(){
		//Wallpapers is removed when navigating site so we clone and store it in a field then stick it back in when clicking the home button 
		globals.globalWallPaperStuff = $('#wallpapers').clone();
		globals.globalLogo.on('click', function(){
			globals.globalContainer.empty().append(globals.globalWallPaperStuff);
			wallpapers.fontSelection();
			//remove styles specfic to sections
			globals.body.removeClass('stacksSidebar');
			header.revealMenu(true);
			globals.body.find('.selectConvert').remove();

			mediaView.slideShowOff($('#slideShowButton'));
			mediaView.killVideo();
		});	
	},

	loadContent : function(){
		//load content into main section
		globals.globalHeader.on('click', 'a.navigation', function(e){
			e.preventDefault();
			$this = $(this);

			mediaView.slideShowOff($('#slideShowButton'));
			mediaView.killVideo();

			//remove styles specfic to sections
			globals.body.removeClass('stacksSidebar');
			header.revealMenu(true);

			//add class for logo animation
			globals.globalLogo.find('svg').addClass('logo_animation');

			//if using phone then add please wait div(phone takes longer to load)
			if(document.body.clientWidth <= 500){
				globals.body.append($('<div id="shallNotPass"><span>Please Wait</span</div>')).fadeIn();
			}

			globals.globalContainer.load($this.attr('href'), function(){
				//another small screen check
				if(document.body.clientWidth <= 500){
					globals.globalHeader.addClass('hide');
				}
				globals.globalLogo.find('svg').removeClass('logo_animation');
				globals.body.find('.selectConvert').remove();				
			});
		});
	},

	loadExternalLinks : function(container){
		globals.globalContainer.find(container).on('click', 'a.external', function(e){
			e.preventDefault();
			var $this = $(this),
			location = $this.data();

			//internal function to load content through different mechanisms
			function loadContent(cont, link){
				var url = $(link).attr('href'),
				$container = $(cont).next('div').find('#content');

				$container.load(url, function(){
					globals.globalLogo.find('svg').removeClass('logo_animation');
					globals.globalContainer.find('button.display_photos').trigger('click');
				});

				$container.parent('div').animate({scrollTop: 0}, 600);
			}

			//add loading animation
			globals.globalLogo.find('svg').addClass('logo_animation');
			if(location.location === "stacks"){
				globals.body.addClass('stacksSidebar');
				loadContent(container, $this);
				$(container).siblings('div').find('button.editProjectFiles').removeClass('activeButton');
			}else if(location.location === "feedContent"){
				loadContent(container, $this);
				feeds.markAsRead($this);
			}else{
				header.loadExternal($this.attr('href'));
			}
		});
	},

	loadExternal : function(link){
		//first we check the x-frame status of the website. If positive we then load link in iframe or throw an error message
		var url = 'php/bookmarks/getHeader.php';

		$.ajax({
		  url: url,
		  data: {'url' : link},
		  success: function(response){
		  	if(response.error){
		  		alert('Page unable to load in iframe');
		  		globals.globalLogo.find('svg').removeClass('logo_animation');
		  	}else{
		  		var iframe = $('#contentFrame');
		  		iframe.attr('src', link);
		  		globals.body.addClass('stacksSidebar');
		  		globals.globalLogo.find('svg').removeClass('logo_animation');
		  		var input = iframe.parent('div').siblings('header').find('input');
		  		input.val(iframe.attr('src'));
		  	}
		  },
		  dataType: 'json',
		  timeout: 10000,
		  error: function(){
		  		alert('There was an error fetching the site');
		  	}
		});
	},

	//Update Feeds badge every five minutes by callling a php page that returns the Feed_List count
	refreshFeedCount : function(flag){
		var feedLink = globals.globalHeader.find("#rss").find("a"),
		url = "./php/rss/feedCount.php";

		$.ajax({
		  type: 'GET',
		  url: url,
		  success: function(resp){
	  		feedLink.attr('data-feedcount', resp);
	  		if(flag){
	  			setTimeout(header.refreshFeedCount, 300000, true);
	  		}
		  },
		  error: function() {
		  	alert('Could not retrieve unread feed count');
		  }
		});
	},
	//when adding/editing new design projects in various places the global add file forms select lists will not be updated. This function fixes that. 
	refreshHeader : function(){
		//we have a part of the header pertaining to the select lists in a php file
		function headerStyle(data){
		    globals.globalHeader.find('#add_file_form').find('select[name="add_model_ID"]').replaceWith(data);
		}
		//we grab that data, store it in a div and replace it in the form. Simple.k 	
		$.get('./php/headerRefresh.php', headerStyle);
	},

	removeContent : function(){
		globals.globalLogo.trigger('click');
	},

	revealMenu : function(flag){
		var wrapper = globals.body.find('#wrapper'),
		$button = $('#revealMenu');

		if(flag){
			wrapper.removeClass('reveal');
			$button.addClass('activeButton');
		}else if(!globals.body.hasClass('stacksSidebar')){
			//don't add class in this event
			$button.removeClass('activeButton');
		}else{
			wrapper.toggleClass('reveal');
			$button.toggleClass('activeButton');
		}
	},

	close : function(flag){
		mediaView.slideShowOff($('#slideShowButton'));
		mediaView.killVideo();

		globals.body.removeClass('stacksSidebar');
		if(flag){
			globals.globalContainer.find('#content').empty();
		}
		globals.globalContainer.find('#content').find('#contentFrame').attr('src', "about:blank");
	},

	iframeChangeLocation : function(button){
		var $button = $(button),
		locations = $button.siblings('input').val();

		header.loadExternal(locations);
	}
};

//lazyload object 
var lazyLoad = {

	midContainer : '',
	midImage : '',
	midImgCont : '',
	contentContainer : '',
	contentImage : '',
	contentImgCont : '',

	init : function(image, imageContainer, container, flag){
		if(flag){
			this.midContainer = $(container);
			this.midImage = image;
			this.midImgCont = imageContainer;

			$(this.midImage, this.midImageContainer).lazyload({
				container : lazyLoad.midContainer
			});
		}else{
			this.contentContainer = $(container);
			this.contentImage = image;
			this.contentImgCont = imageContainer;

			$(this.contentImage, this.contentImageContainer).lazyload({
				container : lazyLoad.contentContainer
			});
		}
		
	},
	//when changing the order of images lazyload stops working correctly so we hit it up again
	triggerLoad : function(flag){
		if(flag){
			lazyLoad.init(this.midImage, this.midImgCont, this.midContainer);
		}else{
			lazyLoad.init(this.contentImage, this.contentImgCont, this.contentContainer);
		}
		
	}

};

var bookmarks = {
	//toggle classes of divs
	bookMenu : function(){
		var bookMenu = $('#bookmark_menu');
		bookMenu.on('click', 'a:not(.external)', function(e){
			e.preventDefault();
			var $this = $(this);
			//check if anything else has active class and remove except itself
			function loopList(container, link){
				container.find('div[class^="bookMenu_"]').each(function(){
					if(link.is($(this))){
					}else if($(this).hasClass('activeBook')){
						$(this).removeClass('activeBook');
						$(this).parents('#section_bookmarks').toggleClass('noScroll');
					}
				});
			}

			loopList(bookMenu, $this.parent('div'));

			//toggle class and scroll to top
			$this.parent('div').toggleClass('activeBook').end().parents('div#section_bookmarks').toggleClass('noScroll').animate({
		          scrollTop: 0
		        }, 600);
		});
	},
	//grab potential name and url from iframe and trigger the add bookmark button in header with those details
	bookmarkAdd : function(button){
		var $button = $(button),
		url = $button.siblings('input').val(),
		worldwide = url.indexOf('www');

		if(worldwide > -1){
			var index1 = url.indexOf('.')+1,
			index2 = url.indexOf('.', index1);
		}else{
			var index1 = 7,
			index2 = url.indexOf('.', index1);
		}
		
		var name = url.substring(index1, index2);

		globals.globalHeader.find('#globalForms').find('.add_bookmark').trigger('click');

		globals.globalHeader.find('#add_bookmark_form').find('input[name="title"]').val(name).end().find('input[name="url"]').val(url);
	}
};

var feeds = {
	sort : function(button){
		$button = $(button),
		menu = $button.siblings('ul:first'),
		stacksInput = $button.siblings('input');

		menu.toggleClass('show');
		if(stacksInput.hasClass('show')){
			stacksInput.removeClass('show').addClass('showUl');
		}else if(stacksInput.hasClass('showUl')){
			stacksInput.removeClass('showUl').addClass('show');
		}
		//remove click handler first otherwise we get multiple handlers each time the sort button is clicked, not ideal!
		menu.off().on('click', 'a', function(e){
			e.preventDefault();
			var $this = $(this),
			container = $button.parent('header').siblings('div');

			//removes all active classes
			function removeClass(){
				menu.find('.feedCat-1, .Graphics').removeClass('active').end().find('a').removeClass('active');
			}
			//hides all elements then displays the relevant items
			function displayFeeds(displayClass, container){
				container.find('div').hide().end().find('div'+displayClass).show();
				stacks.init();
				//trigger lazyload only if it has already been initialised
				if(lazyLoad.container !== ''){
					lazyLoad.triggerLoad(true);
				}
			}
			//different events if link clicked is already active: remove the sort
			if($this.hasClass('active')){
				removeClass();
				displayFeeds('', container);
				$button.removeClass('activeButton');
				menu.removeClass('show');
			}else{
				removeClass();
				$button.addClass('activeButton');
				if($this.attr('id') === undefined){
					displayFeeds('.'+$this.attr('class'), container);
				}else{
					displayFeeds('.'+$this.attr('id'), container);
				}
				
				$this.addClass('active');

				if($this.parent('li').hasClass('feedCat-1') || $this.parent('li').hasClass('Graphics')){
					$this.parent('li').addClass('active');
				}
			}
			
		});
	},

	edit : function(button){
		$button = $(button),
		url = './php/rss/editFeeds.php',
		content = $('div#content');

		globals.body.removeClass('stacksSidebar');

		content.off().load(url, function(){
			content.find('#edit_cont').off().on('click', 'h3 > a', function(e){
				e.preventDefault();
				content.find('ul[class*="feed-"]').hide().end().find('ul.'+$(this).attr('href')).show();
			});
		});

	},

	markAsRead : function(link){
		var ID = link.attr('data-id'),
		container = link.parents('.feedArticle'),
		url = "./php/rss/markasread.php?ID="+ID;

		feeds.readArticle();

		$.ajax({
			url : url,
			type : "GET",
			success : function(resp){
				var json = $.parseJSON(resp);
				if(json.error){
					alert(json.msg);
				}else{
					container.addClass('markedRead');
					var feedCount = $('#rss').find('a'),
					fCount = feedCount.attr('data-feedcount'),
					newCount = parseInt(fCount - 1);
					feedCount.attr('data-feedcount', newCount);
				}
			},
			error : function(resp){
				alert('There was an error marking item as read.');
			}
		});
	},

	getNew : function(button){
		//cache button, feed link in header, and get the last feed id displayed
		var $button = $(button),
		feedLink = globals.globalHeader.find("#rss").find('a'),
		url = "./php/rss/lastItem.php",
		lastFeedID = $button.parents('header').siblings('div').find('.feedArticle:first-child').find('h3').find('a').attr('data-ID');

		//start animation
		$button.addClass('animated');
		//go to lastItem.php and check what the last item is on the server. 
		function repeatGetNew(){
			$.ajax({
			  type: 'GET',
			  url: url,
			  success: function(resp) {
			  	testFeed(resp);
			  },
			  error: function() {
			  	alert('Could not retrieve unread feed count');
			  	$button.addClass('animated');
			  }
			});
		};
		//Compare IDs and run functions based on result
		function testFeed(resp){
			if(resp.length > 0 && resp.length < 2){
				getLess();
			}else if(resp.substr(1) === lastFeedID){
				getLess();
			}else{
				getMore();
			}
		}
		//If there are more items in database then simply trigger a click on feed link
		function getMore(){
			feedLink.trigger('click');
		};
		//Otherwise run the index.php and refresh the feeds
		function getLess(){
			var url = "./php/rss/index.php?password=chang01ChanG";

			$.ajax({
			  type: 'GET',
			  url: url,
			  success: function(resp){

			  	var json = $.parseJSON(resp);
			  	//check for error
			  	if(json.error){
			  		$button.removeClass('animated');
			  		alert('Something went awry :(. Msg: ' +resp.msg);
			  	}else if(parseInt(json.count) > parseInt(feedLink.attr('data-feedcount'))){
			  		//if cool then once more check if theres any actual new feeds before doing anything
		  			getMore();
		  			header.refreshFeedCount(false);
			  	}else{
			  		alert('No new feeds');
			  		$button.removeClass('animated');
			  	}
			  },
			  error: function() {
			  	alert('Could not retrieve unread feed count');
			  	$button.removeClass('animated');
			  }
			});
		};

		//first check if there are any new feeds in the database
		repeatGetNew();
	},

	markAllAsRead : function(button){
		$button = $(button),
		menu = $button.siblings('#markFeeds')

		$button.toggleClass('activeButton');
		menu.toggleClass('show');

		menu.off().on('click', 'a', function(e){
			e.preventDefault();
			url = './php/rss/markasread.php?All='+$(this).attr('data-option');
			var confirmation = confirm('Are sure?');
			getRead(url, confirmation)
		});

		function getRead(url, confirm){
			if(confirm){
				$.ajax({
					url : url,
					type : "GET",
					success : function(resp){
						var json = $.parseJSON(resp);
						if(json.error){
							alert(json.msg);
						}else{
							header.refreshFeedCount(false);
							globals.globalHeader.find("#rss").find('a').trigger('click');
						}
					},
					error : function(resp){
						alert('There was an error marking item as read.');
					}
				});
			}
		}	
	},

	readArticle : function(){
		//store iframe in variable to insert, cache content div
		var iframe = $('<iframe src="about:blank" id="contentFrame" sandbox="allow-forms allow-scripts allow-same-origin"></iframe>'),
		content = $('#content');
		//add click handler to content links
		content.off().on('click', 'a', function(e){
			e.preventDefault();
			//insert iframe into content
			content.empty().append(iframe);
			header.loadExternal($(this).attr('href'));
		});
	}
};

var stacks = {

	init : function(){
		//grab elements needed and run functions
		var searchStacks = $('#searchStacks'),
		$container = $('#stacksScrollContainer');

		stacks.searchProjects(searchStacks, $container);
		stacks.filterProjects('#stacks_control');
	},

	filterProjects: function(header){
		var $header = $(header);
		//needs to be switched off before on as will run many times use tsort to sort asc,desc or rand on whole list of projects
		$header.find('span').off().on('click', 'a', function(e){
			e.preventDefault();
			var ordered = $(this).attr('href');

			$header.siblings('div').find('div').tsort('h3', {
				order : ordered
			});
			
			lazyLoad.triggerLoad(true);
		});

	},

	searchProjects: function(input, container){
		//grab list of visible divs. Probably a slow and costly way to do this. This must be changed each time another event occurs so we can search a filtered list and not mess up the sort by function
		var visibleStacks = container.find('div:visible');
		//function to delay keyup so we wait until a more usable string is collected
		var delay = (function(){
		  var timer = 0;
		  return function(callback, ms){
		    clearTimeout (timer);
		    timer = setTimeout(callback, ms);
		  };
		})();
		//bind the event, add the delay and finally run the function where the real stuff happens
		input.off().keyup(function() {
		    delay(function(){
		      filterItems(input.val(), visibleStacks);
		     lazyLoad.triggerLoad(true);
		    }, 600 );
		});
		//grab search query and list of visible divs. Check for blank and show all otherwise search h3 element for string and hide anything that doesn't match
		var filterItems = function(inputVal, visibleStacks){
			if(inputVal === ' '){
				visibleStacks.each(function(){
					$(this).show();
				});
			}else{
				visibleStacks.each(function(){
					var $this = $(this);
					if ($this.find('h3').text().search(new RegExp(inputVal, "i")) < 0) {
		                $this.hide();
			        } else {
			            $this.show();
			        }
				});
			}
		}
	},

	showSearch : function(button){
		var $button = $(button),
		ul = $button.siblings('ul'),
		input = $button.siblings('input');

		if(globals.body.hasClass('stacksSidebar')){
			if(ul.hasClass('show')){
				if(input.hasClass('showUl')){
					input.removeClass('show').removeClass('showUl');
				}else{
					input.removeClass('show').addClass('showUl');
				}
			}else{
				if(input.hasClass('show')){
					input.removeClass('showUl').removeClass('show');
				}else{
					input.removeClass('showUl').addClass('show');
				}
				
			}
			$button.toggleClass('activeButton');
		}
	}
};

var projects = {
	//alternate between images and videos - videos needs to display as flex so can't use .show()
	reveal : function(button){
		var $button = $(button),
		fileType = $button.attr('data-filetype');

		if(fileType === "images"){
			globals.globalContainer.find('#model_pictures').show().end().find('#model_videos').removeClass('displayFlex');
			$button.addClass('activeButton').next('button').removeClass('activeButton');
		}else{
			globals.globalContainer.find('#model_videos').addClass('displayFlex').end().find('#model_pictures').hide();
			$button.addClass('activeButton').prev('button').removeClass('activeButton');
			lazyLoad.init('img', 'div.modelVideos', '#section_cat_content', false);
		}
	},
	//seperate function for showing project forms - too much work to get the other form function to work
	showForm : function(button){
		var $button = $(button),
		spacePos = $button.attr('class').indexOf(" ");
		if(spacePos > 0){
			classBtn = $button.attr('class').substr(0, spacePos);	
		}else{
			classBtn = $button.attr('class');
		}
		
		$button.toggleClass('activeButton');

		$('#section_cat_content').find('#'+classBtn).toggleClass('show');
	},
	//seperate show form requires seperate remove
	removeForm : function(button){
		var $button = $(button),
		btnClass = $button.parent('form').attr('id');

		$('#section_cat_content').find('button.'+btnClass).trigger('click');	
	},

	loadContent : function(container){
		//kill the slideshow which will persist if the cancel button isn't triggered
		mediaView.slideShowOff($('#slideShowButton'));

		var $container = $(container),
		$mediaContainer = $container.find('#media');
		//setup the mediaView variables when loading projects
		mediaView.init($mediaContainer);

		$container.off().on('click', 'a.onlyThis', function(e){
			e.preventDefault();
			var $this = $(this),
			link = $this.attr('href'),
			holder = $this.parent('div'),
			idFlag = holder.attr('id').substr(-3);
			mediaView.show(link, holder, idFlag);
		});
	},
	//work on this
	submitPhotoForm : function(button){
		var buttonClick = globals.globalStacksContainer.find('button.'+button),
		form = globals.globalStacksContainer.find('form.'+button);

		form.submit(function(e){
			e.preventDefault();
			var $this = $(this),

			checkedBox = globals.globalStacksContainer.find('input[type="checkbox"]:checked').map(function(){
			  return this.value;
			}).get();

			checkedboxDOMElements = globals.globalStacksContainer.find('input[type="checkbox"]:checked').parent();

			var wait = function(){
				checkedboxDOMElements.remove();	
			}

			if(globals.globalStacksContainer.find('input[type="checkbox"]:checked').attr('name') == "multi-select-video"){
				form.find('input[name=videoFlag]').val(true);
			};
			
			form.find('input[name=IDArray]').val(checkedBox);

			if (checkedBox.length > 0){
				$.post($this.attr('action'), $this.serialize(), function(data){
					form.find('input[name=IDArray]').val('');
					models.deselectChecks();
					setTimeout(wait, 600);
					checkedboxDOMElements.addClass('bookmark_remove');
					if(form.attr('name') == 'delete_photo_models'){
						
					} else{
						buttonClick.trigger('click');
					}
					
				});
			} else {
				alert('You have not selected any files');
				return false;
			}
		});
	},
	//work on this
	triggerSubmit : function(button){
		globals.globalStacksContainer.find('form.'+button).find('input[type="submit"]').trigger('click');
		// var form = globals.globalStacksContainer.find('form.'+button).find('input[type="submit"]').trigger('click');
	},

	sorting : function(){
		var pics = globals.globalContainer.find('#model_pictures'),
		vids = pics.siblings('#model_videos');

		globals.globalContainer.find('#models_header').find('span').off().on('click', 'a', function(e){
			e.preventDefault();

			var $this = $(this),
			sortingCriteria = $this.attr('href');

			function sort(container){
				pics.find('div').tsort({
					attr: 'id',
					order: sortingCriteria
				});
				lazyLoad.triggerLoad(false);
			}

			if(pics.is(':visible')){
				sort(pics)
			}else{
				sort(vids)
			}
		});
	},

	editMode : function(button){
		var $button = $(button);
		$button.toggleClass('activeButton');
		//grab buttons and enable disabled buttons
		var btn = $button.siblings('button'),
		btnLength = btn.length;
		disenableBtn();

		function disenableBtn(){
			if($button.hasClass('activeButton')){
				for(var i=1; i <= 4; i++){
					$(btn[btnLength -i]).prop('disabled', false);
				}
			}else{
				for(var i=1; i <= 4; i++){
					$(btn[btnLength -i]).prop('disabled', true);
				}
			}
		}
		//change the click handler if in edit mode or return the click handler back to normal state
		if($button.hasClass('activeButton')){
			projects.editContent('#section_models');
		}else{
			//remove active class from links if done editing
			$button.parents('#section_cat_content').find('.activeTemp').removeClass('activeTemp');
			projects.loadContent('#section_models');
		}
	},

	editContent : function(container){
		var $container = $(container);

		$container.off().on('click', 'a.onlyThis', function(e){
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass('activeTemp');
		});
	},

	select : function(button){
		var $button = $(button),
		$container = $button.parents('#section_cat_content');

		$button.toggleClass('activeButton');

		if($button.hasClass('activeButton')){
			$container.find('.onlyThis').addClass('activeTemp');
		}else{
			$container.find('.onlyThis').removeClass('activeTemp');
		}
	}

};

var crop = {
	init: function(button){
		//grab all selected files and send to crop.php
		var $button = $(button),
		$container = $button.parents('#section_cat_content');
		var picturesToCrop = $container.find('.activeTemp'),
		thumbArray = [],
		picturesArray = [];
		//check if anything was selected
		if(picturesToCrop.length > 0){
			picturesToCrop.each(function(){
				var $this = $(this);

				var thumb = $this.find('img').attr('src'),
				picture = $this.attr('href');
				thumbArray.push(encodeURIComponent(thumb));
				picturesArray.push(encodeURIComponent(picture));
			});
			//create and append crop div
			var container = $('<div id="mega">');
			container.load('./php/thumbs/crop.php?tArr='+thumbArray+'&pArr='+picturesArray);
			globals.body.append(container).find(container).fadeIn();

		}else{
			alert('Please select at least one photo to crop');
		}		
	},

	selectImage: function(list){
		//get image from list and initialise crop object
		var $list = $(list),
		containerWidth = parseInt($('.main_crop_area').width()),
		aspRatio, convertedHeight;

		$list.on('click', 'a', function(e){
			e.preventDefault();
			var mainImage = $('#jcrop');
			var jcrop_api;
			$list.addClass('dim');

			mainImage.attr('src', $(this).attr('href')).one('load', function(){
				//get actual image dimensions
				aspRatio = ($(this)[0].naturalWidth/$(this)[0].naturalHeight);
				//check if it's landscape or portrait
				if($(this)[0].naturalWidth > $(this)[0].naturalHeight){
					convertedHeight = Math.round(containerWidth / aspRatio);
				}else{
					convertedHeight = Math.round(containerWidth * aspRatio);
				}
				//run jcrop with new image dimensions
				cropDelayed();
			});

			function cropDelayed(){
				mainImage.Jcrop({
					//trueSize: [containerWidth, convertedHeight],
					boxWidth: containerWidth,
					//boxHeight: convertedHeight,
					onSelect: crop.showCoords,
			        bgColor:     'black',
		            bgOpacity:   .4,
		            keySupport: false,
		            aspectRatio: 200 / 250
				}, function(){
					jcrop_api = this;
					//setup cancel crop
					crop.removeCrop(jcrop_api, $list);
				});
			}
		});
	},

	removeCrop: function(cropObject, list){
		var form = $('#hidden_crop_form');
		$('#cancel_crop').show().off().on('click', function(){
			cropObject.destroy();
			list.removeClass('dim');
			$('#jcrop').attr('style', '').attr('src', '');	
			$(this).hide().siblings('button').hide();
		});

		$('#submit_crop').show().off().on('click', function(){
			list.removeClass('dim');
			$(this).hide().siblings('button').hide();
			cropObject.destroy();
			$('#jcrop').attr('style', '').attr('src', '');
			form.find('input[type="submit"]').trigger('click');
		});
	},

	submitForm: function(form){
		form.submit(function(e){
			e.preventDefault();
			var $this = $(this);
			$.post($this.attr('action'), $this.serialize(), function(data){
				var pos = data.indexOf('DesignNxt/')+9,
				final = "." + data.substring(pos);
				crop.reviewSteps(final);
			});
		});
		
	},

	reviewSteps: function(newImage){
		var holder = $('#crop_comparrison')
		holder.find('#new_image').attr('src', newImage);
		holder.fadeIn();
	},

	finalise: function(){
		//change, fade out container then do background work
		var form = $('#accept_crop_form');

		var frustrating = function(){
			form.find('input[type="submit"]').trigger('click');
		}

		form.submit(function(e){
			var containerMain = $('#mega');
			e.preventDefault();
			var $this = $(this);
			$.post($this.attr('action'), $this.serialize(), function(data){
				if(data == "Done!"){
					alert(data);
					containerMain.remove();
				} else if(data == "failed"){
					alert(data);
				} else{
					containerMain.empty().remove();
				}
				
			});
		});
		
		setTimeout(frustrating, 600);
	},

	cancelAndClose: function(){
		$('#accept_crop_form').find('#decision_cor').val('false');
		crop.finalise();
	},

	closeFinal : function(){
		$('#crop_comparrison').fadeOut();
	},

	showCoords: function(c){
		var form = $('#hidden_crop_form'),
		imageSrc = $('#jcrop').attr('src');
		form.find('#x_cor').val(c.x);
		form.find('#y_cor').val(c.y);
		form.find('#w_cor').val(c.w);
		form.find('#h_cor').val(c.h);
		form.find('#image_cor').val(imageSrc);
	}
};

var temp = {
	switchFolders: function(){
		globals.globalContainer.find('#dir_list').off().on('click', 'a.folders', function(e){
			e.preventDefault();
			globals.globalContainer.load($(this).attr('href'));
		});
	},

	selectFiles: function(){
		//select and store DOM elements then clone and change. These will get destroyed hence the cloning. 
		var contentCont = globals.globalContainer.find('#display_panel'),
		tempMessage = contentCont.find('#tempMessageCont'),
		tempMessageOriginal = tempMessage.clone(),
		tempMessageFileType = tempMessage.clone();

		tempMessageFileType.find('p').text('Non-image selected');

		globals.globalContainer.find('#file_list').off().on('click', 'a', function(e){
			e.preventDefault();

			//cache link and data attributes
			var $this = $(this),
			dataOriginal = $this.find('img').attr('data-original'),
			notFile = $this.find('span').find('img').attr('data-notfile');

			$this.toggleClass('activeTemp');
			//get file location and list of currently selected files
			var tempImage = "<img src='DesignNxt/"+decodeURIComponent(dataOriginal)+"'>",
			fileList = globals.globalContainer.find('#file_list').find('a.activeTemp');

			//check if anything is selected then check if it isn't a file and react accordingly
			if(fileList.length > 0){
				if(!$this.hasClass('activeTemp')){
					//don't add image if already selected
				}
				else if(notFile === 'true'){
					contentCont.html(tempMessageFileType);
				}else{
					contentCont.html(tempImage);
				}
			}else{
				contentCont.html(tempMessageOriginal);
			}
			
		});
	},

	tempFunction: function(moveOrDelete){
		var fileList = globals.globalContainer.find('#file_list').find('a.activeTemp'),
		fileArray = [],
		postString = moveOrDelete,
		modelID = globals.globalContainer.find('#dir_list').find('select').find('option:selected').val(),

		//remove selected files from DOM after animation has played
		wait = function(){
			fileList.parent().remove();	
			globals.body.find('#shallNotPass').remove();
		};

		//create array to pass as post data to php file
		fileList.each(function(){
			fileArray.push($(this).find('img').data('original'));
		});

		//construct post string
		var finalPostString = "move="+postString+"&modelID="+modelID+"&fileArray="+fileArray+"";

		//check if we're actually sending anything and then post
		if(fileArray.length){
			var shallNotPass = $('<div id="shallNotPass"><span>Please Wait</span</div>');
		    globals.body.append(shallNotPass.clone());
		    shallNotPass.fadeIn();

		    //grab clone inserted into DOM
		    var snp = globals.body.find('#shallNotPass');
		    //double check deletion is wanted
		    if(postString === 'delete'){
		    	var go = confirm('Are you sure you want to delete selected files? Regrets?');
		    }else if(postString === 'move'){
		    	go = true;
		    }

		    if(go){
		    	$.ajax({
				  type: "POST",
				  url: './db/temp_actions.php?',
				  data: finalPostString,
				  datatype: 'json',
				  //check for errors
				  success: function(response){
				  	console.log(response);
				  	var json = $.parseJSON(response);
				  	if(!json.error){
				  		fileList.parent().addClass('bookmark_remove');
				  		snp.remove();
				  		setTimeout(wait, 600);
				  	}else{
				  		snp.remove();
				  		alert(json.msg);
				  	}	
				  }
				});
		    }else{
		    	snp.remove();
		    }

		//otherwise throw an error	
		}else {
			alert('Please select at least one file!');
		}
	},

};

var wallpapers = {
	init: function(){
		var wallPapers = globals.globalContainer.find('#wallpapers'),
		wallPapersControl = wallPapers.find('#wallpaperControl'),
		backgroundImg = $('#bg').find('img');

		wallPapers.toggleClass('active');
		if(wallPapersControl.length){
			wallPapersControl.remove();
		}else{
			$.get('./php/wallpapers.php', function(data){
				wallPapers.append(data);
				$('#wallpaperControl').on('click', 'img', function(){
					var imgSrc = $(this).data('original');
					backgroundImg.attr('src', imgSrc);
				});
			});
			
		}
	},

	remove: function(button, ID){
		$button = $(button),
		wait = function(){
			$button.parent().remove();
		};

		$.get('./db/fav_actions.php?decision=remove&PhotoID='+ID, function(data){

			$button.parent().addClass('bookmark_remove');
			setTimeout(wait, 700);

			if(data == 1){
				alert('There was an error with your form');
			}
		});
	},

	fontSelection: function(){
		globals.globalContainer.find('#wallpapers').off().on('click', 'p.font', function(){
			var fontClass = $(this).text().toLowerCase();

			if(fontClass == 'avenir'){
				globals.body.removeClass();
			}else{
				globals.body.removeClass().addClass(fontClass);
			}
		});
	}
};

var videoThumbs = {
	init: function(button){
		var $button = $(button),
		link = $button.siblings('a'),
		linkage = link.attr('href'),
		linkThumb = link.find('img').data('original'),

		container = $('<div id="mega">'),
		span = $('<span id="bktop" class="loading">Please Wait while generating thumbs</span>');
		container.append(span);
		globals.body.append(container);

		container.load('./php/videoThumbs.php?linkage='+encodeURIComponent(linkage)+'&linkThumb='+encodeURIComponent(linkThumb)+'&descision=false', function(data){
			container.prepend(span).find('span').text('Select a new thumb').removeClass('loading');

			container.find('#videoThumbnailselect').on('click', 'a', function(){

				var newImage = $(this).find('img').data('original');

				$(this).parent().parent().find('a.activeTemp').removeClass('activeTemp').end().end().end().addClass('activeTemp');

				document.getElementById('bktop').scrollIntoView(true);

				span.text('Accept new thumb?').off().on('click', function(){

					$.get('./php/videoThumbs.php?linkage='+encodeURIComponent(linkage)+'&linkThumb='+encodeURIComponent(linkThumb)+'&descision=true&newThumb='+encodeURIComponent(newImage), function(data){

						var dataCleaned = data.replace(/\s+/g, '');

						if(dataCleaned == "success"){
							var refreshIMG = link.find('img'),
							refreshAttr = refreshIMG.attr('src'),
							seconds = new Date().getTime() / 1000,
							finalRefresh = refreshAttr+'?'+seconds;

							refreshIMG.attr('src', finalRefresh);

							span.siblings('div').find('button').trigger('click');

						}else{
							alert('Error! ARGH! Real monsters!');
						}
					});	
				});
			});
		});
	},

	cancelAndClose: function(button){
		var $button = $(button);

		$button.parent().parent().remove();
	}
};

var mediaView = {

	holder : '',
	slideshow : '',
	video : '',
	img : '',
	header : '',
	picControls : '',
	vidControls : '',
	linkedDiv : '',

	init: function(container){
		//set fields of object used for other functions
		this.holder = container.find('.holder'),
		this.header = container.find('#mediaControlBar'),
		this.img = this.holder.find('img'),
		this.video = this.holder.find('video'),
		this.picControls = this.header.find('#pictureControls'),
		this.vidControls = this.header.find('#videoControls');
	},

	show: function(link, div, flag){
		this.keyBoardControls();

		if(this.header.parent().hasClass('hide')){
			this.header.parent().removeClass('hide');
		}
		this.linkedDiv = div;

		function delayPicLoading(){
			mediaView.img.attr('src', link).fadeIn();
		}
		//hide/display elements based on vids or pics and show container
		if(flag === 'pic'){
			this.video.hide();
			this.vidControls.hide();
			this.picControls.show().css("display", "inline-block");
			this.img.fadeOut();
			setTimeout(delayPicLoading, 400);
		}else{
			this.video.fadeIn().attr('src', link);
			this.vidControls.show().css("display", "inline-block");
			this.picControls.hide();
			this.img.hide();
			this.videoInit();
		}

		this.header.parent().fadeIn();
	},

	hide: function(button){
		$button = $(button);
		$button.toggleClass('flip');
		$button.parents('#media').toggleClass('hide');
	},

	close: function(button){
		$button = $(button),
		$container = $button.parents('div#media');
		//stop slideshows/videos playing
		mediaView.slideShowOff($container.find('button#slideShowButton'));
		mediaView.killVideo();
		//kill all content
		this.img.attr('src', '');
		this.video.attr('src', '');
		
		$container.fadeOut();

		//remove keyboard short cuts from window
		$(window).off('keydown');
	},

	killVideo : function(){
		if(this.video === ''){
			//do nothing
		}else{
			this.video[0].pause();
			$('#play-pause').removeClass('playActive');
		}
	},

	back: function(){
		//grab prev div to one that was clicked and trigger otherwise get last div
		if(this.linkedDiv.prev().length){
			this.linkedDiv.prev().find('a').eq(0).trigger('click');
		}else{
			this.linkedDiv.siblings().eq(-1).find('a').eq(0).trigger('click');
		}
	},

	forward: function(){
		//same as back function but obviously checks next div. Note: previously I used the keyword 'this' but when calling the function from slideShow it returns undefined. Curious.

		if(mediaView.linkedDiv.next().length){
			mediaView.linkedDiv.next().find('a').eq(0).trigger('click');	
		}else{
			mediaView.linkedDiv.siblings().eq(0).find('a').eq(0).trigger('click');
		}
	},

	slideShow: function(button){
		$button = $(button);
		$button.attr('onclick', 'mediaView.slideShowOff(this)').addClass('slideShowOn');
		mediaView.slideshow = setInterval(mediaView.forward, 5000);
	},

	slideShowOff: function(button){
		$(button).removeClass('slideShowOn').attr('onclick', 'mediaView.slideShow(this)');
		clearInterval(mediaView.slideshow);
	},

	imageOriginal: function(button){
		$button = $(button);
		this.img.removeClass('fullWidth').addClass('original');
	},

	imageFullWidth: function(button){
		$button = $(button);
		this.img.removeClass('original').addClass('fullWidth');
	},

	imageToFit: function(button){
		$button = $(button);
		this.img.removeClass('original').removeClass('fullWidth');
	},
	//work on this
	favourites : function(button){
		$button = $(button);

		if($button.hasClass('activated')){
			$.get('./db/fav_actions.php?decision=remove&PhotoID='+photoID, function(data){
				$button.removeClass('activated');
				if(data == 1){
					alert('There was an error with your form');
				}
			});

		}else{
			$.get('./db/fav_actions.php?decision=add&PhotoID='+photoID, function(data){
				$button.addClass('activated');
				if(data == 1){
					alert('There was an error with your form');
				}
			});
		}
	},
	//work on this - no current delete function works
	trash: function(){
		mediaView.holder.find('button.delete_photo').trigger('click');
		mediaView.forward();
	},

	videoInit: function(){
		var seekBar = document.getElementById("seek-bar");
  		var volumeBar = document.getElementById("volume-bar");

  		//nb: we need a plain javascript object so we need [0] on all calls to mediaView.video, sucks

		seekBar.addEventListener("change", function() {
  		// Calculate the new time
		var time = mediaView.video[0].duration * (seekBar.value / 100);

		// Update the video time
		mediaView.video[0].currentTime = time;
		});

		// Update the seek bar as the video plays
		mediaView.video[0].addEventListener("timeupdate", function() {
		  // Calculate the slider value
		  var value = (100 / mediaView.video[0].duration) * mediaView.video[0].currentTime;

		  // Update the slider value
		  seekBar.value = value;
		});

		// Pause the video when the slider handle is being dragged
		seekBar.addEventListener("mousedown", function() {
		  mediaView.video[0].pause();
		});

		// Play the video when the slider handle is dropped
		seekBar.addEventListener("mouseup", function() {
		  mediaView.video[0].play();
		});

		// Event listener for the volume bar
		volumeBar.addEventListener("change", function() {
		  // Update the video volume
		  mediaView.video[0].volume = volumeBar.value;
		});
	},

	play: function(button){
		if (mediaView.video[0].paused === true) {
 			mediaView.video[0].play();
 			$(button).addClass('playActive');
 		}else{
 			mediaView.video[0].pause();
 			$(button).removeClass('playActive');
 		}
	},

	mute: function(button){
		 if (mediaView.video[0].muted == false) {
		 	mediaView.video[0].muted = true;
		 	$(button).addClass('muted');
		 } else {
		 	mediaView.video[0].muted = false;
		 	$(button).removeClass('muted');
		 }
	},

	fullscreen: function(){
		if (mediaView.video[0].requestFullscreen) {
		    mediaView.video[0].requestFullscreen();
		} else if (mediaView.video[0].mozRequestFullScreen) {
		    mediaView.video[0].mozRequestFullScreen();
		} else if (mediaView.video[0].webkitRequestFullscreen) {
		    mediaView.video[0].webkitEnterFullscreen();
		}
	},

	keyBoardControls: function(){

		$(window).off('keydown').on('keydown', function(e) {

			switch(e.which) {
				case 32: //space - slideshow
				if(mediaView.vidControls.is(':visible')){
					mediaView.header.find('#play-pause').trigger('click');
				}else{
					mediaView.header.find('#slideShowButton').trigger('click');
				}
		        break;

		        case 37: // left - previous pic
		        mediaView.header.find('.prevPic').trigger('click');
		        break;

		        case 38: // up - trigger image to fit
		        mediaView.header.find('.imageToFit').trigger('click');
		        break;

		        case 39: // right - next image
		        mediaView.header.find('.nextPic').trigger('click');
		        break;

		        case 40: // down - toggle full width pic
		        mediaView.header.find('.imageFullWidth').trigger('click');
		        break;

		        case 70:// F - trigger fav
		        mediaView.header.find('.fav').trigger('click');
		        break;

		        case 88: // X - close Window
		        mediaView.header.find('.mediaClose').trigger('click');
		        break;

		        default: return; // exit this handler for other keys
		    }
	    	e.preventDefault(); // prevent the default action (scroll / move caret)
		});
	} 
};










