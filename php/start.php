<?php
include_once './db/database.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Design Stacks: Next</title>
    <link href="./css/screen-prefixed.css" media="screen, projection" rel="stylesheet" type="text/css" />
    <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta name=viewport content="width=device-width,initial-scale=1 maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="apple-touch-icon" href="./images/logo/logo.png"/>
</head>

<body id="home">
<div id="expose_menus">
    <div class="expose_main"><a href="#"><?php include './images/buttons/hamburger.svg';?></a></div>
    <div class="expose_feeds"><a href="#"><?php include './images/buttons/rss.svg';?></a></div>
    <div class="expose_books"><a href="#"><?php include './images/buttons/book.svg';?></a></div>
    <div class="expose_stacks"><a href="#"><?php include './images/buttons/stack.svg';?></a></div>
</div>
<div id="bg">
	<img src="" alt="fullscreen Background">
</div>