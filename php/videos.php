<div id="mediaContainer">

	<header id="mediaControlBar">
		<button title="Shift Window Right" class="shiftWindow" onclick="mediaView.hide(this)"><?php include("../images/buttons/back.svg"); ?></button>

		<button title="Close Window(Keyboard Shortcut: X)" class="closeMedia" onclick="mediaView.close(this)"><?php include("../images/buttons/delete.svg"); ?></button>
		<button title="Previous File(Keyboard Shortcut: &larr;)" class="prevPic" onclick="mediaView.back()"><?php include("../images/buttons/browserBack.svg"); ?></button>
		<button title="Next File(Keyboard Shortcut: &rarr;)" class="nextPic" onclick="mediaView.forward()"><?php include("../images/buttons/browserForward.svg"); ?></button>

		<div id="pictureControls">
			<button title="Slideshow(Keyboard Shortcut: Space)" id="slideShowButton" onclick="mediaView.slideShow(this)"><?php include("../images/buttons/play.svg"); ?></button>
			<button title="Add/Remove Favourite(Keyboard Shortcut: F)" class="fav" onclick="mediaView.fav(this)"><?php include("../images/buttons/favourite.svg"); ?></button>
			<button title="Delete File" onclick="mediaView.trash()"><?php include("../images/buttons/trash.svg"); ?></button>
			<button title="Image Original Size" onclick="mediaView.imageOriginal(this)"><?php include("../images/buttons/originalSizenobox.svg"); ?></button>
			<button title="Image Fit Width(Keyboard Shortcut: &darr;)" class="imageFullWidth" onclick="mediaView.imageFullWidth(this)"><?php include("../images/buttons/fullWidthnobox.svg"); ?></button>
			<button title="Image Fit Height(Keyboard Shortcut: &uarr;)" class="imageToFit" onclick="mediaView.imageToFit(this)"><?php include("../images/buttons/fullHeightnobox.svg"); ?></button>
		</div>

		<div id="videoControls">
			<button title="Play(Keyboard Shortcut: Space)" type="button" onclick="mediaView.play(this)" id="play-pause"><?php include("../images/buttons/play.svg"); ?></button>
		    <input type="range" id="seek-bar" value="0">
		    <button title="Mute" onclick="mediaView.mute(this)" type="button" id="mute"><?php include("../images/buttons/audio.svg"); ?></button>
		    <input type="range" id="volume-bar" min="0" max="1" step="0.1" value="1">
		    <button title="Fullscreen" onclick="mediaView.fullscreen()" type="button" id="full-screen"><?php include("../images/buttons/move.svg"); ?></button>
		    <button title="Delete Video" onclick="mediaView.trash()"><?php include("../images/buttons/trash.svg"); ?></button>

		</div>
	</header>

	<div class="holder">
		<img src="" style="display:none" />
		<video id="mainVideo" type="video/mp4" style="display:none" src="" width="1000" height="550"></video>
	</div>
</div>

<script> 
	//mediaView.keyBoardControls('#mediaContainer');
</script>

