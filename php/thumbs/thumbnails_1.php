<?php

// ini_set('display_errors',1);
// error_reporting(E_ALL);

function videoThumbs($in, $out) {
	// where ffmpeg is located  
	$ffmpeg = '/usr/local/Cellar/ffmpeg/2.2.3/bin/ffmpeg';
	$ffprobe = '/usr/local/Cellar/ffmpeg/2.2.3/bin/ffprobe';

	$command = "$ffprobe -v quiet -show_format -print_format flat $in";

	exec ($command, $results);

	foreach ($results as $key=>$value){
		if(strpos($value, "duration")){
			$duration = $value;
		}
	}

	$truncatePos = strpos($duration, '"');

	$truncate = substr($duration, $truncatePos+1, -1);

	$finalNumber = round($truncate);

	$interval = rand(0, $finalNumber); 
	  
	//screenshot size  
	$size = '640x360';  
	
	$cmd = "$ffmpeg -ss $interval -i $in -s $size -an -vframes 1 -f image2 $out";

	exec($cmd);	
}


define('THUMB_WIDTH', 200);
define('THUMB_HEIGHT', 250);
define('MAGICK_PATH','/usr/local/Cellar/imagemagick/6.8.9-1/bin/');

function makeThumbnail($in, $out) {
    $width = THUMB_WIDTH;
    $height = THUMB_HEIGHT;
    list($w,$h) = getimagesize($in);

    $thumbRatio = $width/$height;
    $inRatio = $w/$h;
    $isLandscape = $inRatio > $thumbRatio;

    $size = ($isLandscape ? '1000x'.$height : $width.'x1000');
    $xoff = ($isLandscape ? floor((($inRatio*$height)-$width)/2) : 0);
    
    $in = $in.'[0]';

    $command = MAGICK_PATH."convert $in -resize $size -crop {$width}x{$height}+{$xoff}+0 ".
        "-quality 80 $out";

    exec($command);

}

$crop = array_key_exists('crop_cor', $_POST) ? $_POST['crop_cor'] : null;

if(isset($crop) && $crop == 'crop'){
  generateModelThumb();
}elseif(isset($crop) && $crop == 'replace'){
	replaceAndCopy();
}

function generateModelThumb(){

	$cropX = htmlspecialchars($_POST['x_cor']);
	$cropY = htmlspecialchars($_POST['y_cor']);
	$cropW = htmlspecialchars($_POST['w_cor']);
	$cropH = htmlspecialchars($_POST['h_cor']);
	$currentImage = htmlspecialchars($_POST['image_cor']);
	$path = htmlspecialchars($_POST['path_cor']);

	$in = "/Library/WebServer/Documents/DesignNxt".substr($currentImage, 1);

	$out = "/Library/WebServer/Documents/DesignNxt".substr($path, 1)."thumb-1.png";

	$command = MAGICK_PATH."convert $in -crop {$cropW}x{$cropH}+{$cropX}+{$cropY} -resize 200x250 -format png $out";

    exec($command);

    echo $out;

}

function replaceAndCopy(){
	$decision = htmlspecialchars($_POST['decision_cor']);
	$currentImage = htmlspecialchars($_POST['image_cor']);

	$imageRejig = "/Library/WebServer/Documents/DesignNxt" . substr($currentImage, 1) . "thumb.png";
	$newImage = "/Library/WebServer/Documents/DesignNxt" . substr($currentImage, 1) . "thumb-1.png";
	if(is_file($imageRejig) || is_file($newImage))
    {
    	if($decision == 'true'){
    		rename($newImage, $imageRejig);
    		echo "Done!";
    	}else{
    		unlink($newImage);
    	}
    } else{
    	echo "Failed";

    }
}
	
?>