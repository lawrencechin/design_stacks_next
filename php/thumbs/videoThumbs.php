<?php
ini_set('display_errors',1);
error_reporting(E_ALL);

date_default_timezone_set('UTC');
include_once ('../../db/database.php');
$in = htmlspecialchars_decode($_GET['linkage']);
$inThumb = htmlspecialchars_decode($_GET['linkThumb']);
$acceptNewThumb = htmlspecialchars($_GET['decision']);

$tempFolder = ROOT.PAGEURL.THUMBDIR."/video/";
$inFull = ROOT.PAGEURL.substr($in, 1);
$inFullEscape = escapeshellarg($inFull);
$inThumbFull = $inThumb;

if(isset($acceptNewThumb) && $acceptNewThumb == 'false'){
	generateThumbs($inFullEscape, $tempFolder);
}else if(isset($acceptNewThumb) && $acceptNewThumb == 'true'){
	switchThumbs($inThumbFull);
}

function generateThumbs(&$inFullEscape, &$tempFolder){
	//
	$command = FFPROBE. " -v quiet -show_format -print_format flat $inFullEscape";
	$size = VIDSIZE; 

	exec($command, $results);
	foreach ($results as $key=>$value){
		if(strpos($value, "duration")){
			$duration = $value;
		}
	}
	$truncatePos = strpos($duration, '"');
	$truncate = substr($duration, $truncatePos+1, -1);
	$finalNumber = round($truncate);
	$startingPoint = round($finalNumber/21);
	//used to force new images display, ignoring cache
	$seconds = date('u');

	echo '<button onclick="videoThumbs.cancelAndClose(this);" id="close_crop">';
	include("../../images/buttons/delete.svg");
	echo '</button><span id="selectVidThumb">Select a new thumbnail</span>';

	for($i = 1; $i < 22; $i++){
		$interval = $startingPoint * $i;
		$output = $tempFolder .'thumb-'.$i.'.jpg';

		$cmd = FFMPEG. " -y -ss $interval -i $inFullEscape -s $size -an -vframes 1 -f image2 $output";

		exec($cmd);

		echo '<div><a href="#"><img data-original="./content/thumb/video/thumb-'.$i.'.jpg" src="./content/thumb/video/thumb-'.$i.'.jpg?'.$seconds.'" /></a></div>';

	} 
}

function switchThumbs(&$inThumbFull){
	$newThumb = htmlspecialchars_decode($_GET['newThumb']);

	$test1 = '../..'.substr($newThumb, 1);
	$test2 = '../..'.substr($inThumbFull, 1);

	if(rename($test1, $test2)){
		echo json_encode(array('msg'=>'FilesChanged', 'error'=>false));
	}else{
		echo json_encode(array('msg'=>'Could not move file. Sorry.', 'error'=>true));
	}
}

?>




